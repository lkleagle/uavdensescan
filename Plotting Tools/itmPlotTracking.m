clear all; close all; clc;

% make sure to update T_mocapG_vioG for each dataset

% bagFile = 'drz-rig-result_2016-03-19-15-35-46.bag';   % icp
% bagFile = 'drz-rig-result_2016-03-23-15-30-27.bag';   % vio

% bagFile = 'drz-rig-result_2016-04-07-20-15-00_clipped.bag'; % vio
% bagFile = 'drz-rig-result_2016-04-07-20-15-00_clipped2.bag'; % icp
% bagFile = 'drz-rig-result_2016-04-07-20-15-00_clipped3.bag'; % closed-loop1 (rovio estimate to icp)
% bagFile = 'drz-rig-result_2016-04-07-20-15-00_clipped4.bag'; % closed-loop2 (rovio estimate to icp and icp estimate as pose update to rovio)

% bagFile = 'drz-rig-result_2016-04-17-21-29-54_1.bag'; % vio,icp (with filter update)
% bagFile = 'drz-rig-result_2016-04-17-21-29-54_2.bag'; % icp+color (with filter update)
% bagFile = 'drz-rig-result_2016-04-17-21-29-54_3.bag'; % icp+odom+color (with filter update)
% bagFile = 'drz-rig-result_2016-04-17-21-29-54_4.bag'; % _3 with swapping
bagFile = 'drz-rig-result_2016-04-17-21-29-54_5_halfway_icp.bag'; % closed-loop2 but ICP kciks in after few minutes

% bagFile = 'drz-rig-result_2016-06-18-20-11-54_1.bag'; % fast dataset: icp+odom+color (with filter update)

% bagFile = '/media/anurag/DATA-EXT/optitrack_data/InfiniTAM/drz-rig_2016-04-17-21-29-54.bag';    % for stats
% bagFile = '/media/anurag/DATA-EXT/optitrack_data/InfiniTAM/fast/drz-rig_2016-06-18-20-11-54.bag';    % for stats
% bagFile = '/media/anurag/DATA-EXT/optitrack_data/InfiniTAM/long/drz-rig_2016-06-18-20-19-40.bag';    % for stats

% bagFile = 'result.bag';

% drz-rig-result_2016-04-17-21-29-54.bag
T_mocapG_vioG = [0.997018433560414,0.0692557250412428,0.0340277488199069,-5.37535074444125; ...
    -0.0669325883443907,0.995623068314548,-0.0652283255759770,-13.2626509160921; ...
    -0.0383962466689029,0.0627562776854801,0.997290016922261,-33.3077260981753; ...
    0         0         0         1     ];

% drz-rig-result_2016-04-17-21-29-54_4.bag
% T_mocapG_vioG = [0.996711205405121,0.0734676591905975,0.0341946790148073,3.36755803382560; ...
%     -0.0711029983150999,0.995291346396616,-0.0658748769912582,-3.18160607534302; ...
%     -0.0388733411282640,0.0632268838474957,0.997241808443900,19.7432636818979; ...
%      0         0         0         1     ];
 
% drz-rig-result_2016-06-18-20-11-54_1
% T_mocapG_vioG = [-0.176438396266925,-0.983711894866088,-0.0343569530332071,-20.9893976284256; ...
%      0.983740059931369,-0.175039095373237,-0.0402095707157636,0.642655157698904; ...
%      0.0335408230218424,-0.0408928232076172,0.998600415682433,-94.1556923308688; ...
%      0         0         0         1     ];
     
% drz-rig-result_2016-04-17-21-29-54_3.bag
% T_mocapG_icpG = [-0.986472483107334,0.117269962898739,-0.114541677453138,8.91597046540763;...
%        0.149743928716920,0.360339831942482,-0.920723607456704,-0.466696968559571;...
%       -0.0666992944825669,-0.925420424087016,-0.373025793743724,-0.651575393901992;...
%          0         0         0    1.0000];
     
% drz-rig-result_2016-04-17-21-29-54_4.bag
% T_mocapG_icpG = [-0.987411731070626,0.111008045083282,-0.112673365401489,4.05135529528871; ...
%        0.145879804122199,0.363816961979502,-0.919976250196316,-0.676549508102277; ...
%       -0.0611322835609653,-0.924832110224766,-0.375430968093774,-1.04910864722570; ...
%       0         0         0    1.0000];

% drz-rig-result_2016-06-18-20-11-54_1
% T_mocapG_icpG = [-0.104450946583976,-0.229757122635755,0.967626820812575,3.61692265243062; ...
%     -0.982805608549334,0.172801929618295,-0.0650586575650604,-0.530188802432473; ...
%     -0.152260091822136,-0.957784504843513,-0.243855913850745,-0.782532320580810; ...
%      0         0         0         1     ];
     
% drz-rig-result_2016-04-17-21-29-54_5.bag
T_mocapG_icpG = [-0.7588    0.5605   -0.3318    3.9151; ...
    0.6137    0.4445   -0.6526   -0.5745; ...
   -0.2183   -0.6988   -0.6812   -1.0165; ...
    0         0         0    1.0000];
         
bag = rosbag(bagFile);
%%
%topics
icpPoseTopic = '/itm/pose';
mocapTopic = '/drz_rig/estimated_transform';
mocapOdomTopic = '/drz_rig/estimated_odometry';
vioTopic = '/rovio/odometry';
camTopic = '/cam0/image_raw';

% mocapData = MocapInfo(bag, mocapTopic);
% vioData = VioInfo(bag, vioTopic);
msgData = ReadData(bag, {mocapTopic, mocapOdomTopic, vioTopic, icpPoseTopic, camTopic});
%%
plotMocap = 1;
plotVio = 1;
drawMocapVioEdges = 0;
plotVioUncertainity = 1;
plotIcp = 1;
drawMocapIcpEdges = 0;
showImage = 0;
plotRunningError = 0;
association_time_delta = 0.05;
running_average_width = 500;
plotLive = 0;
plotTrajectoryRange = [1 40000]; % if not live, only trajectory msgs within this range will be plotted

%setup for plotting    
if(plotMocap || plotVio || plotIcp)
    R = [1 0 0; 0 1 0; 0 0 1];
    figure(1);
    hold on
    title('real-time pose estimates from Mocap, VIsual-Inertial Odometry and ICP')
    marker = plotCamera('Location', [0 0 0], 'Orientation', R, 'Opacity', 0, 'Size', 0.07, 'Color', [1 0 0], 'Label', 'marker', 'AxesVisible', 1);
    odom = plotCamera('Location', [0 0 0], 'Orientation', R, 'Opacity', 0, 'Size', 0.07, 'Color', [0 0 1], 'Label', 'odom', 'AxesVisible', 1);
    icp = plotCamera('Location', [0 0 0], 'Orientation', R, 'Opacity', 0, 'Size', 0.07, 'Color', [0 1 0], 'Label', 'icp', 'AxesVisible', 1);
%     hold on
    grid on
    axis equal
    axis manual
    xlim([-2, 2]);
    ylim([-2, 2]);
    zlim([0, 3]);
    
    num_msgs = size(msgData.times, 1);
    plot3([0, 0.2], [0, 0], [0, 0], 'r');
    plot3([0, 0], [0, 0.2], [0, 0], 'g');
    plot3([0, 0], [0, 0], [0, 0.2], 'b');
    plot3([0, 0], [0, 0], [0, 0], 'k');

    T_mocap_vio = [-0.98165076 0.00766302 0.18984042 0.00108668; ...
                    0.17795358 -0.31287988  0.93287942 0.11919152; ...
                    0.06656298  0.94966982  0.30594812 -0.19977433];

    T_vio_mocap = my_inv(T_mocap_vio);
    T_vioG_mocapG = my_inv(T_mocapG_vioG);
    T_icpG_mocapG = my_inv(T_mocapG_icpG);
    T_vioG_vio = eye(4);
    T_icpG_icp = eye(4);
    T_mocap_mocapG = eye(4);
    latest_mocap_time = inf;
    latest_icp_time = inf;
    latest_vio_time = inf;
    position_vio = [inf,inf,inf];
    position_mocap = [inf,inf,inf];
    position_icp = [inf,inf,inf];
    distance_travelled = 0.0;
    avg_trans_vel = 0.0;
    avg_rot_vel = 0.0;
    num_mocap_odom_data = 0;
    % eval error (cost function)
    error_icp = 0;
    running_error_icp = 0;
    error_vio = 0;
    running_error_vio = 0;
    num_matches_icp = 0;
    running_num_matches_icp = 0;
    num_matches_vio = 0;
    running_num_matches_vio = 0;
    % non-live data
    vio_uncertainity_data = [];
    icp_pos_data = [];
    vio_pos_data = [];
    mocap_pos_data = [];
    icp_match_data = [];
    vio_match_data = [];
    error_icp_data = [];
    error_vio_data = [];
    running_error_icp_data = [];
    running_error_vio_data = [];
    linear_vel_data = [];
    angular_vel_data = [];
    % init alignment
    do_vio_initialization = 1;
    do_icp_initialization = 0;
    init_vio_delta = [0,0,0];
    init_icp_delta = [0,0,0];
    for i = 1:num_msgs
        disp(sprintf('msg #%d', i));
        disp(sprintf('distance travelled: %d m', distance_travelled));
        disp(sprintf('avg. translational velocity: %d m/s', avg_trans_vel));
        disp(sprintf('avg. rotational velocity: %d deg/s', 180*avg_rot_vel/pi));
        T = reshape(msgData.T_G_F(i,:), [4, 4]);
        
        % draw cameras (pose, position)
        if (strcmp(msgData.source{i}, mocapTopic) == 1)
            latest_mocap_time = msgData.times(i);
            color = [1, ((num_msgs-i)/num_msgs)*0.6, ((num_msgs-i)/num_msgs)*0.6];
            T_mocapG_mocap = T;
            T_mocap_mocapG = my_inv(T_mocapG_mocap);
            T_transformed = T_vio_mocap*T_mocap_mocapG;
            T_other = my_inv(T_transformed);
            if (position_mocap(1) ~= inf)
                distance_travelled = distance_travelled + norm(position_mocap-T_other(1:3,4)');
            end
            position_mocap = [T_other(1, 4), T_other(2, 4), T_other(3, 4)];
            if (plotMocap)
                if (plotLive)
                    marker.Orientation = T_transformed(1:3, 1:3);
                    marker.Location = position_mocap;
                    plot3(position_mocap(1), position_mocap(2), position_mocap(3), '.', 'Color', color, 'MarkerSize', 1);
                else 
                    if (i >= plotTrajectoryRange(1) && i <= plotTrajectoryRange(2))
                        mocap_pos_data = [mocap_pos_data, position_mocap'];
                    end
                end
            end
        elseif (strcmp(msgData.source{i}, mocapOdomTopic) == 1)
            tw = msgData.twist(i,:);
            linear_vel = norm(tw(1:3));
            angular_vel = norm(tw(4:6));
            avg_trans_vel = (avg_trans_vel*num_mocap_odom_data + linear_vel)/(num_mocap_odom_data+1);
            avg_rot_vel = (avg_rot_vel*num_mocap_odom_data + angular_vel)/(num_mocap_odom_data+1);
            num_mocap_odom_data = num_mocap_odom_data + 1;
            if (plotLive)
                figure(5)
                hold on
                plot(i, linear_vel, '.', 'Color', [0 0 1]);
                plot(i, angular_vel, '.', 'Color', [1 0 0]);
                title('Camera Velocity(linear, angular) in m/s')
                legend('linear', 'angular');
                xlabel('msg #')
                ylabel('velocity in m/s')
                figure(1)
            else
                linear_vel_data = [linear_vel_data, [i; linear_vel]];
                angular_vel_data = [angular_vel_data, [i; 180*angular_vel/pi]];
            end
        elseif (strcmp(msgData.source{i}, icpPoseTopic) == 1)
            color = [((num_msgs-i)/num_msgs)*0.6, 1.0, ((num_msgs-i)/num_msgs)*0.6];
            T_icpG_icp = T;
            T_icp_icpG = my_inv(T_icpG_icp);
            T_transformed = T_icp_icpG*T_icpG_mocapG;
            T_other = my_inv(T_transformed);
            position_icp = [T_other(1, 4), T_other(2, 4), T_other(3, 4)] + init_icp_delta;
            % initializing icp position == mocap position
            if (latest_icp_time == inf && do_icp_initialization)
                if (position_mocap(1) ~= inf && (abs(latest_mocap_time-msgData.times(i)) < association_time_delta))
                    disp('Initializing ICP')
                    do_icp_initialization = 0;
                    init_icp_delta = position_mocap - position_icp;
                    position_icp = position_mocap;
                    latest_icp_time = msgData.times(i);
                end
            else
                latest_icp_time = msgData.times(i);
            end
            if (plotIcp)
                if (plotLive)
                    icp.Orientation = T_transformed(1:3, 1:3);
                    icp.Location = position_icp;
                    plot3(position_icp(1), position_icp(2), position_icp(3), '.', 'Color', color, 'MarkerSize', 1);
                else 
                    if (i >= plotTrajectoryRange(1) && i <= plotTrajectoryRange(2))
                        icp_pos_data = [icp_pos_data, position_icp'];
                    end
                end
            end
        elseif (strcmp(msgData.source{i}, vioTopic) == 1)
            color = [((num_msgs-i)/num_msgs)*0.6, ((num_msgs-i)/num_msgs)*0.6, 1.0];
            T_vioG_vio = T;
            T_vio_vioG = my_inv(T_vioG_vio);
            T_transformed = T_vio_vioG*T_vioG_mocapG;
            T_other = my_inv(T_transformed);
            position_vio = [T_other(1, 4), T_other(2, 4), T_other(3, 4)] + init_vio_delta;
            % initializing vio position == mocap position
            if (latest_vio_time == inf && do_vio_initialization)
                if (position_mocap(1) ~= inf && (abs(latest_mocap_time-msgData.times(i)) < association_time_delta))
                    disp('Initializing VIO')
                    do_vio_initialization = 0;
                    init_vio_delta = position_mocap - position_vio;
                    position_vio = position_mocap;
                    latest_vio_time = msgData.times(i);
                end
            else
                latest_vio_time = msgData.times(i);
            end
            % covariance
            pose_cov = reshape(msgData.covariance{i}, 6, 6)';
            if (plotVio)
                if (plotLive)
                    odom.Orientation = T_transformed(1:3, 1:3);
                    odom.Location = position_vio;
                    plot3(position_vio(1), position_vio(2), position_vio(3), '.', 'Color', color, 'MarkerSize', 1);
                else
                    if (i >= plotTrajectoryRange(1) && i <= plotTrajectoryRange(2))
                        vio_pos_data = [vio_pos_data, position_vio'];
                    end
                end
                % TODO: transform covariance matrix to mocap inertial frame
                if (rem(distance_travelled, 0.5) < 0.05 && plotVioUncertainity)
                    covarianceEllipse3D(position_vio, pose_cov(1:3,1:3), color, 1.0);
                end
            end
        elseif (strcmp(msgData.source{i}, camTopic) == 1 && showImage)
            figure(2)
            imshow(msgData.image{i});
            figure(1)
        end
        
        
        % evaluate and plot alignment errors
        % ICP
        if (rem(i,running_average_width) == 0 && plotRunningError)
            if (plotLive)
                figure(3)
                hold on
                bar(i, 100.0*running_error_icp, running_average_width, 'c');
                figure(1)
            else
                running_error_icp_data = [running_error_icp_data, [i; 100.0*running_error_icp]];
            end
            running_error_icp = 0;
            running_num_matches_icp = 0;
        end
        if (latest_mocap_time ~= inf && latest_icp_time ~= inf)
            if (abs(latest_mocap_time-latest_icp_time) < association_time_delta)
                latest_icp_time = inf;
                % average alignment error
                error_icp = (num_matches_icp*(error_icp) + norm(position_mocap-position_icp))/...
                            (num_matches_icp+1);
                running_error_icp = (running_num_matches_icp*(running_error_icp) + norm(position_mocap-position_icp))/...
                            (running_num_matches_icp+1);
                num_matches_icp = num_matches_icp + 1;
                running_num_matches_icp = running_num_matches_icp + 1;
                if (plotLive)
                    figure(3)
                    title('Mocap - ICP alignment error')
                    xlabel('msg #')
                    ylabel('avg. error in cm')
                    hold on
                    plot(i, 100.0*error_icp, '*', 'Color', [0 0 1]);
                    figure(1)
                else
                    error_icp_data = [error_icp_data, [i; 100.0*error_icp]];
                end
                disp(sprintf('mocap - icp alignment error: %f cm', 100.0*(error_icp)));
                % plot
                if (drawMocapIcpEdges)
                    if (plotLive)
                        plot3([position_mocap(1) position_icp(1)], ...
                              [position_mocap(2) position_icp(2)], ...
                              [position_mocap(3) position_icp(3)], '-', 'Color', [0 0 0]);
                    else
                        if (i >= plotTrajectoryRange(1) && i <= plotTrajectoryRange(2))
                            icp_match_data = [icp_match_data, position_mocap', position_icp', [NaN; NaN; NaN]];
                        end
                    end
                end
                % inertial align
                T_mocap_icp_align(:,:,i) = my_inv(T_icpG_icp*T_vio_mocap*T_mocap_mocapG);
                disp('mocap-icp')
                disp(T_mocap_icp_align(:,:,i))
            end
        end
        % vio
        if (rem(i,running_average_width) == 0 && plotRunningError)
            if (plotLive)
                figure(4)
                hold on
                bar(i, 100.0*running_error_vio, running_average_width, 'c');
                figure(1)
            else
                running_error_vio_data = [running_error_vio_data, [i; 100.0*running_error_vio]];
            end
            running_error_vio = 0;
            running_num_matches_vio = 0;
        end
        % VIO
        if (latest_mocap_time ~= inf && latest_vio_time ~= inf)
            if (abs(latest_mocap_time-latest_vio_time) < association_time_delta)
                latest_vio_time = inf;
                % average alignment error
                error_vio = (num_matches_vio*(error_vio) + norm(position_mocap-position_vio))/...
                            (num_matches_vio+1);
                running_error_vio = (running_num_matches_vio*(running_error_vio) + norm(position_mocap-position_vio))/...
                                    (running_num_matches_vio+1);
                num_matches_vio = num_matches_vio + 1;
                running_num_matches_vio = running_num_matches_vio + 1;
                if (plotLive)
                    figure(4)
                    xlabel('msg #')
                    ylabel('avg. error in cm')
                    title('Mocap - Visual-Inertial Odometry alignment error')
                    hold on
                    plot(i, 100.0*error_vio, '*', 'Color', [0 0 1]);
                else
                    error_vio_data = [error_vio_data, [i; 100.0*error_vio]];
                end
                if (rem(distance_travelled, 0.5) < 0.05)
                    if (plotLive)
                        covarianceEllipse1D([i; 100.0*error_vio], pose_cov(1:3,1:3), [0 1 0], 100.0, 1);
                    else
                        error = covarianceEllipse1D([i; 100.0*error_vio], pose_cov(1:3,1:3), [0 1 0], 100.0, 0);
                        vio_uncertainity_data = [vio_uncertainity_data, [i; 100.0*error_vio; error]];
                    end
                end
                figure(1)
                disp(sprintf('mocap - vio alignment error: %f cm', 100.0*(error_vio)));
                % plot
                if (drawMocapVioEdges)
                    if (plotLive)
                        plot3([position_mocap(1) position_vio(1)], ...
                              [position_mocap(2) position_vio(2)], ...
                              [position_mocap(3) position_vio(3)], '-', 'Color', [0 1 1]);
                    else
                        if (i >= plotTrajectoryRange(1) && i <= plotTrajectoryRange(2))
                            vio_match_data = [vio_match_data, position_mocap', position_vio', [NaN; NaN; NaN]];
                        end
                    end
                end
                % inertial align
                T_mocap_vio_align(:,:,i) = my_inv(T_vioG_vio*T_vio_mocap*T_mocap_mocapG);
                disp('mocap-vio')
                disp(T_mocap_vio_align(:,:,i))
            end
        end
        drawnow;
%         pause;
    end
%     plot3(msgData.T_G_F(:,13), msgData.T_G_F(:,14), msgData.T_G_F(:,15));

%%
    % if not live
    if (~plotLive)
        % trajectory
        if (plotMocap)
            plot3(mocap_pos_data(1,:), mocap_pos_data(2,:), mocap_pos_data(3,:), '.', 'Color', [1 0 0], 'MarkerSize', 1);
        end
        if (plotIcp)
            plot3(icp_pos_data(1,:), icp_pos_data(2,:), icp_pos_data(3,:), '.', 'Color', [0 1 0], 'MarkerSize', 1);
        end
        if (plotVio)
            plot3(vio_pos_data(1,:), vio_pos_data(2,:), vio_pos_data(3,:), '.', 'Color', [0 0 1], 'MarkerSize', 1);
        end
        if (plotIcp && drawMocapIcpEdges)
            plot3(icp_match_data(1,:), icp_match_data(2,:), icp_match_data(3,:), '-', 'Color', [0 0 0]);
        end
        if (plotVio && drawMocapVioEdges)
            plot3(vio_match_data(1,:), vio_match_data(2,:), vio_match_data(3,:), '-', 'Color', [0 1 1]);
        end
        
        % icp
        if (plotIcp)
            figure
            title('Mocap - ICP alignment error')
            xlabel('msg #')
            ylabel('avg. error in cm')
            hold on
            if (plotRunningError)
                bar(running_error_icp_data(1,:), running_error_icp_data(2,:), 'c');
            end
            plot(error_icp_data(1,:), error_icp_data(2,:), '*', 'Color', [0 0 1]);
        end
        
        %vio
        if (plotVio)
            figure
            xlabel('msg #')
            ylabel('avg. error in cm')
            title('Mocap - Visual-Inertial Odometry alignment error')
            hold on
            if (plotRunningError)
                bar(running_error_vio_data(1,:), running_error_vio_data(2,:), 'c');
            end
            plot(error_vio_data(1,:), error_vio_data(2,:), '*', 'Color', [0 0 1]);
            errorbar(vio_uncertainity_data(1,:), vio_uncertainity_data(2,:), vio_uncertainity_data(3,:), '.', 'Color', color);
        end
        
        % velocities
        figure
        hold on
        [hAx,hLine1,hLine2] = plotyy(linear_vel_data(1,:), linear_vel_data(2,:), angular_vel_data(1,:), angular_vel_data(2,:));
        hLine1.LineWidth = 2;
        hLine2.LineWidth = 2;
        title('Camera Velocity (linear in m/s, angular in deg/s)')
        legend('linear', 'angular');
        xlabel('msg #')
        ylabel('velocity (m/s or deg/s)')
    end
end
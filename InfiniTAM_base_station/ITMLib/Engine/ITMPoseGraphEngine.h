/*
 * ITMPoseGraphEngine.h
 *
 *  Created on: May 17, 2016
 *      Author: anurag
 */

#ifndef INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMPOSEGRAPHENGINE_H_
#define INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMPOSEGRAPHENGINE_H_

#include "../Utils/ITMLibDefines.h"
#include "../Utils/ITMLibSettings.h"

#include "../Engine/ITMLowLevelEngine.h"

#include "../../Utils/gnuplot-iostream/gnuplot-iostream.h"
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/geometry/Point3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Rot3.h>
#include <gtsam/slam/dataset.h>

#include "kindr/rotations/RotationEigen.hpp"
// ROS
#include <ros/ros.h>
#include <nav_msgs/Path.h>

namespace rot = kindr::rotations::eigen_impl;

typedef rot::RotationQuaternionPD QPD;
typedef rot::RotationMatrixPD MPD;

namespace ITMLib
{
  namespace Engine
  {
    template<class T>
    class ITMPoseGraphEngine
    {
     protected:
      // Create an empty nonlinear factor graph
      gtsam::NonlinearFactorGraph graph_;
      gtsam::Key current_node_;
      // Create the data structure to hold the initialEstimate estimate to the solution
      gtsam::Values initial_estimates_;

     public:
      void addPrior(const T &priorMean, const gtsam::noiseModel::Base::shared_ptr &priorNoise) {
        graph_.add(gtsam::PriorFactor<T>(0, priorMean, priorNoise));
      }
      void addOdometry(const T &odometry, const gtsam::noiseModel::Base::shared_ptr &odometryNoise) {
        graph_.add(gtsam::BetweenFactor<T>(current_node_, current_node_+1, odometry, odometryNoise));
      }
      void addOdometry(const T &odometry, const gtsam::noiseModel::Base::shared_ptr &odometryNoise, const int node_id) {
        graph_.add(gtsam::BetweenFactor<T>(node_id, node_id+1, odometry, odometryNoise));
      }
      virtual void addInitialEstimate(const T &estimate) {
        initial_estimates_.insert(current_node_, estimate);
      }
      virtual void addInitialEstimate(const T &estimate, const int node_id) {
        initial_estimates_.insert(node_id, estimate);
      }
      void propogate() {
        current_node_++;
      }
      void printGraph() {
        graph_.print("\nCurrent Factor Graph:\n"); // print
      }
      void writeToFile(const std::string& fileName) {
        std::cout << "Writing results to file: " << fileName  << std::endl;
        gtsam::writeG2o(graph_, initial_estimates_, fileName);
        std::cout << "done! " << std::endl;
      }
      virtual gtsam::Values performOptimization() {
        // LM
        gtsam::LevenbergMarquardtOptimizer optimizer(graph_, initial_estimates_);

        // GN
//        gtsam::GaussNewtonParams params;
//        params.setVerbosity("TERMINATION"); // this will show info about stopping conditions
//        gtsam::GaussNewtonOptimizer optimizer(graph_, initial_estimates_, params);

        gtsam::Values result = optimizer.optimize();

        std::cout << "Optimization complete" << std::endl;
        std::cout << "initial error = " << graph_.error(initial_estimates_) << std::endl;
        std::cout << "final error = " << graph_.error(result) << std::endl;
//        result.print("Final Result:\n");
        return result;
      }
      virtual void publishTrajectory() {};

      ITMPoseGraphEngine() {
        current_node_ = 0;
      }
      ~ITMPoseGraphEngine() {};
    };

    class ITMPose3GraphEngine : public ITMPoseGraphEngine<gtsam::Pose3>
    {
     private:
      // ROS
      nav_msgs::Path ros_trajectory_, ros_optimized_trajectory_;
      geometry_msgs::PoseStamped ros_pose_, ros_optimized_pose_;
      ros::Publisher ros_trajectory_pub_, ros_optimized_trajectory_pub_;
      ros::NodeHandle nh_;

     public:
      void addPose3Prior(const Matrix4f& pose) {
//        Matrix4f bla(pose);
//        bla.setIdentity();
        gtsam::Rot3 prior_rotation_d(pose.getRot().toEigen().cast<double>());
        gtsam::Point3 prior_translation_d(pose.getTrans().x, pose.getTrans().y, pose.getTrans().z);
        Eigen::VectorXd priorSigmas(6);
        priorSigmas << 1e-6, 1e-6, 1e-6, 1e-4, 1e-4, 1e-4;
        Eigen::MatrixXd priorCovariance = Eigen::MatrixXd::Identity(6, 6);
        gtsam::noiseModel::Diagonal::shared_ptr priorNoiseModel = gtsam::noiseModel::Diagonal::Sigmas(priorSigmas);
        graph_.add(gtsam::PriorFactor<gtsam::Pose3>(current_node_, gtsam::Pose3(prior_rotation_d, prior_translation_d), priorNoiseModel));
      }
      void addInitialEstimate(const gtsam::Pose3& estimate) {
        ITMPoseGraphEngine::addInitialEstimate(estimate);

        // ROS
        ros_pose_.header.stamp = ros::Time::now();
        ros_pose_.pose.position.x = estimate.x();
        ros_pose_.pose.position.y = estimate.y();
        ros_pose_.pose.position.z = estimate.z();
//        std::cout << "mpd 1" << std::endl;
        Eigen::AngleAxisd aa(estimate.rotation().matrix());    // RotationMatrix to AxisAngle
        QPD q(MPD(aa.toRotationMatrix()));
//        std::cout << "mpd 1.1" << std::endl;
        ros_pose_.pose.orientation.x = q.x(); // JPL form
        ros_pose_.pose.orientation.y = q.y();
        ros_pose_.pose.orientation.z = q.z();
        ros_pose_.pose.orientation.w = q.w();

        ros_trajectory_.header.stamp = ros::Time::now();
        ros_trajectory_.poses.push_back(ros_pose_);
      }
      void addInitialEstimate(const gtsam::Pose3& estimate, const int node_id) {
        ITMPoseGraphEngine::addInitialEstimate(estimate, node_id);
      }
      virtual gtsam::Values performOptimization() {
        gtsam::Values result = ITMPoseGraphEngine::performOptimization();
        // ROS
        ros_optimized_trajectory_.poses.clear();
        ros_optimized_trajectory_.header.stamp = ros::Time::now();

        for (gtsam::Values::iterator it = result.begin(); it != result.end(); ++it) {
          ros_optimized_pose_.header.stamp = ros::Time::now();
          ros_optimized_pose_.pose.position.x = it->value.cast<gtsam::Pose3>().x();
          ros_optimized_pose_.pose.position.y = it->value.cast<gtsam::Pose3>().y();
          ros_optimized_pose_.pose.position.z = it->value.cast<gtsam::Pose3>().z();
//          std::cout << "mpd 0" << std::endl;
          Eigen::AngleAxisd aa(it->value.cast<gtsam::Pose3>().rotation().matrix());    // RotationMatrix to AxisAngle
          QPD q(MPD(aa.toRotationMatrix()));
          ros_optimized_pose_.pose.orientation.x = q.x(); // JPL form
          ros_optimized_pose_.pose.orientation.y = q.y();
          ros_optimized_pose_.pose.orientation.z = q.z();
          ros_optimized_pose_.pose.orientation.w = q.w();

          ros_optimized_trajectory_.poses.push_back(ros_optimized_pose_);
        }

//        result.print("Final Result:\n");
        return result;
      }
      void publishTrajectory() {
        ros_trajectory_pub_.publish(ros_trajectory_);
        ros_optimized_trajectory_pub_.publish(ros_optimized_trajectory_);
      }

      ITMPose3GraphEngine(void) : ITMPoseGraphEngine() {
        ros_trajectory_pub_ = nh_.advertise<nav_msgs::Path>("itm/trajectory", 1);
        ros_optimized_trajectory_pub_ = nh_.advertise<nav_msgs::Path>("itm/optimized_trajectory", 1);
        ros_trajectory_.header.frame_id = "infinitam_global";
        ros_optimized_trajectory_.header.frame_id = "infinitam_global";
        ros_pose_.header.frame_id = "infinitam_global";
        ros_optimized_pose_.header.frame_id = "infinitam_global";
      };
      ~ITMPose3GraphEngine(void) {};
    };

  }
}

#endif /* INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMPOSEGRAPHENGINE_H_ */

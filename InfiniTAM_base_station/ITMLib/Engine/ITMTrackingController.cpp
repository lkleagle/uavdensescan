// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMTrackingController.h"

#include "../Objects/ITMRenderState_VH.h"

#include "../ITMLib.h"

using namespace ITMLib::Engine;

std::pair<Matrix4f, float> ITMTrackingController::Track(ITMTrackingState *trackingState, const ITMView *view, ITMRenderState *renderState, const int &numReliableInactivePoints, const float &numInactiveThresholdPercent)
{
  std::pair<Matrix4f, float> lc_res(Matrix4f(), -1.0);
	if (trackingState->age_pointCloud!=-1) {
	  tracker->TrackCamera(trackingState, view);

	  // Loop Closures
	  std::cout << "num inactive points: " << renderState->noTotalInactivePoints << std::endl;
	  std::cout << "num reliable inactive points: " << numReliableInactivePoints << std::endl;
	  std::cout << "Current stable inactive map overlap percentage: " << 100.0 * static_cast<float>(numReliableInactivePoints)/(trackingState->trackingImageSize.x*trackingState->trackingImageSize.y) << std::endl;
	  if (numReliableInactivePoints >= numInactiveThresholdPercent*(trackingState->trackingImageSize.x*trackingState->trackingImageSize.y) &&
	      sdkGetTimerValue(&(renderState->timer)) > last_lc_time + 5000) {
	    std::cout << "checking for Loop Closures..." << std::endl;
	    lc_res = loopClosureDetector->DetectLoopClosure(trackingState, renderState, view);
	    if (lc_res.second > settings->lcNormThreshold) {
	      last_lc_time = sdkGetTimerValue(&(renderState->timer));
	      settings->shouldFuse = false;
	    }
	  }
	}

	trackingState->requiresFullRendering = trackingState->TrackerFarFromPointCloud() || !settings->useApproximateRaycast;

	return lc_res;
}

void ITMTrackingController::Prepare(ITMTrackingState *trackingState, const ITMView *view, ITMRenderState *renderState)
{
	//render for tracking

  trackingState->requiresFullRendering = trackingState->requiresFullRendering && tracker->isFullRenderingNeeded();

	if (settings->trackerType == ITMLibSettings::TRACKER_COLOR)
	{
		ITMPose pose_rgb(view->calib->trafo_rgb_to_depth.calib_inv * trackingState->pose_d->GetM());
		visualisationEngine->CreateExpectedDepths(&pose_rgb, &(view->calib->intrinsics_rgb), renderState);
		visualisationEngine->CreatePointCloud(view, trackingState, renderState, settings->skipPoints);
		trackingState->age_pointCloud = 0;
	}
	else
	{
		visualisationEngine->CreateExpectedDepths(trackingState->pose_d, &(view->calib->intrinsics_d), renderState);

		if (trackingState->requiresFullRendering)
		{
		  std::cout << "full rendering..." << std::endl;
			visualisationEngine->CreateICPMaps(view, trackingState, renderState);
			std::cout << "full rendering done!!" << std::endl;
			trackingState->pose_pointCloud->SetFrom(trackingState->pose_d);
			if (trackingState->age_pointCloud==-1) trackingState->age_pointCloud=-2;
			else trackingState->age_pointCloud = 0;
		}
		else
		{
		  std::cout << "forward rendering..." << std::endl;
			visualisationEngine->ForwardRender(view, trackingState, renderState);
			trackingState->age_pointCloud++;
		}
	}
}

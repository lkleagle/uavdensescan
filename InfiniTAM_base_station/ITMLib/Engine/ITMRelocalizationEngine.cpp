/*
 * ITMRelocalizationEngine.cpp
 *
 *  Created on: Jun 5, 2016
 *      Author: anurag
 */

#include "ITMRelocalizationEngine.h"

using namespace ITMLib::Engine;

ITMRelocalizationEngine::ITMRelocalizationEngine(std::string &sceneMeshFilename, const ITMLibSettings *settings, InfiniTAM::Engine::ImageSourceEngine *imageSource, InfiniTAM::Engine::IMUSourceEngine *imuSource, InfiniTAM::Engine::OdometrySourceEngine *odomSource, const ITMRGBDCalib *calib, Vector2i imgSize_rgb, Vector2i imgSize_d)
  : pc_viewer_("ICP visualizer"), scene_cloud_pointer_(new pcl::PointCloud<pcl::PointXYZRGB>),
    current_view_cloud_pointer_(new pcl::PointCloud<pcl::PointXYZRGB>), sceneMeshFilename_(sceneMeshFilename+".ply"),
    initialPoseEstimateFilename_(sceneMeshFilename+".txt"), scene_lpm_(DP::load(sceneMeshFilename_)),
    LPMBaseDir_("../InfiniTAM/Files/Relocalization_configs/"),
    LPMConfigFile_("icp_cfg_point-plane-run.yaml")
{
  tracker_type_ = DepthTrackerType::TRACKER_LPM; // Change this as per need

  readInitialPoseEstimate(initialPoseEstimateFilename_, approxInvPose_);

  settings_ = settings;
  viewHierarchy_ = new ITMImageHierarchy<ITMTemplatedHierarchyLevel<ITMFloatImage> >(imgSize_d, settings->trackingRegime, settings->noHierarchyLevels, MEMORYDEVICE_CPU, true);

  noIterationsPerLevel_ = new int[settings->noHierarchyLevels];
  distThresh_ = new float[settings->noHierarchyLevels];

  noIterationsPerLevel_[0] = 2; //TODO -> make parameter
  for (int levelId = 1; levelId < settings->noHierarchyLevels; levelId++)
  {
    noIterationsPerLevel_[levelId] = noIterationsPerLevel_[levelId - 1] + 2;
  }
  noIterationsPerLevel_[0] = 10; //TODO -> make parameter

  float distThreshStep = settings->depthTrackerICPThreshold / settings->noHierarchyLevels;
  distThresh_[settings->noHierarchyLevels - 1] = settings->depthTrackerICPThreshold;
  for (int levelId = settings->noHierarchyLevels - 2; levelId >= 0; levelId--)
    distThresh_[levelId] = distThresh_[levelId + 1] - distThreshStep;

  noICPLevel_ = settings->noICPRunTillLevel;

  terminationThreshold_ = settings->depthTrackerTerminationThreshold;

  lowLevelEngine_ = new ITMLowLevelEngine_CPU();
  viewBuilder_ = new ITMViewBuilder_CPU(calib);

  view_ = NULL; // will be allocated by the view builder

  imageSource_ = imageSource;
  imuSource_ = imuSource;
  odomSource_ = odomSource;

  bool allocateGPU = false;

  inputRGBImage_ = new ITMUChar4Image(imageSource->getRGBImageSize(), true, allocateGPU);
  inputRawDepthImage_ = new ITMShortImage(imageSource->getDepthImageSize(), true, allocateGPU);
  inputIMUMeasurement_ = new ITMIMUMeasurement();
  inputOdometryMeasurement_ = new ITMOdometryMeasurement();

  // PCL
  pcl::io::loadPLYFile(sceneMeshFilename_, *scene_cloud_pointer_);
  //filterSceneMesh(scene_cloud_pointer_, approxInvPose_, calib->intrinsics_d.projectionParamsSimple.all, imgSize_d);
  if (viz_icp_) {
    pc_viewer_.setBackgroundColor(0, 0, 0);
    pc_viewer_.addPointCloud<pcl::PointXYZRGB>(scene_cloud_pointer_, "scene cloud");
    pc_viewer_.setPointCloudRenderingProperties(
        pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "scene cloud");
    pc_viewer_.addPointCloud<pcl::PointXYZRGB>(current_view_cloud_pointer_, "current scan");
    pc_viewer_.setPointCloudRenderingProperties(
        pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "current scan");
  }
}

ITMRelocalizationEngine::~ITMRelocalizationEngine() {
  delete viewBuilder_;
  delete lowLevelEngine_;
  if (view_ != NULL) delete view_;

  delete inputRGBImage_;
  delete inputRawDepthImage_;
  delete inputIMUMeasurement_;
  delete inputOdometryMeasurement_;
}

void ITMRelocalizationEngine::filterSceneMesh(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud, const Matrix4f &pose, const Vector4f &intrinsics, const Vector2i &imgSize) {

  Matrix4f invPose;
  pose.inv(invPose);
  Vector4i lims;
  lims.x = -imgSize.x / 8; lims.y = imgSize.x + imgSize.x / 8;
  lims.z = -imgSize.y / 8; lims.w = imgSize.y + imgSize.y / 8;
  for (pcl::PointCloud<pcl::PointXYZRGB>::iterator it = cloud->begin(); it != cloud->end(); ++it) {
    Vector4f point(it->x, it->y, it->z, 1.0);
    Vector4f point_camera = invPose * point;

    if (point_camera.z > 0) {
      Vector2i pixel_camera(
          static_cast<int>(intrinsics.x * point_camera.x / point_camera.z + intrinsics.z),
          static_cast<int>(intrinsics.y * point_camera.y / point_camera.z + intrinsics.w));
      if (pixel_camera.x < lims.x || pixel_camera.x >= lims.y || pixel_camera.y < lims.z || pixel_camera.y >= lims.w) {
        cloud->points.erase(it);
      }
    }
  }
}

bool ITMRelocalizationEngine::readInitialPoseEstimate(const std::string &fileName, Matrix4f & dest)
{
  std::ifstream src(fileName);
  src >> dest.m00 >> dest.m10 >> dest.m20 >> dest.m30;
  src >> dest.m01 >> dest.m11 >> dest.m21 >> dest.m31;
  src >> dest.m02 >> dest.m12 >> dest.m22 >> dest.m32;
  dest.m03 = 0.0f; dest.m13 = 0.0f; dest.m23 = 0.0f; dest.m33 = 1.0f;
  if (src.fail()) return false;

  return true;
}

void ITMRelocalizationEngine::SetEvaluationData()
{
  viewHierarchy_->levels[0]->intrinsics = view_->calib->intrinsics_d.projectionParamsSimple.all;

  // the image hierarchy allows pointers to external data at level 0
  viewHierarchy_->levels[0]->depth = view_->depth;
}

void ITMRelocalizationEngine::PrepareForEvaluation()
{
  for (int i = 1; i < viewHierarchy_->noLevels; i++)
  {
    ITMTemplatedHierarchyLevel<ITMFloatImage> *currentLevelView = viewHierarchy_->levels[i], *previousLevelView = viewHierarchy_->levels[i - 1];
    lowLevelEngine_->FilterSubsampleWithHoles(currentLevelView->depth, previousLevelView->depth);
    currentLevelView->intrinsics = previousLevelView->intrinsics * 0.5f;
  }
}

void ITMRelocalizationEngine::SetEvaluationParams(int levelId)
{
  levelId_ = levelId;
  iterationType_ = viewHierarchy_->levels[levelId]->iterationType;
  viewHierarchyLevel_ = viewHierarchy_->levels[levelId];
}

Matrix4f ITMRelocalizationEngine::RelocalizeFrame() {

  // Data sources
  if (!imageSource_->hasMoreImages()) return 1;
  imageSource_->getImages(inputRGBImage_, inputRawDepthImage_);

//  if (imuSource_ != NULL) {
//    if (!imuSource_->hasMoreMeasurements()) return;
//    else imuSource_->getMeasurement(inputIMUMeasurement_);
//  } else if (odomSource_ != NULL) {
//    if (!odomSource_->hasMoreMeasurements()) return;
//    else odomSource_->getMeasurement(inputOdometryMeasurement_);
//  }

  std::cout << "got input data..." << std::endl;

  // prepare image and turn it into a depth image
  if (inputOdometryMeasurement_==NULL) viewBuilder_->UpdateView(&view_, inputRGBImage_, inputRawDepthImage_, settings_->useBilateralFilter, settings_->modelSensorNoise);
  else viewBuilder_->UpdateView(&view_, inputRGBImage_, inputRawDepthImage_, settings_->useBilateralFilter, inputOdometryMeasurement_);

  std::cout << "updated view..." << std::endl;

  SetEvaluationData();
  PrepareForEvaluation();

  int noValidPoints_new;
  Vector4f* matches;

  // Libpointmatcher
  if (tracker_type_ == DepthTrackerType::TRACKER_LPM) {
    SetEvaluationParams(0);
    return getLPMICPTF(approxInvPose_);
  }

  for (int levelId = viewHierarchy_->noLevels - 1; levelId >= noICPLevel_; levelId--)
  {
    SetEvaluationParams(levelId);
    if (iterationType_ == TRACKER_ITERATION_NONE) continue;

    // Libpointmatcher
    if (tracker_type_ == DepthTrackerType::TRACKER_LPM_HIERARCHY) {
      std::cout << "Level ID: " << levelId << std::endl;
      approxInvPose_ = getLPMICPTF(approxInvPose_);
    }
  }

//  // Visualization
//  if (viz_icp_) {
//    std::vector<Matrix4f*> tf_chain{&approxInvPose, &scenePose};
//    // visualize TF update
//    visualizeTracker(
//        sceneHierarchyLevel->pointsMap, viewHierarchyLevel_->depth,
//        viewHierarchyLevel_->intrinsics, memory_type, tf_chain, converged);
//  }

  return approxInvPose_;
}

// Use libpointmatcher for ICP routine
Matrix4f ITMRelocalizationEngine::getLPMICPTF(Matrix4f& prevInvPose) {
  Matrix4f sceneInvPose;
  sceneInvPose.setIdentity();
  DP current_view_lpm = FloatImagetoLPMPointCloud(
      viewHierarchyLevel_->depth, viewHierarchyLevel_->intrinsics);

  // load YAML config
  std::ifstream conf((LPMBaseDir_+LPMConfigFile_).c_str());
  if (!conf.good())
  {
    std::cerr << "Cannot open ICP config file"; exit(1);
  }
  icp_.loadFromYaml(conf);

  // Camera inverse pose
  PM::TransformationParameters prev_cam_tf = PM::TransformationParameters::Identity(4, 4);
  for (int row = 0; row < 3; ++row) {
    for (int col = 0; col < 4; ++col) {
      prev_cam_tf(row, col) = prevInvPose(col, row);
    }
  }
  // Scene inverse pose
  PM::TransformationParameters prev_scene_tf = PM::TransformationParameters::Identity(4, 4);
  for (int row = 0; row < 3; ++row) {
    for (int col = 0; col < 4; ++col) {
      prev_scene_tf(row, col) = sceneInvPose(col, row);
    }
  }

  PM::Transformation* rigidTrans;
  rigidTrans = PM::get().REG(Transformation).create("RigidTransformation");

  // TF sanity check
  if (!rigidTrans->checkParameters(prev_cam_tf)) {
    std::cerr << std::endl
       << "Initial camera transformation is not rigid, identity will be used"
       << std::endl;
    prev_cam_tf = PM::TransformationParameters::Identity(4, 4);
  }
  if (!rigidTrans->checkParameters(prev_scene_tf)) {
    std::cerr << std::endl
       << "Initial scene transformation is not rigid, identity will be used"
       << std::endl;
    prev_scene_tf = PM::TransformationParameters::Identity(4, 4);
  }

  // initialize current scan by best pose estimate from previous run
  // Also transform scene to global ref frame
  const DP GlobalScan = rigidTrans->compute(current_view_lpm, prev_cam_tf);
  const DP GlobalScene = rigidTrans->compute(scene_lpm_, prev_scene_tf);

  // Compute the transformation to express scan in ref
  PM::TransformationParameters T = icp_(GlobalScan, GlobalScene);
  std::cout << "LPM match ratio: " << icp_.errorMinimizer->getWeightedPointUsedRatio() << std::endl;
  std::cout << "ICP transformation:" << std::endl << T << std::endl;

  // Transform data to express it in ref
  DP data_out(GlobalScan);
  icp_.transformations.apply(data_out, T);

  // Safe files to see the results
  GlobalScene.save(LPMBaseDir_ + "test_ref.vtk");
  GlobalScan.save(LPMBaseDir_ + "test_data_in.vtk");
  data_out.save(LPMBaseDir_ + "test_data_out.vtk");

  // Refined Camera inverse pose
  Matrix4f newInvPose = prevInvPose;
  PM::TransformationParameters new_cam_tf = prev_scene_tf * T;
  for (int row = 0; row < 3; ++row) {
    for (int col = 0; col < 4; ++col) {
      newInvPose(col, row) = new_cam_tf(row, col);
    }
  }
  return newInvPose;
}

// FloatImage to LPM Point cloud
DP ITMRelocalizationEngine::FloatImagetoLPMPointCloud(const ITMFloatImage* im, const Vector4f intrinsics) {

  DP::Labels feature_labels;
  feature_labels.push_back(DP::Label("x"));
  feature_labels.push_back(DP::Label("y"));
  feature_labels.push_back(DP::Label("z"));

  const float* v = im->GetData(MemoryDeviceType(MEMORYDEVICE_CPU));
  float point;
  PM::Matrix features(4, im->noDims.width * im->noDims.height);
  for (int row = 0; row < im->noDims.height; ++row) {
    for (int col = 0; col < im->noDims.width; ++col) {
      point = v[row*im->noDims.width + col];

      Vector4f vec_point(point * ((float(col) - intrinsics.z) / intrinsics.x),
                         point * ((float(row) - intrinsics.w) / intrinsics.y),
                         point, 1.0);

      features(0, row*im->noDims.width + col) = vec_point.x;
      features(1, row*im->noDims.width + col) = vec_point.y;
      features(2, row*im->noDims.width + col) = vec_point.z;
      features(3, row*im->noDims.width + col) = 1.0;
    }
  }

  return DP(features, feature_labels);
}

// FloatImage to PCL point cloud
void ITMRelocalizationEngine::FloatImagetoPclPointCloud(
    const ITMFloatImage* im, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
    const Vector4f intrinsics, Vector3i color, int memory_type, std::vector<Matrix4f*>& tf_chain) {

  cloud->clear();
  const float* v = im->GetData(MemoryDeviceType(MEMORYDEVICE_CPU));
  float point;
  pcl::PointXYZRGB pc_point;
  for (int row = 0; row < im->noDims.height; ++row) {
    for (int col = 0; col < im->noDims.width; ++col) {
      point = v[row*im->noDims.width + col];

      Vector4f vec_point(point * ((float(col) - intrinsics.z) / intrinsics.x),
                         point * ((float(row) - intrinsics.w) / intrinsics.y),
                         point, 1.0);

      // apply transform chain
      for (std::vector<Matrix4f*>::iterator it = tf_chain.begin(); it != tf_chain.end(); ++it) {
        vec_point = (**it) * vec_point;
        vec_point.w = 1.0;
      }

      pc_point.x = vec_point.x;
      pc_point.y = vec_point.y;
      pc_point.z = vec_point.z;

      pc_point.r = color[0];
      pc_point.g = color[1];
      pc_point.b = color[2];

      cloud->push_back(pc_point);
    }
  }
}


// Draw ICP point matches
void ITMRelocalizationEngine::DrawPointMatches(
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, Vector4f* matches, Vector3i color) {

  std::cout << "scan cloud size: " << cloud->size() << " " << viewHierarchyLevel_->depth->noDims.height * viewHierarchyLevel_->depth->noDims.width << std::endl;
  for (int i = 0; i < cloud->size(); i+=10) {
    if (matches[i].w != 0.0) {
      pcl::PointXYZ m;
      m.x = matches[i].x;
      m.y = matches[i].y;
      m.z = matches[i].z;
      std::string l_string("line");
      l_string += std::to_string(i);
      if (!pcl::isFinite(cloud->points[i]) || !pcl::isFinite(m)) std::cout << "NOT FINITE POINT!!!!!!!!!!!" << std::endl;
      if (cloud->points[i].x == m.x && cloud->points[i].y == m.y && cloud->points[i].z == m.z) std::cout << "ZERO LENGTH LINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      if (cloud->points[i].x != m.x || cloud->points[i].y != m.y || cloud->points[i].z != m.z) {
        pc_viewer_.addLine(cloud->points[i], m, color.r, color.g, color.b, l_string);
      }
    }
  }
}


// Tracker TF Update Visualization
void ITMRelocalizationEngine::visualizeTracker(
    const ITMFloatImage* current_view, const Vector4f intrinsics,
    int memory_type, std::vector<Matrix4f*>& tf_chain, bool converged) {

  // scene
  pc_viewer_.updatePointCloud(scene_cloud_pointer_, "scene cloud");

  // current view
  FloatImagetoPclPointCloud(current_view, current_view_cloud_pointer_, intrinsics,
                            Vector3i(0, 0, 255), memory_type, tf_chain);
  pc_viewer_.updatePointCloud(current_view_cloud_pointer_, "current scan");

  // Message
  if (converged) {
    std::string msg("msg0");
    pc_viewer_.addText("ICP converged", 50, 50, 30, 1.0, 1.0, 1.0, msg);
  }

  pcl_render_stop_ = false;
  boost::thread t(boost::bind(&ITMRelocalizationEngine::pcl_render_loop, this));
  if (std::cin.get() == '\n') {
    std::cout << "Pressed ENTER" << std::endl;
    pcl_render_stop_ = true;
    std::cout << "waiting to join...." << std::endl;
    t.join();
    std::cout << "joined!!!" << std::endl;
  }
}


// Tracker Matches Visualization
void ITMRelocalizationEngine::visualizeTracker(
    const ITMFloatImage* current_view, const Vector4f intrinsics,
    Vector4f* matches, int memory_type, std::vector<Matrix4f*>& tf_chain) {

//  pc_viewer.removeAllPointClouds();
  // scene
  pc_viewer_.updatePointCloud(scene_cloud_pointer_, "scene cloud");

  // current view
  FloatImagetoPclPointCloud(current_view, current_view_cloud_pointer_, intrinsics,
                            Vector3i(0, 0, 255), memory_type, tf_chain);
  pc_viewer_.updatePointCloud(current_view_cloud_pointer_, "current scan");

  // matches
  DrawPointMatches(current_view_cloud_pointer_, matches, Vector3i(255, 255, 255));

  pcl_render_stop_ = false;
  boost::thread t(boost::bind(&ITMRelocalizationEngine::pcl_render_loop, this));
  if (std::cin.get() == '\n') {
    std::cout << "Pressed ENTER" << std::endl;
    pcl_render_stop_ = true;
    std::cout << "waiting to join...." << std::endl;
    t.join();
    std::cout << "joined!!!" << std::endl;
    std::cout << "here -1" << std::endl;
  }
}

void ITMRelocalizationEngine::pcl_render_loop() {
  std::cout << "stop flag: " << pcl_render_stop_ << std::endl;
  while (!pcl_render_stop_) {
    std::cout << "SPINNING......." << std::endl;
    pc_viewer_.spinOnce (100);
  }
  pc_viewer_.removeAllShapes();
}



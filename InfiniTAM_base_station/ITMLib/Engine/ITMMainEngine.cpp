// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMMainEngine.h"

using namespace ITMLib::Engine;

ITMMainEngine::ITMMainEngine(const ITMLibSettings *settings, const ITMRGBDCalib *calib, Vector2i imgSize_rgb, Vector2i imgSize_d)
  : pc_viewer("Cloud visualizer"), pcl_cloud_pointer(new pcl::PointCloud<pcl::PointXYZRGB>)
{
	// create all the things required for marching cubes and mesh extraction
	// - uses additional memory (lots!)
	static const bool createMeshingEngine = true;

	if ((imgSize_d.x == -1) || (imgSize_d.y == -1)) imgSize_d = imgSize_rgb;

	this->settings = settings;

	this->scene = new ITMScene<ITMVoxel, ITMVoxelIndex>(&(settings->sceneParams), settings->useSwapping, 
		settings->deviceType == ITMLibSettings::DEVICE_CUDA ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);

	this->viz_pal = new ITMPalette(settings->deviceType == ITMLibSettings::DEVICE_CUDA ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU);
	this->viz_pal->SetPaletteType(ITMPalette::False_color_palette4);

	meshingEngine = NULL;
	switch (settings->deviceType)
	{
	case ITMLibSettings::DEVICE_CPU:
		lowLevelEngine = new ITMLowLevelEngine_CPU();
		viewBuilder = new ITMViewBuilder_CPU(calib);
		visualisationEngine = new ITMVisualisationEngine_CPU<ITMVoxel, ITMVoxelIndex>(scene);
		if (createMeshingEngine) meshingEngine = new ITMMeshingEngine_CPU<ITMVoxel, ITMVoxelIndex>();
		break;
	case ITMLibSettings::DEVICE_CUDA:
#ifndef COMPILE_WITHOUT_CUDA
		lowLevelEngine = new ITMLowLevelEngine_CUDA();
		viewBuilder = new ITMViewBuilder_CUDA(calib);
		visualisationEngine = new ITMVisualisationEngine_CUDA<ITMVoxel, ITMVoxelIndex>(scene, viz_pal);
		if (createMeshingEngine) meshingEngine = new ITMMeshingEngine_CUDA<ITMVoxel, ITMVoxelIndex>();
#endif
		break;
	case ITMLibSettings::DEVICE_METAL:
#ifdef COMPILE_WITH_METAL
		lowLevelEngine = new ITMLowLevelEngine_Metal();
		viewBuilder = new ITMViewBuilder_Metal(calib);
		visualisationEngine = new ITMVisualisationEngine_Metal<ITMVoxel, ITMVoxelIndex>(scene);
		if (createMeshingEngine) meshingEngine = new ITMMeshingEngine_CPU<ITMVoxel, ITMVoxelIndex>();
#endif
		break;
	}

	mesh = NULL;
	if (createMeshingEngine) mesh = new ITMMesh(settings->deviceType == ITMLibSettings::DEVICE_CUDA ? MEMORYDEVICE_CUDA : MEMORYDEVICE_CPU, ITMVoxel::hasColorInformation);

	Vector2i trackedImageSize = ITMTrackingController::GetTrackedImageSize(settings, imgSize_rgb, imgSize_d);

	sdkCreateTimer(&main_timer, true);
  sdkStartTimer(&main_timer);

	renderState_live = visualisationEngine->CreateRenderState(trackedImageSize, sdkGetTimerValue(&main_timer));
	renderState_freeview = NULL; //will be created by the visualisation engine

	denseMapper = new ITMDenseMapper<ITMVoxel, ITMVoxelIndex>(settings);
	float check_time = sdkGetTimerValue(&main_timer);
	denseMapper->ResetScene(scene, sdkGetTimerValue(&main_timer));

	// Place recognition
	std::string brief_pattern_file = std::string(settings->resourcesDirectory) + std::string("/PlaceDetector/brief_pattern.yml");
	std::string vocabulary_file = std::string(settings->resourcesDirectory) + std::string("/PlaceDetector/brief_k10L6.voc.gz");
	featureExtractor = new BriefExtractor(brief_pattern_file);
	placeDetector = new ITMPlaceDetector<BriefVocabulary, BriefLoopDetector, FBrief::TDescriptor>(vocabulary_file, Vector2i(640, 480), 5000);

	imuCalibrator = new ITMIMUCalibrator_DRZ(calib->trafo_rgb_to_imu);
	tracker = ITMTrackerFactory<ITMVoxel, ITMVoxelIndex>::Instance().Make(trackedImageSize, settings, lowLevelEngine, imuCalibrator, scene);
	poseGraphEngine = new ITMPose3GraphEngine();
	gtsam::Rot3 prior_rotation_d(Eigen::Matrix3d::Identity());
  gtsam::Point3 prior_translation_d(0, 0, 0);
  Eigen::VectorXd priorSigmas(6);
  priorSigmas << 0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
  Eigen::MatrixXd priorCovariance = Eigen::MatrixXd::Identity(6, 6);
  gtsam::noiseModel::Diagonal::shared_ptr priorNoiseModel = gtsam::noiseModel::Diagonal::Sigmas(priorSigmas);
//  gtsam::SharedNoiseModel priorNoiseModel = gtsam::noiseModel::Gaussian::Information(priorCovariance);
  poseGraphEngine->addPrior(gtsam::Pose3(prior_rotation_d, prior_translation_d), priorNoiseModel);
  poseGraphEngine->addInitialEstimate(gtsam::Pose3(prior_rotation_d, prior_translation_d));
	switch (settings->deviceType)
  {
  case ITMLibSettings::DEVICE_CPU:
    loopClosureDetector = new ITMLoopClosureDetection_CPU(trackedImageSize, settings->trackingRegime, settings->noHierarchyLevels,
        settings->noICPRunTillLevel, settings->depthTrackerICPThreshold, settings->depthTrackerTerminationThreshold, settings->LCTrackerType,
        settings->visualizeLC, std::string(settings->resourcesDirectory)+std::string("/LPM/"), lowLevelEngine);
    break;
  case ITMLibSettings::DEVICE_CUDA:
#ifndef COMPILE_WITHOUT_CUDA
    loopClosureDetector = new ITMLoopClosureDetection_CUDA(trackedImageSize, settings->trackingRegime, settings->noHierarchyLevels,
        settings->noICPRunTillLevel, settings->depthTrackerICPThreshold, settings->depthTrackerTerminationThreshold, settings->LCTrackerType,
        settings->visualizeLC, std::string(settings->resourcesDirectory)+std::string("/LPM/"), lowLevelEngine);
#endif
    break;
  }
	trackingController = new ITMTrackingController(tracker, loopClosureDetector, visualisationEngine, lowLevelEngine, settings);

	trackingState = trackingController->BuildTrackingState(trackedImageSize, this->settings->visualizeICP, this->settings->visualizeLC);
	sdkRewindTimerByTime(&(trackingState->tracking_timer), sdkGetTimerValue(&main_timer));
	tracker->UpdateInitialPose(trackingState);

	view = NULL; // will be allocated by the view builder

	fusionActive = true;
	mainProcessingActive = true;

  // PCL
  pc_viewer.setBackgroundColor(0, 0, 0);
  pc_viewer.addPointCloud<pcl::PointXYZRGB>(pcl_cloud_pointer, "cloud");
  pc_viewer.setPointCloudRenderingProperties(
      pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");

  // ROS
  ITMPoseMsg.header.frame_id = "infinitam_global";
  ITMPoseMsg.child_frame_id = "IMU";
  pubITMPose = nh.advertise<geometry_msgs::TransformStamped>("itm/pose", 1);
  pubITMImage = nh.advertise<sensor_msgs::Image>("itm/image", 1);
  subClock = nh.subscribe("clock", 1000, &ITMMainEngine::clockCallback,this);
}

ITMMainEngine::~ITMMainEngine()
{
	delete renderState_live;
	if (renderState_freeview!=NULL) delete renderState_freeview;

	delete scene;

	delete denseMapper;
	sdkDeleteTimer(&main_timer);
	delete trackingController;

	delete tracker;
	delete loopClosureDetector;
	delete imuCalibrator;

	delete featureExtractor;
	delete placeDetector;

	delete lowLevelEngine;
	delete viewBuilder;

	delete trackingState;
	if (view != NULL) delete view;

	delete visualisationEngine;

	if (meshingEngine != NULL) delete meshingEngine;

	if (mesh != NULL) delete mesh;
}

// Clock subscriber callback
void ITMMainEngine::clockCallback(const rosgraph_msgs::ClockConstPtr & clock_msg) {
  clockAvailable = true;
  clockMsg = *clock_msg;
}

ITMMesh* ITMMainEngine::UpdateMesh(void)
{
	if (mesh != NULL) meshingEngine->MeshScene(mesh, scene);
	return mesh;
}

void ITMMainEngine::SaveSceneToMesh(const char *objFileName)
{
	if (mesh == NULL) return;
	meshingEngine->MeshScene(mesh, scene);
//	mesh->WriteSTL(objFileName);
	char fileName[100];
  strcpy(fileName, const_cast<char*>(settings->storageDirectory));
	strcat(fileName, "/");
	strcat(fileName, objFileName);
	mesh->WritePLY(fileName);
}

void ITMMainEngine::SaveGraph(const char *graphFileName)
{
  if (poseGraphEngine == NULL) return;
  // store pose graph
  char fileName[100];
  strcpy(fileName, const_cast<char*>(settings->storageDirectory));
  strcat(fileName, "/");
  strcat(fileName, graphFileName);
  poseGraphEngine->writeToFile(std::string(fileName));
}

void ITMMainEngine::ProcessFrame(ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage, ITMPose *rvizPose, bool publishROSImage)
{
  // deafult -> should fuse
  settings->shouldFuse = true;

  // prepare image and turn it into a depth image
  viewBuilder->UpdateView(&view, rgbImage, rawDepthImage, settings->useBilateralFilter,settings->modelSensorNoise);

  if (!mainProcessingActive) return;

  // Place recognition
  if (ToCvImage(&rgb_image, rgbImage) == 0) {
    cv::cvtColor(rgb_image, grayscale_image, CV_BGR2GRAY);
    cv::resize(grayscale_image, grayscale_image, cv::Size(640, 480));
    placeDetector->ProcessFrame(grayscale_image, trackingState->pose_d, *featureExtractor);
  }

  // tracking
  int numStableInactivePoints = lowLevelEngine->FilterInactiveMap(trackingState->pointCloud->filtered_inactive_locations, trackingState->pointCloud->inactive_locations, trackingState->pointCloud->inactive_reliability, (float)settings->inactiveReliabilityThresh);
  trackingController->Track(trackingState, view, renderState_live, numStableInactivePoints, (float)settings->numInactiveThresholdPercent);

  // publish ITM tracker pose
  PublishROSPoseMsg();

  // fusion
  visualization_msgs::Marker swappedOutMeshMsg;
  if (fusionActive) denseMapper->ProcessFrame(view, trackingState, scene, renderState_live, &swappedOutMeshMsg, settings->deltaTime, settings->shouldFuse);

  // raycast to renderState_live for tracking and free visualisation
  trackingController->Prepare(trackingState, view, renderState_live);

  // ROS
  if (publishROSImage) {
    if (rvizPose != NULL) {
      PublishROSImageMsg(ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME, rvizPose, &view->calib->intrinsics_d);
    } else {
      PublishROSImageMsg(ITMMainEngine::InfiniTAM_IMAGE_SCENERAYCAST);
    }
  }
}

void ITMMainEngine::ProcessFrame(ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage, ITMIMUMeasurement *imuMeasurement, ITMPose *rvizPose, bool publishROSImage)
{
  // deafult -> should fuse
  settings->shouldFuse = true;

	// prepare image and turn it into a depth image
	if (imuMeasurement==NULL) viewBuilder->UpdateView(&view, rgbImage, rawDepthImage, settings->useBilateralFilter,settings->modelSensorNoise);
	else viewBuilder->UpdateView(&view, rgbImage, rawDepthImage, settings->useBilateralFilter, imuMeasurement);

	if (!mainProcessingActive) return;

	// Place recognition
  if (ToCvImage(&rgb_image, rgbImage) == 0) {
    cv::cvtColor(rgb_image, grayscale_image, CV_BGR2GRAY);
    cv::resize(grayscale_image, grayscale_image, cv::Size(640, 480));
    placeDetector->ProcessFrame(grayscale_image, trackingState->pose_d, *featureExtractor);
  }

	// tracking
	int numStableInactivePoints = lowLevelEngine->FilterInactiveMap(trackingState->pointCloud->filtered_inactive_locations, trackingState->pointCloud->inactive_locations, trackingState->pointCloud->inactive_reliability, (float)settings->inactiveReliabilityThresh);
	trackingController->Track(trackingState, view, renderState_live, numStableInactivePoints, (float)settings->numInactiveThresholdPercent);

	// publish ITM tracker pose
  PublishROSPoseMsg();

	// fusion
  visualization_msgs::Marker swappedOutMeshMsg;
	if (fusionActive) denseMapper->ProcessFrame(view, trackingState, scene, renderState_live, &swappedOutMeshMsg, settings->deltaTime, settings->shouldFuse);

	// raycast to renderState_live for tracking and free visualisation
	trackingController->Prepare(trackingState, view, renderState_live);

	// ROS
	if (publishROSImage) {
    if (rvizPose != NULL) {
      PublishROSImageMsg(ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME, rvizPose, &view->calib->intrinsics_d);
    } else {
      PublishROSImageMsg(ITMMainEngine::InfiniTAM_IMAGE_SCENERAYCAST);
    }
	}
}

void ITMMainEngine::ProcessFrame(ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage, ITMOdometryMeasurement *odomMeasurement, ITMPose *rvizPose, bool publishROSImage)
{
  // deafult -> should fuse
  settings->shouldFuse = true;

  // prepare image and turn it into a depth image
  if (odomMeasurement==NULL) viewBuilder->UpdateView(&view, rgbImage, rawDepthImage, settings->useBilateralFilter,settings->modelSensorNoise);
  else viewBuilder->UpdateView(&view, rgbImage, rawDepthImage, settings->useBilateralFilter, odomMeasurement);

  if (!mainProcessingActive) return;

  // Place recognition
  if (ToCvImage(&rgb_image, rgbImage) == 0) {
    cv::cvtColor(rgb_image, grayscale_image, CV_BGR2GRAY);
    cv::resize(grayscale_image, grayscale_image, cv::Size(640, 480));
    placeDetector->ProcessFrame(grayscale_image, trackingState->pose_d, *featureExtractor);
    cv::imshow("Display window", grayscale_image);
  }

  // tracking
  int numStableInactivePoints = lowLevelEngine->FilterInactiveMap(trackingState->pointCloud->filtered_inactive_locations, trackingState->pointCloud->inactive_locations, trackingState->pointCloud->inactive_reliability, (float)settings->inactiveReliabilityThresh);
  std::pair<Matrix4f, float> lc_res = trackingController->Track(trackingState, view, renderState_live, numStableInactivePoints, (float)settings->numInactiveThresholdPercent);

  // Build Pose Graph
  if (odomMeasurement != NULL && trackingState->age_pointCloud!=-1) {
    // Odometry update
    Matrix4f bla;
    imuCalibrator->differential_trafo_change.inv(bla);
//    gtsam::Rot3 odom_rot(imuCalibrator->differential_trafo_change.getRot().toEigen().cast<double>());
//    gtsam::Point3 odom_trans(imuCalibrator->differential_trafo_change.getTrans().x, imuCalibrator->differential_trafo_change.getTrans().y, imuCalibrator->differential_trafo_change.getTrans().z);
    gtsam::Rot3 odom_rot(bla.getRot().toEigen().cast<double>());
    QPD odom_quat;//(MPD(imuCalibrator->differential_trafo_change.getRot().toEigen().cast<double>()));
    gtsam::Point3 odom_trans(bla.getTrans().x, bla.getTrans().y, bla.getTrans().z);
    gtsam::Pose3 odom_pose(odom_rot, odom_trans);
    gtsam::SharedNoiseModel noiseModel = gtsam::noiseModel::Gaussian::Information(((ITMViewOdometry*)view)->odom->cov.toEigen());
    if (std::isfinite(odom_trans.x()) && std::isfinite(odom_trans.y()) && std::isfinite(odom_trans.z()) &&
        std::isfinite(odom_quat.x()) && std::isfinite(odom_quat.y()) && std::isfinite(odom_quat.z()) && std::isfinite(odom_quat.w())) {
      poseGraphEngine->addOdometry(odom_pose, noiseModel);
    } else {
      poseGraphEngine->addOdometry(gtsam::Pose3(), noiseModel);
    }
    poseGraphEngine->propogate();
//  std::cout << "noise check: " << ((ITMViewOdometry*)view)->odom->cov.toEigen() << std::endl;
//    std::cout << "current cam pose: " << trackingState->pose_d->GetM() << std::endl;

    // initial estimates
    gtsam::Rot3 rotation_d = gtsam::Rot3(trackingState->pose_d->GetInvM().getRot().toEigen().cast<double>());
    gtsam::Point3 translation_d = gtsam::Point3(trackingState->pose_d->GetInvM().getTrans().x, trackingState->pose_d->GetInvM().getTrans().y, trackingState->pose_d->GetInvM().getTrans().z);
    poseGraphEngine->addInitialEstimate(gtsam::Pose3(rotation_d, translation_d));
    poseGraphEngine->publishTrajectory();

    // perform bundle adjustment
    if (lc_res.second > settings->lcNormThreshold) {// && lc_res.second < 0.3) {
      static_cast<ITMPose3GraphEngine*>(poseGraphEngine)->addPose3Prior(
          lc_res.first * trackingState->pose_d->GetInvM());
      poseGraphEngine->performOptimization();
    }
//    poseGraphEngine->printGraph();
  }

  // publish ITM tracker pose
  PublishROSPoseMsg();

  // fusion
  visualization_msgs::Marker swappedOutMeshMsg;
  if (fusionActive) denseMapper->ProcessFrame(view, trackingState, scene, renderState_live, &swappedOutMeshMsg, settings->deltaTime, settings->shouldFuse);
  std::cout << "Should fuse set to: " << settings->shouldFuse << std::endl;

//  Vector4f * blabla = trackingState->pointCloud->inactive_locations->GetData(MEMORYDEVICE_CUDA);
//  //  visualizePcl(blabla, trackingState->pointCloud->inactive_locations->noDims.x * trackingState->pointCloud->inactive_locations->noDims.y);
//    std::cout << "balsdasldsa: " << std::endl;
//    Vector4f pix;
//    for (int i = 0; i < 120000; ++i) {
//  #ifndef COMPILE_WITHOUT_CUDA
//      ITMSafeCall(cudaMemcpy(&pix, &blabla[i], sizeof(Vector4f), cudaMemcpyDeviceToHost));
//  #else
//      pix = blabla[i];
//  #endif
//      if (pix.w > 0) {
//        std::cout << "pix: " << (float)pix.x << ", " << (float)pix.y << ", " << (float)pix.z << std::endl;
//      }
//    }

  // raycast to renderState_live for tracking and free visualisation
  trackingController->Prepare(trackingState, view, renderState_live);

  // ROS
  if (publishROSImage) {
    if (rvizPose != NULL) {
      PublishROSImageMsg(ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME, rvizPose, &view->calib->intrinsics_d);
    } else {
      PublishROSImageMsg(ITMMainEngine::InfiniTAM_IMAGE_SCENERAYCAST);
    }
  }
}

// Publish ITM ROS pose message
void ITMMainEngine::PublishROSPoseMsg() {
//  std::cout << "fail0" << std::endl;
//  std::cout << "fail1" << std::endl;
  if (clockAvailable) {
    ITMPoseMsg.header.stamp = clockMsg.clock;
  } else {
    ITMPoseMsg.header.stamp = ros::Time::now();
  }

  Matrix4f pose_imu = view->calib->trafo_rgb_to_imu.calib * view->calib->trafo_rgb_to_depth.calib_inv * trackingState->pose_d->GetM();
//    ITMPose itm_pose(pose_imu);
//    itm_pose.Coerce();
//    pose_imu = itm_pose.GetM();
  Vector3f t_inv = pose_imu.getRot().t() * (-1.0 * pose_imu.getTrans());
  ITMPoseMsg.transform.translation.x = t_inv.x;
  ITMPoseMsg.transform.translation.y = t_inv.y;
  ITMPoseMsg.transform.translation.z = t_inv.z;
  Matrix3f rot_imu = pose_imu.getRot();
//  std::cout << "fail2" << std::endl;
  Eigen::AngleAxisd aa(rot_imu.toEigen().matrix().cast<double>());    // RotationMatrix to AxisAngle
  MPD R(aa.toRotationMatrix());
//    MPD R(pose_imu.m00, pose_imu.m10, pose_imu.m20,
//          pose_imu.m01, pose_imu.m11, pose_imu.m21,
//          pose_imu.m02, pose_imu.m12, pose_imu.m22);
//  std::cout << "fail3" << std::endl;
  QPD q(R);
//  std::cout << "fail4" << std::endl;
  ITMPoseMsg.transform.rotation.x = q.x(); // JPL form
  ITMPoseMsg.transform.rotation.y = q.y();
  ITMPoseMsg.transform.rotation.z = q.z();
  ITMPoseMsg.transform.rotation.w = q.w();

  pubITMPose.publish(ITMPoseMsg);
  ITMPoseMsg.header.stamp = ros::Time::now(); // just a hack to ensure TF tree is formed. Since, ITM_global <-> IMU is published with clock stamps but ITM_global <-> optitrack is published with ros::Time::now()
  pubITMTf.sendTransform(ITMPoseMsg);
//  std::cout << "fail5" << std::endl;
}

// Publish ITM ROS Image message
void ITMMainEngine::PublishROSImageMsg(ITMMainEngine::GetImageType type, ITMPose *viewpoint_pose, ITMIntrinsics *viewpoint_intrinsics) {
  ITMUChar4Image *image = new ITMUChar4Image(view->depth->noDims, true, settings->deviceType == ITMLibSettings::DEVICE_CUDA);
  GetImage(image, type, viewpoint_pose, viewpoint_intrinsics);
  cv::Mat cv_image(image->noDims.height, image->noDims.width, CV_8UC3);

  Vector4u *imData = image->GetData(MEMORYDEVICE_CPU);
  uchar *cvImData = cv_image.data;
  for (int x = 0; x < image->noDims.width; ++x) {
    for (int y = 0; y < image->noDims.height; ++y) {
      cvImData[cv_image.channels()*(cv_image.cols*y + x) + 0] = imData[cv_image.cols*y + x].b;
      cvImData[cv_image.channels()*(cv_image.cols*y + x) + 1] = imData[cv_image.cols*y + x].g;
      cvImData[cv_image.channels()*(cv_image.cols*y + x) + 2] = imData[cv_image.cols*y + x].r;
    }
  }
  delete image;
  ITMImgMsg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", cv_image).toImageMsg();
  pubITMImage.publish(ITMImgMsg);
}

Vector2i ITMMainEngine::GetImageSize(void) const
{
	return renderState_live->raycastImage->noDims;
}

void ITMMainEngine::GetImage(ITMUChar4Image *out, GetImageType getImageType, ITMPose *pose, ITMIntrinsics *intrinsics)
{
	if (view == NULL) return;

	out->Clear();

	switch (getImageType)
	{
	case ITMMainEngine::InfiniTAM_IMAGE_ORIGINAL_RGB:
		out->ChangeDims(view->rgb->noDims);
		if (settings->deviceType == ITMLibSettings::DEVICE_CUDA) 
			out->SetFrom(view->rgb, ORUtils::MemoryBlock<Vector4u>::CUDA_TO_CPU);
		else out->SetFrom(view->rgb, ORUtils::MemoryBlock<Vector4u>::CPU_TO_CPU);
		break;
	case ITMMainEngine::InfiniTAM_IMAGE_ORIGINAL_DEPTH:
		out->ChangeDims(view->depth->noDims);
		if (settings->trackerType==ITMLib::Objects::ITMLibSettings::TRACKER_WICP)
		{
			if (settings->deviceType == ITMLibSettings::DEVICE_CUDA) view->depthUncertainty->UpdateHostFromDevice();
			ITMVisualisationEngine<ITMVoxel, ITMVoxelIndex>::WeightToUchar4(out, view->depthUncertainty);
		}
		else
		{
			if (settings->deviceType == ITMLibSettings::DEVICE_CUDA) view->depth->UpdateHostFromDevice();
			ITMVisualisationEngine<ITMVoxel, ITMVoxelIndex>::DepthToUchar4(
			    out, view->depth, Vector2f(settings->sceneParams.viewFrustum_min,
			                               settings->sceneParams.viewFrustum_max));
		}

		break;
	case ITMMainEngine::InfiniTAM_IMAGE_ORIGINAL_DEPTH_WITH_RGB:
    out->ChangeDims(view->depth->noDims);
    if (settings->deviceType == ITMLibSettings::DEVICE_CUDA)
      out->SetFrom(view->rgb_d, ORUtils::MemoryBlock<Vector4u>::CUDA_TO_CPU);
    else out->SetFrom(view->rgb_d, ORUtils::MemoryBlock<Vector4u>::CPU_TO_CPU);
    break;
	case ITMMainEngine::InfiniTAM_IMAGE_SCENERAYCAST:
	{
		ORUtils::Image<Vector4u> *srcImage = renderState_live->raycastImage;
		out->ChangeDims(srcImage->noDims);
		if (settings->deviceType == ITMLibSettings::DEVICE_CUDA)
			out->SetFrom(srcImage, ORUtils::MemoryBlock<Vector4u>::CUDA_TO_CPU);
		else out->SetFrom(srcImage, ORUtils::MemoryBlock<Vector4u>::CPU_TO_CPU);	
		break;
	}
	case ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_SHADED:
	case ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME:
	case ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_NORMAL:
	{
		IITMVisualisationEngine::RenderImageType type = IITMVisualisationEngine::RENDER_SHADED_GREYSCALE;
		if (getImageType == ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME) type = IITMVisualisationEngine::RENDER_COLOUR_FROM_VOLUME;
		else if (getImageType == ITMMainEngine::InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_NORMAL) type = IITMVisualisationEngine::RENDER_COLOUR_FROM_NORMAL;
		if (renderState_freeview == NULL) renderState_freeview = visualisationEngine->CreateRenderState(out->noDims, sdkGetTimerValue(&main_timer));

		visualisationEngine->FindVisibleBlocks(pose, intrinsics, renderState_freeview);
		visualisationEngine->CreateExpectedDepths(pose, intrinsics, renderState_freeview);
		visualisationEngine->RenderImage(pose, intrinsics, trackingState, renderState_freeview, renderState_freeview->raycastImage, settings->deltaTime, type);
//    Vector4f * blabla = renderState_freeview->inactiveRaycastResult->GetData(MEMORYDEVICE_CUDA);
//    std::cout << "balsdasldsa: " << std::endl;
//    Vector4f pix;
//    for (int i = 0; i < 120000; ++i) {
//  #ifndef COMPILE_WITHOUT_CUDA
//      ITMSafeCall(cudaMemcpy(&pix, &blabla[i], sizeof(Vector4f), cudaMemcpyDeviceToHost));
//  #else
//      pix = blabla[i];
//  #endif
//      if (pix.w > 0) {
//        std::cout << "pix: " << pix << std::endl;
//      }
//    }

		if (settings->deviceType == ITMLibSettings::DEVICE_CUDA)
			out->SetFrom(renderState_freeview->raycastImage, ORUtils::MemoryBlock<Vector4u>::CUDA_TO_CPU);
		else out->SetFrom(renderState_freeview->raycastImage, ORUtils::MemoryBlock<Vector4u>::CPU_TO_CPU);
		break;
	}
	case ITMMainEngine::InfiniTAM_IMAGE_UNKNOWN:
		break;
	};
}

void ITMMainEngine::turnOnIntegration() { fusionActive = true; }
void ITMMainEngine::turnOffIntegration() { fusionActive = false; }
void ITMMainEngine::turnOnMainProcessing() { mainProcessingActive = true; }
void ITMMainEngine::turnOffMainProcessing() { mainProcessingActive = false; }


// PCL Visualization
void ITMMainEngine::visualizePcl(const Vector4f* pcl, const int cloudSize) {

  pcl_cloud_pointer->clear();

  pcl::PointXYZRGB pc_point;
  Vector4f point;
  for (int i = 0; i < cloudSize; ++i){
#ifndef COMPILE_WITHOUT_CUDA
    ITMSafeCall(cudaMemcpy(&point, &pcl[i], sizeof(Vector4f), cudaMemcpyDeviceToHost));
#else
    point = pcl[i];
#endif
    if (point.w > 0) {
      pc_point.x = point.x;
      pc_point.y = point.y;
      pc_point.z = point.z;

      pc_point.r = 255;
      pc_point.g = 0;
      pc_point.b = 0;

      pcl_cloud_pointer->push_back(pc_point);
    }
  }

  pc_viewer.updatePointCloud(pcl_cloud_pointer, "cloud");

  pcl_render_stop = false;
  boost::thread t(boost::bind(&ITMMainEngine::pcl_render_loop, this));
  if (std::cin.get() == '\n') {
    std::cout << "Pressed ENTER" << std::endl;
    pcl_render_stop = true;
    std::cout << "waiting to join...." << std::endl;
    t.join();
  }
}

void ITMMainEngine::pcl_render_loop() {
  std::cout << "stop flag: " << pcl_render_stop << std::endl;
  while (!pcl_render_stop) {
    std::cout << "SPINNING......." << std::endl;
    pc_viewer.spinOnce (100);
  }
  pc_viewer.removeAllShapes();
}

int ITMMainEngine::ToCvImage(cv::Mat *image_out, const ITMUChar4Image *image_in) const
{
  const Vector4u *rgb = image_in->GetData(MEMORYDEVICE_CPU);
  if (image_out->empty()) {
    image_out->create(cv::Size(image_in->noDims.width, image_in->noDims.height), CV_8UC3);
  } else {
    if (image_out->type() != CV_8UC3 || image_out->cols != image_in->noDims.width ||
        image_out->rows != image_in->noDims.height) {
      return -1;
    }
  }
  // RGB data
  uchar* rgb_pointer = (uchar*)image_out->data;
  for (int j = 0; j < image_in->noDims.height; ++j)
  {
    for (int i = 0; i < image_in->noDims.width; ++i)
    {
      rgb_pointer[j*3*image_in->noDims.width + 3*i] = rgb[j*image_in->noDims.width + i].b;
      rgb_pointer[j*3*image_in->noDims.width + 3*i +1] = rgb[j*image_in->noDims.width + i].g;
      rgb_pointer[j*3*image_in->noDims.width + 3*i + 2] = rgb[j*image_in->noDims.width + i].r;
    }
  }

  return 0;
}

int ITMMainEngine::ToCvImage(cv::Mat *image_out, const ITMFloatImage *image_in) const
{
  const float *in = image_in->GetData(MEMORYDEVICE_CPU);
  if (image_out->empty()) {
    image_out->create(cv::Size(image_in->noDims.width, image_in->noDims.height), CV_32FC1);
  } else {
    if (image_out->type() != CV_32FC1 || image_out->cols != image_in->noDims.width ||
        image_out->rows != image_in->noDims.height) {
      return -1;
    }
  }
  // data
  float* out_pointer = (float*)image_out->data;
  memcpy(out_pointer, in, image_in->noDims.width * image_in->noDims.height * sizeof(float));

  return 0;
}

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../ITMLib.h"
#include "../Utils/ITMLibSettings.h"
#include "../../Utils/NVTimer.h"
#include <opencv2/calib3d/calib3d.hpp>

#include "kindr/rotations/RotationEigen.hpp"
#include <ros/ros.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/Image.h>
#include <tf/transform_broadcaster.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

// PCL
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/transforms.h>

namespace rot = kindr::rotations::eigen_impl;

typedef rot::RotationQuaternionPD QPD;
typedef rot::RotationMatrixPD MPD;
typedef rot::RotationQuaternionPF QPF;
typedef rot::RotationMatrixPF MPF;

/** \mainpage
    This is the API reference documentation for InfiniTAM. For a general
    overview additional documentation can be found in the included Technical
    Report.

    For use of ITMLib in your own project, the class
    @ref ITMLib::Engine::ITMMainEngine should be the main interface and entry
    point to the library.
*/

namespace ITMLib
{
	namespace Engine
	{
		/** \brief
		    Main engine, that instantiates all the other engines and
		    provides a simplified interface to them.

		    This class is the main entry point to the ITMLib library
		    and basically performs the whole KinectFusion algorithm.
		    It stores the latest image internally, as well as the 3D
		    world model and additionally it keeps track of the camera
		    pose.

		    The intended use is as follows:
		    -# Create an ITMMainEngine specifying the internal settings,
		       camera parameters and image sizes
		    -# Get the pointer to the internally stored images with
		       @ref GetView() and write new image information to that
		       memory
		    -# Call the method @ref ProcessFrame() to track the camera
		       and integrate the new information into the world model
		    -# Optionally access the rendered reconstruction or another
		       image for visualisation using @ref GetImage()
		    -# Iterate the above three steps for each image in the
		       sequence

		    To access the internal information, look at the member
		    variables @ref trackingState and @ref scene.
		*/
		class ITMMainEngine
		{
		private:
			const ITMLibSettings *settings;

			bool fusionActive, mainProcessingActive;

			ITMLowLevelEngine *lowLevelEngine;
			IITMVisualisationEngine *visualisationEngine;
			// Color Palette for visualization engine
			ITMPalette *viz_pal;

			ITMMeshingEngine<ITMVoxel, ITMVoxelIndex> *meshingEngine;
			ITMMesh *mesh;

			ITMViewBuilder *viewBuilder;		
			ITMDenseMapper<ITMVoxel,ITMVoxelIndex> *denseMapper;
			ITMTrackingController *trackingController;

			ITMTracker *tracker;
			ITMPoseGraphEngine<gtsam::Pose3> *poseGraphEngine;
			ITMLoopClosureDetection *loopClosureDetector;
			ITMIMUCalibrator *imuCalibrator;

			// Place recognition
			cv::Mat rgb_image, grayscale_image;
			ITMPlaceDetector<BriefVocabulary, BriefLoopDetector, FBrief::TDescriptor> *placeDetector;
			BriefExtractor *featureExtractor;

			ITMView *view;
			ITMTrackingState *trackingState;

			ITMScene<ITMVoxel, ITMVoxelIndex> *scene;
			ITMRenderState *renderState_live;
			ITMRenderState *renderState_freeview;

			// ROS
			ros::NodeHandle nh;
			ros::Publisher pubITMPose, pubITMImage;
			tf::TransformBroadcaster pubITMTf;
			ros::Subscriber subClock;
			geometry_msgs::TransformStamped ITMPoseMsg;
			sensor_msgs::ImagePtr ITMImgMsg;
			rosgraph_msgs::Clock clockMsg;
			bool clockAvailable = false;
			// Publish ITM ROS pose message
			void PublishROSPoseMsg();
			// Clock subscriber callback
			void clockCallback(const rosgraph_msgs::ClockConstPtr & clock_msg);
			// PCL
      bool pcl_render_stop = false;
      pcl::visualization::PCLVisualizer pc_viewer;
      pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcl_cloud_pointer;
      void visualizePcl(const Vector4f* pcl, const int cloudSize);
      void pcl_render_loop();

		public:
			enum GetImageType
			{
				InfiniTAM_IMAGE_ORIGINAL_RGB,
				InfiniTAM_IMAGE_ORIGINAL_DEPTH,
				InfiniTAM_IMAGE_ORIGINAL_DEPTH_WITH_RGB,
				InfiniTAM_IMAGE_SCENERAYCAST,
				InfiniTAM_IMAGE_FREECAMERA_SHADED,
				InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME,
				InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_NORMAL,
				InfiniTAM_IMAGE_UNKNOWN
			};

			// timer to keep track of voxel update times
            StopWatchInterface* main_timer;

			/// Gives access to the current input frame
			ITMView* GetView() { return view; }

			/// Gives access to the current camera pose and additional tracking information
			ITMTrackingState* GetTrackingState(void) { return trackingState; }

			/// Gives access to the internal world representation
			ITMScene<ITMVoxel, ITMVoxelIndex>* GetScene(void) { return scene; }

			/// Process a frame with rgb and depth images and optionally a corresponding imu measurement
			void ProcessFrame(ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage, ITMPose *rvizPose=NULL, bool publishROSImage = true);
			void ProcessFrame(ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage, ITMIMUMeasurement *imuMeasurement, ITMPose *rvizPose=NULL, bool publishROSImage = true);
			void ProcessFrame(ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage, ITMOdometryMeasurement *odomMeasurement, ITMPose *rvizPose=NULL, bool publishROSImage = true);

			// Gives access to the data structure used internally to store any created meshes
			ITMMesh* GetMesh(void) { return mesh; }

			/// Update the internally stored mesh data structure and return a pointer to it
			ITMMesh* UpdateMesh(void);

			/// Extracts a mesh from the current scene and saves it to the obj file specified by the file name
			void SaveSceneToMesh(const char *objFileName);

			// saves pose graph
			void SaveGraph(const char *graphFileName);

      // Publish ITM ROS Image message
      void PublishROSImageMsg(ITMMainEngine::GetImageType type, ITMPose *viewpoint_pose = NULL,
                              ITMIntrinsics *viewpoint_intrinsics = NULL);

			/// Get a result image as output
			Vector2i GetImageSize(void) const;

			void GetImage(ITMUChar4Image *out, GetImageType getImageType, ITMPose *pose = NULL, ITMIntrinsics *intrinsics = NULL);

			/// switch for turning intergration on/off
			void turnOnIntegration();
			void turnOffIntegration();

			/// switch for turning main processing on/off
			void turnOnMainProcessing();
			void turnOffMainProcessing();

			int ToCvImage(cv::Mat *image_out, const ITMUChar4Image *image_in) const;
      int ToCvImage(cv::Mat *image_out, const ITMFloatImage *image_in) const;

			/** \brief Constructor
			    Ommitting a separate image size for the depth images
			    will assume same resolution as for the RGB images.
			*/
			ITMMainEngine(const ITMLibSettings *settings, const ITMRGBDCalib *calib, Vector2i imgSize_rgb, Vector2i imgSize_d = Vector2i(-1,-1));
			~ITMMainEngine();
		};
	}
}


/*
 * ITMLoopClosureDetection.cpp
 *
 *  Created on: May 13, 2016
 *      Author: anurag
 */

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMLoopClosureDetection.h"
#include "../../ORUtils/Cholesky.h"

#include <math.h>

using namespace ITMLib::Engine;

ITMLoopClosureDetection::ITMLoopClosureDetection(Vector2i imgSize, TrackerIterationType *trackingRegime, int noHierarchyLevels, int noICPRunTillLevel, float distThresh,
  float terminationThreshold, ITMLibSettings::DepthTrackerType tracker_type, bool visualize_lc, const ITMLowLevelEngine *lowLevelEngine, MemoryDeviceType memoryType)
    : pc_viewer("LC visualizer"), scene_cloud_pointer(new pcl::PointCloud<pcl::PointXYZRGB>),
      inactive_scene_cloud_pointer(new pcl::PointCloud<pcl::PointXYZRGB>),
      match_cloud_pointer(new pcl::PointCloud<pcl::PointXYZRGB>),
      itm_pal(MEMORYDEVICE_CPU)
{
  inactiveSceneHierarchy = new ITMImageHierarchy<ITMSceneHierarchyLevel>(imgSize, trackingRegime, noHierarchyLevels, memoryType, true);
  sceneHierarchy = new ITMImageHierarchy<ITMSceneHierarchyLevel>(imgSize, trackingRegime, noHierarchyLevels, memoryType, true);

	this->noIterationsPerLevel = new int[noHierarchyLevels];
	this->distThresh = new float[noHierarchyLevels];
	
	this->noIterationsPerLevel[0] = 2; //TODO -> make parameter
	for (int levelId = 1; levelId < noHierarchyLevels; levelId++)
	{
		noIterationsPerLevel[levelId] = noIterationsPerLevel[levelId - 1] + 2;
	}
	this->noIterationsPerLevel[0] = 10; //TODO -> make parameter

  float distThreshStep = distThresh / noHierarchyLevels;
  this->distThresh[noHierarchyLevels - 1] = distThresh;
  for (int levelId = noHierarchyLevels - 2; levelId >= 0; levelId--)
    this->distThresh[levelId] = this->distThresh[levelId + 1] - distThreshStep;

  this->lowLevelEngine = lowLevelEngine;

  this->noICPLevel = noICPRunTillLevel;

  this->terminationThreshold = terminationThreshold;

  this->memory_type = memoryType;

  type = tracker_type;

  // PCL viewer
  if (visualize_lc) {
    itm_pal.SetPaletteType(ITMPalette::False_color_palette4);
    pc_viewer.setBackgroundColor(0, 0, 0);
    pc_viewer.addPointCloud<pcl::PointXYZRGB>(scene_cloud_pointer, "scene cloud");
    pc_viewer.setPointCloudRenderingProperties(
        pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "scene cloud");
    pc_viewer.addPointCloud<pcl::PointXYZRGB>(inactive_scene_cloud_pointer, "inactive scene cloud");
    pc_viewer.setPointCloudRenderingProperties(
        pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "inactive scene cloud");
    pc_viewer.addPointCloud<pcl::PointXYZRGB>(match_cloud_pointer, "alignment heatmap");
    pc_viewer.setPointCloudRenderingProperties(
    pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "alignment heatmap");
  }
}

ITMLoopClosureDetection::~ITMLoopClosureDetection(void)
{
  delete this->inactiveSceneHierarchy;
  delete this->sceneHierarchy;

  delete[] this->noIterationsPerLevel;
  delete[] this->distThresh;
}

void ITMLoopClosureDetection::SetEvaluationData(ITMTrackingState *trackingState, const ITMView *view)
{
  this->trackingState = trackingState;
  this->view = view;

  sceneHierarchy->levels[0]->intrinsics = view->calib->intrinsics_d.projectionParamsSimple.all;
  inactiveSceneHierarchy->levels[0]->intrinsics = view->calib->intrinsics_d.projectionParamsSimple.all;

  // the image hierarchy allows pointers to external data at level 0
  inactiveSceneHierarchy->levels[0]->pointsMap = trackingState->pointCloud->filtered_inactive_locations;
//  lowLevelEngine->FilterInactiveMap(trackingState->pointCloud->filtered_inactive_locations, trackingState->pointCloud->inactive_locations, trackingState->pointCloud->inactive_update_count, (short)15);
  inactiveSceneHierarchy->levels[0]->normalsMap = trackingState->pointCloud->inactive_colours;
  sceneHierarchy->levels[0]->pointsMap = trackingState->pointCloud->locations;
  sceneHierarchy->levels[0]->normalsMap = trackingState->pointCloud->colours;

  scenePose = trackingState->pose_pointCloud->GetM();
  sceneInvPose = trackingState->pose_pointCloud->GetInvM();
}

void ITMLoopClosureDetection::PrepareForEvaluation()
{
  for (int i = 1; i < inactiveSceneHierarchy->noLevels; i++)
  {
    ITMSceneHierarchyLevel *currentLevelInactiveScene = inactiveSceneHierarchy->levels[i], *previousLevelInactiveScene = inactiveSceneHierarchy->levels[i - 1];
    lowLevelEngine->FilterSubsampleWithHoles(currentLevelInactiveScene->pointsMap, previousLevelInactiveScene->pointsMap);
//    lowLevelEngine->FilterSubsampleWithHoles(currentLevelInactiveScene->normalsMap, previousLevelInactiveScene->normalsMap);
    currentLevelInactiveScene->intrinsics = previousLevelInactiveScene->intrinsics * 0.5f;

    ITMSceneHierarchyLevel *currentLevelScene = sceneHierarchy->levels[i], *previousLevelScene = sceneHierarchy->levels[i - 1];
    //lowLevelEngine->FilterSubsampleWithHoles(currentLevelScene->pointsMap, previousLevelScene->pointsMap);
    //lowLevelEngine->FilterSubsampleWithHoles(currentLevelScene->normalsMap, previousLevelScene->normalsMap);
    currentLevelScene->intrinsics = previousLevelScene->intrinsics * 0.5f;
  }
}

void ITMLoopClosureDetection::SetEvaluationParams(int levelId)
{
  this->levelId = levelId;
  this->iterationType = inactiveSceneHierarchy->levels[levelId]->iterationType;
  this->sceneHierarchyLevel = sceneHierarchy->levels[0];
  this->inactiveSceneHierarchyLevel = inactiveSceneHierarchy->levels[levelId];
}

void ITMLoopClosureDetection::ComputeDelta(float *step, float *nabla, float *hessian, bool shortIteration) const
{
  for (int i = 0; i < 6; i++) step[i] = 0;

  if (shortIteration)
  {
    float smallHessian[3 * 3];
    for (int r = 0; r < 3; r++) for (int c = 0; c < 3; c++) smallHessian[r + c * 3] = hessian[r + c * 6];

    ORUtils::Cholesky cholA(smallHessian, 3);
    cholA.Backsub(step, nabla);
  }
  else
  {
    ORUtils::Cholesky cholA(hessian, 6);
    cholA.Backsub(step, nabla);
  }
}

bool ITMLoopClosureDetection::HasConverged(float *step) const
{
  float stepLength = 0.0f;
  for (int i = 0; i < 6; i++) stepLength += step[i] * step[i];

  if (sqrt(stepLength) / 6 < terminationThreshold) return true; //converged

  return false;
}

void ITMLoopClosureDetection::ApplyDelta(const Matrix4f & para_old, const float *delta, Matrix4f & para_new) const
{
  float step[6];

  switch (iterationType)
  {
  case TRACKER_ITERATION_ROTATION:
    step[0] = (float)(delta[0]); step[1] = (float)(delta[1]); step[2] = (float)(delta[2]);
    step[3] = 0.0f; step[4] = 0.0f; step[5] = 0.0f;
    break;
  case TRACKER_ITERATION_TRANSLATION:
    step[0] = 0.0f; step[1] = 0.0f; step[2] = 0.0f;
    step[3] = (float)(delta[0]); step[4] = (float)(delta[1]); step[5] = (float)(delta[2]);
    break;
  default:
  case TRACKER_ITERATION_BOTH:
    step[0] = (float)(delta[0]); step[1] = (float)(delta[1]); step[2] = (float)(delta[2]);
    step[3] = (float)(delta[3]); step[4] = (float)(delta[4]); step[5] = (float)(delta[5]);
    break;
  }

  Matrix4f Tinc;

  Tinc.m00 = 1.0f;    Tinc.m10 = step[2];   Tinc.m20 = -step[1];  Tinc.m30 = step[3];
  Tinc.m01 = -step[2];  Tinc.m11 = 1.0f;    Tinc.m21 = step[0];   Tinc.m31 = step[4];
  Tinc.m02 = step[1];   Tinc.m12 = -step[0];  Tinc.m22 = 1.0f;    Tinc.m32 = step[5];
  Tinc.m03 = 0.0f;    Tinc.m13 = 0.0f;    Tinc.m23 = 0.0f;    Tinc.m33 = 1.0f;

  para_new = Tinc * para_old;
}

std::pair<Matrix4f, float> ITMLoopClosureDetection::DetectLoopClosure(ITMTrackingState *trackingState, ITMRenderState *renderState, const ITMView *view)
{
  this->SetEvaluationData(trackingState, view);
  this->PrepareForEvaluation();
//  ITMPose *inactiveScenePose(trackingState->pose_d);
//  ITMPose inactiveScenePose(*(trackingState->pose_d));
  ITMPose inactiveScenePose;
//  std::cout << "start inactiv pose: " << inactiveScenePose.GetM() << std::endl;
  Matrix4f initPose = inactiveScenePose.GetM();

  float f_old = 1e10, f_new;  // error metric
  int noValidPoints_new;
  Vector4f* matches;

  float hessian_good[6 * 6], hessian_new[6 * 6], A[6 * 6];
  float nabla_good[6], nabla_new[6];
  float step[6];

  // Libpointmatcher
  if (type == ITMLibSettings::TRACKER_LPM) {
    this->SetEvaluationParams(0);
    Matrix4f approxInvPose = inactiveScenePose.GetInvM(); // T_S_S'

    // Pre-Visualization
    if (trackingState->visualize_lc) {
      Matrix4f identity;
      identity.setIdentity();
      std::vector<Matrix4f*> tf_chain{&identity};
      // visualize initial TF
      std::cout << "vizing initial tf" << std::endl;
      visualizeTracker(
          this->sceneHierarchyLevel->pointsMap, this->inactiveSceneHierarchyLevel->pointsMap,
          NULL, memory_type, tf_chain);
    }

    approxInvPose = this->getLPMICPTF(approxInvPose);
    inactiveScenePose.SetInvM(approxInvPose);
    inactiveScenePose.Coerce();
    f_new = 0.0;

    // Post-Visualization
    if (trackingState->visualize_lc) {
      Matrix4f inactive_pose = inactiveScenePose.GetInvM();
      std::vector<Matrix4f*> tf_chain{&inactive_pose};
      // visualize TF update
      std::cout << "vizing converged tf" << std::endl;
      visualizeTracker(
          this->sceneHierarchyLevel->pointsMap, this->inactiveSceneHierarchyLevel->pointsMap,
          NULL, memory_type, tf_chain, true, true);
    }
  } else {
    for (int levelId = inactiveSceneHierarchy->noLevels - 1; levelId >= noICPLevel; levelId--)
    {
      this->SetEvaluationParams(levelId);
      if (iterationType == TRACKER_ITERATION_NONE) continue;

      // T_g,k =  approxInvPose
      Matrix4f approxInvPose = inactiveScenePose.GetInvM();
      ITMPose lastKnownGoodPose(inactiveScenePose);
      f_old = 1e20f;
      float lambda = 1.0;

      // Libnabo
      Eigen::MatrixXf M;
      if (type == ITMLibSettings::TRACKER_NABO) {
        if (memory_type == MEMORYDEVICE_CPU) {
        //      delete nns;
          Eigen::MatrixXf M1 = ITMVectorToEigenMatrix(
              sceneHierarchy->levels[0]->pointsMap->GetData(MemoryDeviceType(memory_type)),
              sceneHierarchy->levels[0]->pointsMap->noDims);
          M = M1;
        } else {
          std::cout << "NABO tracker only supported for CPU architecture!" << std::endl;
          exit(1);
        }
      } else {
        Eigen::MatrixXf M1(1, 1);
        M = M1;
      }
      nns.reset(Nabo::NNSearchF::createKDTreeLinearHeap(M, M.rows()));
      //    nns = Nabo::NNSearchF::createKDTreeLinearHeap(M, M.rows());
      std::cout << "Tree Prepared......................................... " << nns->cloud.cols() << std::endl;

      // Hierarchical Libpointmatcher
      if (type == ITMLibSettings::TRACKER_LPM_HIERARCHY) {
        std::cout << "Level ID: " << levelId << std::endl;
        approxInvPose = this->getLPMICPTF(approxInvPose);
        inactiveScenePose.SetInvM(approxInvPose);
        inactiveScenePose.Coerce();
        approxInvPose = inactiveScenePose.GetInvM();
      } else {
        for (int iterNo = 0; iterNo < noIterationsPerLevel[levelId]; iterNo++)
        {
          std::cout << "LC [ Level ID, Iteration no ]: " << "[ " << levelId << ", "
              << iterNo << " ]" << std::endl;
          std::cout << "f_old: " << f_old << std::endl;
          // evaluate error function and gradients
          std::pair<Vector4f*, int> res = this->ComputeGandH(f_new, nabla_new, hessian_new, approxInvPose, trackingState->visualize_lc);
          matches = res.first;
          noValidPoints_new = res.second;
    //      std::cout << "num matches: " << noValidPoints_new << std::endl;

          if (trackingState->visualize_lc) {
            Matrix4f inactive_pose = inactiveScenePose.GetInvM();
            std::vector<Matrix4f*> tf_chain{&inactive_pose};
            // visualize matches
            visualizeTracker(this->sceneHierarchyLevel->pointsMap, this->inactiveSceneHierarchyLevel->pointsMap,
                             matches, memory_type, tf_chain);
          }

          // check if error increased. If so, revert
          if ((noValidPoints_new <= 0)||(f_new > f_old)) {
            std::cout << "bad f_new: " << f_new << std::endl;
            inactiveScenePose.SetFrom(&lastKnownGoodPose);
            approxInvPose = inactiveScenePose.GetInvM();
            lambda *= 10.0f;
          } else {
            std::cout << "good f_new: " << f_new << std::endl;
            lastKnownGoodPose.SetFrom(&inactiveScenePose);
            f_old = f_new;

            for (int i = 0; i < 6*6; ++i) hessian_good[i] = hessian_new[i] / noValidPoints_new;
            for (int i = 0; i < 6; ++i) nabla_good[i] = nabla_new[i] / noValidPoints_new;
            lambda /= 10.0f;
          }
          for (int i = 0; i < 6*6; ++i) A[i] = hessian_good[i];
          for (int i = 0; i < 6; ++i) A[i+i*6] *= 1.0f + lambda;

          // compute a new step and make sure we've got an SE3
          ComputeDelta(step, nabla_good, A, iterationType != TRACKER_ITERATION_BOTH);
          ApplyDelta(approxInvPose, step, approxInvPose);
          inactiveScenePose.SetInvM(approxInvPose);
          inactiveScenePose.Coerce();
          approxInvPose = inactiveScenePose.GetInvM();

    //      std::cout << "f_old: " << f_old << std::endl;
    //      std::cout << "f_new: " << f_new << std::endl;
    //      std::cout << "new inactiv pose: " << inactiveScenePose->GetM() << std::endl;

          // if step is small, assume it's going to decrease the error and finish
          bool converged = HasConverged(step);

          // Visualization
          if (trackingState->visualize_lc) {
            Matrix4f inactive_pose = inactiveScenePose.GetInvM();
            std::vector<Matrix4f*> tf_chain{&inactive_pose};
            // visualize TF update
            std::cout << "vizing updated tf" << std::endl;
            visualizeTracker(
                this->sceneHierarchyLevel->pointsMap, this->inactiveSceneHierarchyLevel->pointsMap,
                matches, memory_type, tf_chain, converged);
          }

          if (trackingState->visualize_lc) {
#ifndef COMPILE_WITHOUT_CUDA
            ITMSafeCall(cudaFreeHost(matches));
#else
            free(matches);
#endif
          }
          // if step is small, assume it's going to decrease the error and finish
          if (converged) break;
        } // iterations
      } // type
    } // level change
  } // tracker LPM

  // Loop Closure constraint plot
  std::cout << "inactiv pose: " << inactiveScenePose.GetM() << std::endl;

  float lc_norm = std::numeric_limits<float>::infinity();
  if (f_new < 1e-4) {
    Matrix4f lc_delta;
    lc_delta.setIdentity();
    lc_delta -= inactiveScenePose.GetM();
    lc_norm = lc_delta.getNorm();
    std::cout << "LC constraint: " << lc_norm << std::endl;
    gp_lc_dist.push_back(std::make_pair(sdkGetTimerValue(&(renderState->timer))/1000.0, lc_norm));
    // Un-comment to plot
    gp << "plot '-' with lines title 'LC constraint'\n";
    gp.send1d(gp_lc_dist);
  } else {
    std::cout << "didn't consider LC because error = " << f_new << std::endl;
  }

  return std::make_pair(inactiveScenePose.GetM(), lc_norm);
}


const Eigen::MatrixXf ITMLoopClosureDetection::ITMVectorToEigenMatrix(
    const Vector4f* vector, const Vector2i dim) {
  Eigen::MatrixXf m(3, dim.height * dim.width);
  Vector4f point;
  for (int i = 0; i < dim.width * dim.height; ++i) {
#ifndef COMPILE_WITHOUT_CUDA
    ITMSafeCall(cudaMemcpy(&point, &vector[i], sizeof(Vector4f), cudaMemcpyDeviceToHost));
#else
    point = vector[i];
#endif
    m(0, i) = point.x;
    m(1, i) = point.y;
    m(2, i) = point.z;
  }

  return m;
}

// Float4Image to PCL point cloud
void ITMLoopClosureDetection::Float4ImagetoPclPointCloud(
    const ITMFloat4Image* im, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, Vector3i color,
    int memory_type) {

  cloud->clear();
  const Vector4f* v = im->GetData(MemoryDeviceType(memory_type));
  Vector4f point;
  pcl::PointXYZRGB pc_point;
  for (int i = 0; i < im->noDims.width * im->noDims.height; ++i) {

#ifndef COMPILE_WITHOUT_CUDA
    ITMSafeCall(cudaMemcpy(&point, &v[i], sizeof(Vector4f), cudaMemcpyDeviceToHost));
#else
    point = v[i];
#endif

    pc_point.x = point.x;
    pc_point.y = point.y;
    pc_point.z = point.z;

    pc_point.r = color[0];
    pc_point.g = color[1];
    pc_point.b = color[2];

    cloud->push_back(pc_point);
  }
}


// Float4Image to PCL point cloud after TF chain applied
void ITMLoopClosureDetection::Float4ImagetoPclPointCloud(
    const ITMFloat4Image* im, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
    Vector3i color, int memory_type, std::vector<Matrix4f*>& tf_chain) {

  cloud->clear();
  const Vector4f* v = im->GetData(MemoryDeviceType(memory_type));
  Vector4f point;
  pcl::PointXYZRGB pc_point;
  for (int i = 0; i < im->noDims.width * im->noDims.height; ++i) {

#ifndef COMPILE_WITHOUT_CUDA
    ITMSafeCall(cudaMemcpy(&point, &v[i], sizeof(Vector4f),
                           cudaMemcpyDeviceToHost));
#else
    point = v[i];
#endif

    if (point.w > 0) {
      // apply transform chain
      for (std::vector<Matrix4f*>::iterator it = tf_chain.begin(); it != tf_chain.end(); ++it) {
        point = (**it) * point;
        point.w = 1.0;
      }

      pc_point.x = point.x;
      pc_point.y = point.y;
      pc_point.z = point.z;

      pc_point.r = color[0];
      pc_point.g = color[1];
      pc_point.b = color[2];
    }

    cloud->push_back(pc_point);
  }
}


// Draw ICP point matches
void ITMLoopClosureDetection::DrawPointMatches(
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, Vector4f* matches, Vector3i color, bool converged) {

  std::cout << "inactive scene cloud size: " << cloud->size() << " " << inactiveSceneHierarchyLevel->pointsMap->noDims.height * inactiveSceneHierarchyLevel->pointsMap->noDims.width << std::endl;
  int jump_size = std::max(1, static_cast<int>(cloud->size()/1000));
  double avg_error = 0;
  int num_matches = 0;
  int error = 0;
  double error_norm = 0;
  double max_error = 0.3;
  pcl::PointXYZRGB pc_point;
  // palette
  ITMPalette::color *pal_colors = itm_pal.GetColors();
  if (matches == NULL) std::cout << "No matches found!" << std::endl;
  for (int i = 0; i < cloud->size(); i+=1) {
//    std::cout << "vor" << std::endl;
    if (matches[i].w != 0.0) {
//      std::cout << "aft" << std::endl;
      pcl::PointXYZ m;
      m.x = matches[i].x;
      m.y = matches[i].y;
      m.z = matches[i].z;
      std::string l_string("line");
      l_string += std::to_string(i);
      if (!pcl::isFinite(cloud->points[i]) || !pcl::isFinite(m)) std::cout << "NOT FINITE POINT!!!!!!!!!!!" << std::endl;
      if (cloud->points[i].x == m.x && cloud->points[i].y == m.y && cloud->points[i].z == m.z) std::cout << "ZERO LENGTH LINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
      if (cloud->points[i].x != m.x || cloud->points[i].y != m.y || cloud->points[i].z != m.z) {
        if (!converged) {
          if (i%jump_size == 0) pc_viewer.addLine(cloud->points[i], m, color.r, color.g, color.b, l_string);
        } else {
          pc_point.x = m.x;
          pc_point.y = m.y;
          pc_point.z = m.z;

          // Heatmap
          error_norm = sqrt(pow(cloud->points[i].x-m.x, 2) + pow(cloud->points[i].y-m.y, 2) +
                       pow(cloud->points[i].z-m.z, 2));
          error = static_cast<int>((std::min(error_norm, max_error)) * 255/max_error);
          pc_point.r = pal_colors[error].rgbRed;
          pc_point.g = pal_colors[error].rgbGreen;
          pc_point.b = pal_colors[error].rgbBlue;

          match_cloud_pointer->push_back(pc_point);

          avg_error += error_norm;
          num_matches += 1;
        }
      }
    }
  }
  avg_error /= num_matches;
  std::cout << "avg alignment error: " << avg_error << std::endl;
  if (converged) pc_viewer.updatePointCloud(match_cloud_pointer, "alignment heatmap");
}


// Tracker TF Update Visualization
void ITMLoopClosureDetection::visualizeTracker(
    const ITMFloat4Image* scene, const ITMFloat4Image* inactive_scene,
    Vector4f* matches, int memory_type, std::vector<Matrix4f*>& tf_chain, bool converged,
    bool show_inactive) {

  // scene
  Float4ImagetoPclPointCloud(scene, scene_cloud_pointer, Vector3i(255, 0, 0), memory_type);
  pc_viewer.updatePointCloud(scene_cloud_pointer, "scene cloud");

  // current view
  Float4ImagetoPclPointCloud(inactive_scene, inactive_scene_cloud_pointer, Vector3i(0, 0, 255), memory_type, tf_chain);
  if (show_inactive) pc_viewer.updatePointCloud(inactive_scene_cloud_pointer, "inactive scene cloud");

  // matches
  if (matches != NULL) DrawPointMatches(inactive_scene_cloud_pointer, matches, Vector3i(255, 255, 255), true);

  if (!show_inactive) {
    inactive_scene_cloud_pointer->clear();
    pc_viewer.updatePointCloud(inactive_scene_cloud_pointer, "inactive scene cloud");
  }

  // Message
  if (converged) {
    std::string msg("msg0");
    pc_viewer.addText("Loop Closure ICP converged", 50, 50, 30, 1.0, 1.0, 1.0, msg);
  }

  pcl_render_stop = false;
  boost::thread t(boost::bind(&ITMLoopClosureDetection::pcl_render_loop, this));
  if (std::cin.get() == '\n') {
    std::cout << "Pressed ENTER" << std::endl;
    pcl_render_stop = true;
    std::cout << "waiting to join...." << std::endl;
    t.join();
    std::cout << "joined!!!" << std::endl;
  }
//  scene_cloud_pointer->clear();
//  pc_viewer.updatePointCloud(scene_cloud_pointer, "scene cloud");
}


// Tracker Matches Visualization
void ITMLoopClosureDetection::visualizeTracker(
    const ITMFloat4Image* scene, const ITMFloat4Image* inactive_scene,
    Vector4f* matches, int memory_type, std::vector<Matrix4f*>& tf_chain) {

//  pc_viewer.removeAllPointClouds();
  // scene
  Float4ImagetoPclPointCloud(scene, scene_cloud_pointer, Vector3i(255, 0, 0), memory_type);
  pc_viewer.updatePointCloud(scene_cloud_pointer, "scene cloud");

  // current view
  Float4ImagetoPclPointCloud(inactive_scene, inactive_scene_cloud_pointer,
                             Vector3i(0, 0, 255), memory_type, tf_chain);
  pc_viewer.updatePointCloud(inactive_scene_cloud_pointer, "inactive scene cloud");

  // matches
  if (matches != NULL) DrawPointMatches(inactive_scene_cloud_pointer, matches, Vector3i(255, 255, 255), false);

  pcl_render_stop = false;
  boost::thread t(boost::bind(&ITMLoopClosureDetection::pcl_render_loop, this));
  if (std::cin.get() == '\n') {
    std::cout << "Pressed ENTER" << std::endl;
    pcl_render_stop = true;
    std::cout << "waiting to join...." << std::endl;
    t.join();
    std::cout << "joined!!!" << std::endl;
  }
}

void ITMLoopClosureDetection::pcl_render_loop() {
  std::cout << "stop flag: " << pcl_render_stop << std::endl;
  while (!pcl_render_stop) {
    std::cout << "SPINNING......." << std::endl;
    pc_viewer.spinOnce (100);
  }
  pc_viewer.removeAllShapes();
  match_cloud_pointer->clear();
  pc_viewer.updatePointCloud(match_cloud_pointer, "alignment heatmap");
}



/*
 * ITMRelocalizationEngine.h
 *
 *  Created on: Jun 5, 2016
 *      Author: anurag
 */

#ifndef INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMRELOCALIZATIONENGINE_H_
#define INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMRELOCALIZATIONENGINE_H_

#pragma once

#include "../ITMLib.h"
#include "../Utils/ITMLibSettings.h"
#include <opencv2/viz/vizcore.hpp>
#include <opencv2/viz/types.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "../../Engine/ImageSourceEngine.h"
#include "../../Engine/IMUSourceEngine.h"
#include "../../Engine/OdometrySourceEngine.h"

#include "kindr/rotations/RotationEigen.hpp"
#include <ros/ros.h>
#include <geometry_msgs/TransformStamped.h>

// PCL
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h>

// Libpointmatcher
#include "pointmatcher/PointMatcher.h"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "../../Utils/gnuplot-iostream/gnuplot-iostream.h"

using namespace ITMLib::Objects;
typedef PointMatcher<float> PM;
typedef PM::DataPoints DP;

namespace ITMLib
{
  namespace Engine
  {
    class ITMRelocalizationEngine
    {
     private:
      enum GetImageType
      {
        InfiniTAM_IMAGE_ORIGINAL_RGB,
        InfiniTAM_IMAGE_ORIGINAL_DEPTH,
        InfiniTAM_IMAGE_ORIGINAL_DEPTH_WITH_RGB,
        InfiniTAM_IMAGE_SCENERAYCAST,
        InfiniTAM_IMAGE_FREECAMERA_SHADED,
        InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_VOLUME,
        InfiniTAM_IMAGE_FREECAMERA_COLOUR_FROM_NORMAL,
        InfiniTAM_IMAGE_UNKNOWN
      };

      typedef enum {
        //! Using Libpointmatcher for ICP (only CPU)
        TRACKER_LPM,
        //! TRACKER_LPM but operates on image hierarchy (only CPU)
        TRACKER_LPM_HIERARCHY,
      } DepthTrackerType;

      // T_g,k =  approxInvPose
      Matrix4f approxInvPose_;

      DepthTrackerType tracker_type_;
      const ITMLibSettings *settings_;
      ITMLowLevelEngine *lowLevelEngine_;

      std::string sceneMeshFilename_, initialPoseEstimateFilename_;

      ITMView *view_;
      ITMViewBuilder *viewBuilder_;
      ITMImageHierarchy<ITMTemplatedHierarchyLevel<ITMFloatImage> > *viewHierarchy_;
      ITMTemplatedHierarchyLevel<ITMFloatImage> *viewHierarchyLevel_;

      InfiniTAM::Engine::ImageSourceEngine *imageSource_;
      InfiniTAM::Engine::IMUSourceEngine *imuSource_;
      InfiniTAM::Engine::OdometrySourceEngine *odomSource_;
      ITMUChar4Image *inputRGBImage_; ITMShortImage *inputRawDepthImage_;
      ITMIMUMeasurement *inputIMUMeasurement_;
      ITMOdometryMeasurement *inputOdometryMeasurement_;

      // libnabo kd-tree
      boost::shared_ptr<Nabo::NNSearchF> nns_;

      // Libpointmatcher
      DP scene_lpm_;
      PM::ICP icp_;
      // Read Write Directory for LPM metadata
      std::string LPMBaseDir_;
      std::string LPMConfigFile_;

      int *noIterationsPerLevel_;
      int noICPLevel_;
      float terminationThreshold_;
      float *distThresh_;
      int levelId_;
      TrackerIterationType iterationType_;

      // PCL
      bool pcl_render_stop_ = false;
      pcl::visualization::PCLVisualizer pc_viewer_;
      pcl::PointCloud<pcl::PointXYZRGB>::Ptr scene_cloud_pointer_, current_view_cloud_pointer_;
      bool viz_icp_ = false; // Whether or not to run visualization routine

      void pcl_render_loop();
      void GetImage(ITMUChar4Image *out, GetImageType getImageType, ITMPose *pose = NULL, ITMIntrinsics *intrinsics = NULL);
      void PrepareForEvaluation();
      void SetEvaluationParams(int levelId);
      void SetEvaluationData();
      void filterSceneMesh(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud, const Matrix4f &pose, const Vector4f &intrinsics, const Vector2i &imgSize);
      bool readInitialPoseEstimate(const std::string &fileName, Matrix4f & dest);
      // Use libpointmatcher for ICP routine
      Matrix4f getLPMICPTF(Matrix4f& prevInvPose);

      // FloatImage to LPM Point cloud
      DP FloatImagetoLPMPointCloud(const ITMFloatImage* im, const Vector4f intrinsics);

      // Depth Map to PCL point cloud
      void FloatImagetoPclPointCloud(
          const ITMFloatImage* im, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
          const Vector4f intrinsics, Vector3i color, int memory_type, std::vector<Matrix4f*>& tf_chain);

      // Draw ICP point matches
      void DrawPointMatches(
          pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, Vector4f* matches, Vector3i color);

     public:
      // Tracker Matches Visualization
      void visualizeTracker(
          const ITMFloatImage* current_view, const Vector4f intrinsics,
          Vector4f* matches, int memory_type, std::vector<Matrix4f*>& tf_chain);

      // Tracker TF Update Visualization
      void visualizeTracker(
          const ITMFloatImage* current_view, const Vector4f intrinsics,
          int memory_type, std::vector<Matrix4f*>& tf_chain, bool converged);

      // Relocalize current frame against the mesh
      Matrix4f RelocalizeFrame();

      ITMRelocalizationEngine(std::string &sceneMeshFilename, const ITMLibSettings *settings, InfiniTAM::Engine::ImageSourceEngine *imageSource, InfiniTAM::Engine::IMUSourceEngine *imuSource, InfiniTAM::Engine::OdometrySourceEngine *odomSource, const ITMRGBDCalib *calib, Vector2i imgSize_rgb, Vector2i imgSize_d = Vector2i(-1,-1));
      ~ITMRelocalizationEngine();
    };

  }
}

#endif /* INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMRELOCALIZATIONENGINE_H_ */

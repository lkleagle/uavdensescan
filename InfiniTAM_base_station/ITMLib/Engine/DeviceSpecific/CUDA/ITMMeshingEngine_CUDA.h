// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../../ITMMeshingEngine.h"

namespace ITMLib
{
	namespace Engine
	{
		template<class TVoxel, class TIndex>
		class ITMMeshingEngine_CUDA : public ITMMeshingEngine < TVoxel, TIndex >
		{};

		template<class TVoxel>
		class ITMMeshingEngine_CUDA<TVoxel, ITMVoxelBlockHash> : public ITMMeshingEngine < TVoxel, ITMVoxelBlockHash >
		{
		private:
			unsigned int  *noTriangles_device, *noTransferTriangles_device;
			Vector4s *visibleBlockGlobalPos_device, *transferBlockGlobalPos_device;

		public:
			void MeshScene(ITMMesh *mesh, const ITMScene<TVoxel, ITMVoxelBlockHash> *scene);
			void MeshSwappedScene(ITMMesh *mesh, const ITMScene<TVoxel, ITMVoxelBlockHash> *scene, const int *entryIds, const int noTotalEntries);

			ITMMeshingEngine_CUDA(void);
			~ITMMeshingEngine_CUDA(void);
		};

		template<class TVoxel>
		class ITMMeshingEngine_CUDA<TVoxel, ITMPlainVoxelArray> : public ITMMeshingEngine < TVoxel, ITMPlainVoxelArray >
		{
		public:
			void MeshScene(ITMMesh *mesh, const ITMScene<TVoxel, ITMPlainVoxelArray> *scene);
			void MeshSwappedScene(ITMMesh *mesh, const ITMScene<TVoxel, ITMPlainVoxelArray> *scene, const int *entryIds, const int noTotalEntries);

			ITMMeshingEngine_CUDA(void);
			~ITMMeshingEngine_CUDA(void);
		};
	}
}

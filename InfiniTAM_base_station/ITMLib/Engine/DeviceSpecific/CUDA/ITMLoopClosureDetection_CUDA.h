/*
 * ITMLoopClosureDetection_CUDA.h
 *
 *  Created on: May 13, 2016
 *      Author: anurag
 */

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#ifndef ITMLOOPCLOSUREDETECTION_CUDA_H_
#define ITMLOOPCLOSUREDETECTION_CUDA_H_

#pragma once

#include "../../ITMLoopClosureDetection.h"

namespace ITMLib
{
	namespace Engine
	{
		class ITMLoopClosureDetection_CUDA : public ITMLoopClosureDetection
		{
		public:
			struct AccuCell;

		private:
			AccuCell *accu_host;
			AccuCell *accu_device;
			Vector4f* host_matches, *device_matches;

		protected:
			std::pair<Vector4f*, int> ComputeGandH(float &f, float *nabla, float *hessian, Matrix4f approxInvPose, bool visualize);

			// Libpointmatcher
      PM::ICP icp;

      // Use libpointmatcher for ICP routine
      Matrix4f getLPMICPTF(Matrix4f& prevInvPose);

      // Flaot4Image to LPM Point cloud
      DP Float4ImagetoLPMPointCloud(const ITMFloat4Image* im);

      // FlaotImage to LPM Point cloud
      DP FloatImagetoLPMPointCloud(const ITMFloatImage* im, const Vector4f intrinsics);

		public:
			ITMLoopClosureDetection_CUDA(Vector2i imgSize, TrackerIterationType *trackingRegime, int noHierarchyLevels, int noICPRunTillLevel, float distThresh,
				float terminationThreshold, ITMLibSettings::DepthTrackerType tracker_type, bool visualize_lc, std::string resourcesDir, const ITMLowLevelEngine *lowLevelEngine);
			~ITMLoopClosureDetection_CUDA(void);
		};
	}
}

#endif /* ITMLOOPCLOSUREDETECTION_CUDA_H_ */

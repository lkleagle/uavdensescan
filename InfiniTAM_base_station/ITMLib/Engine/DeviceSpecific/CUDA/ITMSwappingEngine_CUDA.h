// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../../ITMSwappingEngine.h"

namespace ITMLib
{
	namespace Engine
	{
		template<class TVoxel, class TIndex>
		class ITMSwappingEngine_CUDA : public ITMSwappingEngine < TVoxel, TIndex >
		{
		public:
			void IntegrateGlobalIntoLocal(ITMScene<TVoxel, TIndex> *scene, ITMTrackingState *trackingState, ITMRenderState *renderState, bool &should_fuse) {}
			void SaveToGlobalMemory(ITMScene<TVoxel, TIndex> *scene, ITMRenderState *renderState, visualization_msgs::Marker *meshMsg) {}
		};

		template<class TVoxel>
		class ITMSwappingEngine_CUDA<TVoxel, ITMVoxelBlockHash> : public ITMSwappingEngine < TVoxel, ITMVoxelBlockHash >
		{
		private:
			int *noNeededEntries_device, *noAllocatedVoxelEntries_device, *noRemappedInactivePoints_device;
			bool *swappedInDataIsOld_device;
			int LoadFromGlobalMemory(ITMScene<TVoxel, ITMVoxelBlockHash> *scene);

		public:
			void IntegrateGlobalIntoLocal(ITMScene<TVoxel, ITMVoxelBlockHash> *scene, const ITMView *view, ITMTrackingState *trackingState, ITMRenderState *renderState, bool &should_fuse);
			void SaveToGlobalMemory(ITMScene<TVoxel, ITMVoxelBlockHash> *scene, ITMRenderState *renderState, visualization_msgs::Marker *meshMsg);
			void remapInactiveLocations(const Vector4f* inactiveLocations, const short* inactiveNumUpdates, const float* inactiveSdf, Vector4f* remappedLocations, short* remappedInactiveNumUpdates, float* remappedSdf, int *noRemappedEntries, const Vector2i &imgSize, const Matrix4f &M_d, const Vector4f &projParams_d);

			ITMSwappingEngine_CUDA(void);
			~ITMSwappingEngine_CUDA(void);
		};
	}
}

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMSwappingEngine_CUDA.h"
#include "ITMCUDAUtils.h"
#include "../../DeviceAgnostic/ITMSwappingEngine.h"
#include "../../../Objects/ITMRenderState_VH.h"

using namespace ITMLib::Engine;

__global__ void buildListToSwapIn_device(int *neededEntryIDs, int *noNeededEntries, ITMHashSwapState *swapStates, int noTotalEntries);

template<class TVoxel>
__global__ void integrateOldIntoActiveData_device(TVoxel *localVBA, ITMHashSwapState *swapStates, TVoxel *syncedVoxelBlocks_local,
	int *neededEntryIDs_local, ITMHashEntry *hashTable, int maxW, const double update_time);

__global__ void remapInactiveLocations_device(const Vector4f* inactiveLocations, const short* inactiveNumUpdates, const float* inactiveReliability, Vector4f* remappedLocations, short* remappedInactiveNumUpdates, float* remappedReliability, int *noRemappedEntries, const Vector2i imgSize, const Matrix4f M_d, const Vector4f projParams_d);

template<class TVoxel>
__global__ void readSwappedInDataAsPointCloud_device(TVoxel *localVBA, TVoxel *syncedVoxelBlocks_local, Vector4f *inactiveLocations, short* inactiveNumUpdates, float *inactiveReliability,
	ITMHashSwapState *swapStates, int *neededEntryIDs_local, ITMHashEntry *hashTable, Matrix4f M_d, Vector4f projParams_d, Vector2i imgSize, float voxelSize,
	float alpha, float beta, int w_max, float mu, const double current_time, bool *is_old);

__global__ void buildListToSwapOut_device(int *neededEntryIDs, int *neededMeshEntryIDs, int *noNeededEntries, ITMHashSwapState *swapStates,
	ITMHashEntry *hashTable, uchar *entriesVisibleType, int noTotalEntries);

template<class TVoxel>
__global__ void cleanMemory_device(int *voxelAllocationList, int *noAllocatedVoxelEntries, ITMHashSwapState *swapStates,
	ITMHashEntry *hashTable, TVoxel *localVBA, int *neededEntryIDs_local, int noNeededEntries);

template<class TVoxel>
__global__ void moveActiveDataToTransferBuffer_device(TVoxel *syncedVoxelBlocks_local, bool *hasSyncedData_local,
	int *neededEntryIDs_local, ITMHashEntry *hashTable, TVoxel *localVBA);

template<class TVoxel>
ITMSwappingEngine_CUDA<TVoxel,ITMVoxelBlockHash>::ITMSwappingEngine_CUDA(void)
{
	ITMSafeCall(cudaMalloc((void**)&noAllocatedVoxelEntries_device, sizeof(int)));
	ITMSafeCall(cudaMalloc((void**)&noNeededEntries_device, sizeof(int)));
	ITMSafeCall(cudaMalloc((void**)&swappedInDataIsOld_device, sizeof(bool)));
//	ITMSafeCall(cudaMalloc((void**)&noRemappedInactivePoints_device, sizeof(int)));
	noRemappedInactivePoints_device = (int*)malloc(sizeof(int));
	this->inactiveMesh = new ITMMesh(MEMORYDEVICE_CUDA, ITMVoxel::hasColorInformation);
	this->meshingEngine = new ITMMeshingEngine_CUDA<ITMVoxel, ITMVoxelIndex>();
}

template<class TVoxel>
ITMSwappingEngine_CUDA<TVoxel,ITMVoxelBlockHash>::~ITMSwappingEngine_CUDA(void)
{
	ITMSafeCall(cudaFree(noAllocatedVoxelEntries_device));
	ITMSafeCall(cudaFree(noNeededEntries_device));
	ITMSafeCall(cudaFree(swappedInDataIsOld_device));
//	ITMSafeCall(cudaFree(noRemappedInactivePoints_device));
	free(noRemappedInactivePoints_device);
	if (this->inactiveMesh != NULL) delete this->inactiveMesh;
    if (this->meshingEngine != NULL) delete this->meshingEngine;
}

template<class TVoxel>
int ITMSwappingEngine_CUDA<TVoxel,ITMVoxelBlockHash>::LoadFromGlobalMemory(ITMScene<TVoxel,ITMVoxelBlockHash> *scene)
{
	ITMGlobalCache<TVoxel> *globalCache = scene->globalCache;

	ITMHashSwapState *swapStates = globalCache->GetSwapStates(true);

	TVoxel *syncedVoxelBlocks_local = globalCache->GetSyncedVoxelBlocks(true);
	bool *hasSyncedData_local = globalCache->GetHasSyncedData(true);
	int *neededEntryIDs_local = globalCache->GetNeededEntryIDs(true);

	TVoxel *syncedVoxelBlocks_global = globalCache->GetSyncedVoxelBlocks(false);
	bool *hasSyncedData_global = globalCache->GetHasSyncedData(false);
	int *neededEntryIDs_global = globalCache->GetNeededEntryIDs(false);

	dim3 blockSize(256);
	dim3 gridSize((int)ceil((float)scene->index.noTotalEntries / (float)blockSize.x));

	ITMSafeCall(cudaMemset(noNeededEntries_device, 0, sizeof(int)));

	buildListToSwapIn_device << <gridSize, blockSize >> >(neededEntryIDs_local, noNeededEntries_device, swapStates,
		scene->globalCache->noTotalEntries);

	int noNeededEntries;
	ITMSafeCall(cudaMemcpy(&noNeededEntries, noNeededEntries_device, sizeof(int), cudaMemcpyDeviceToHost));

	if (noNeededEntries > 0)
	{
		noNeededEntries = MIN(noNeededEntries, SDF_TRANSFER_BLOCK_NUM);
		ITMSafeCall(cudaMemcpy(neededEntryIDs_global, neededEntryIDs_local, sizeof(int) * noNeededEntries, cudaMemcpyDeviceToHost));

		memset(syncedVoxelBlocks_global, 0, noNeededEntries * SDF_BLOCK_SIZE3 * sizeof(TVoxel));
		memset(hasSyncedData_global, 0, noNeededEntries * sizeof(bool));
		for (int i = 0; i < noNeededEntries; i++)
		{
			int entryId = neededEntryIDs_global[i];

			if (globalCache->HasStoredData(entryId))
			{
				hasSyncedData_global[i] = true;
				memcpy(syncedVoxelBlocks_global + i * SDF_BLOCK_SIZE3, globalCache->GetStoredVoxelBlock(entryId), SDF_BLOCK_SIZE3 * sizeof(TVoxel));
			}
		}

		ITMSafeCall(cudaMemcpy(hasSyncedData_local, hasSyncedData_global, sizeof(bool) * noNeededEntries, cudaMemcpyHostToDevice));
		ITMSafeCall(cudaMemcpy(syncedVoxelBlocks_local, syncedVoxelBlocks_global, sizeof(TVoxel) *SDF_BLOCK_SIZE3 * noNeededEntries, cudaMemcpyHostToDevice));
	}

	return noNeededEntries;
}

template<class TVoxel>
void ITMSwappingEngine_CUDA<TVoxel, ITMVoxelBlockHash>::IntegrateGlobalIntoLocal(ITMScene<TVoxel, ITMVoxelBlockHash> *scene, const ITMView *view, ITMTrackingState *trackingState, ITMRenderState *renderState, bool &should_fuse)
{
	ITMGlobalCache<TVoxel> *globalCache = scene->globalCache;

	ITMHashEntry *hashTable = scene->index.GetEntries();

	ITMHashSwapState *swapStates = globalCache->GetSwapStates(true);

	TVoxel *syncedVoxelBlocks_local = globalCache->GetSyncedVoxelBlocks(true);
	int *neededEntryIDs_local = globalCache->GetNeededEntryIDs(true);

	TVoxel *localVBA = scene->localVBA.GetVoxelBlocks();

	// GPU parallelized version for remapping
//	Vector4f * inactiveLocations = trackingState->pointCloud->inactive_locations->GetData(MEMORYDEVICE_CUDA);
//	short * inactiveNumUpdates = trackingState->pointCloud->inactive_update_count->GetData(MEMORYDEVICE_CUDA);
//	float * inactiveReliability = trackingState->pointCloud->inactive_reliability->GetData(MEMORYDEVICE_CUDA);
//	Vector4f * remappedInactiveLocations = trackingState->pointCloud->remapped_inactive_locations->GetData(MEMORYDEVICE_CUDA);
//	short * remappedInactiveNumUpdates = trackingState->pointCloud->remapped_inactive_update_count->GetData(MEMORYDEVICE_CUDA);
//	float * remappedInactiveReliability = trackingState->pointCloud->remapped_inactive_reliability->GetData(MEMORYDEVICE_CUDA);

	// CPU non-parellized version for remapping
	trackingState->pointCloud->UpdateHostFromDevice();
  Vector4f * inactiveLocations = trackingState->pointCloud->inactive_locations->GetData(MEMORYDEVICE_CPU);
  short * inactiveNumUpdates = trackingState->pointCloud->inactive_update_count->GetData(MEMORYDEVICE_CPU);
  float * inactiveReliability = trackingState->pointCloud->inactive_reliability->GetData(MEMORYDEVICE_CPU);
  Vector4f * remappedInactiveLocations = trackingState->pointCloud->remapped_inactive_locations->GetData(MEMORYDEVICE_CPU);
  short * remappedInactiveNumUpdates = trackingState->pointCloud->remapped_inactive_update_count->GetData(MEMORYDEVICE_CPU);
  float * remappedInactiveReliability = trackingState->pointCloud->remapped_inactive_reliability->GetData(MEMORYDEVICE_CPU);


	Matrix4f M_d = trackingState->pose_d->GetM();
	Vector4f projParams_d = view->calib->intrinsics_d.projectionParamsSimple.all;
	Vector2i imgSize = view->depth->noDims;
	float voxelSize = scene->sceneParams->voxelSize;
	float mu = scene->sceneParams->mu;
	int w_max = scene->sceneParams->maxW;
	float alpha = scene->sceneParams->alpha;
	float beta = scene->sceneParams->beta;

	// GPU parallelized version for remapping
//	ITMSafeCall(cudaMemset(remappedInactiveLocations, 0, trackingState->pointCloud->inactive_locations->noDims.x * trackingState->pointCloud->inactive_locations->noDims.y * sizeof(Vector4f)));
//  ITMSafeCall(cudaMemset(remappedInactiveNumUpdates, 0, trackingState->pointCloud->inactive_update_count->noDims.x * trackingState->pointCloud->inactive_update_count->noDims.y * sizeof(short)));
//  ITMSafeCall(cudaMemset(remappedInactiveReliability, 0, trackingState->pointCloud->inactive_reliability->noDims.x * trackingState->pointCloud->inactive_reliability->noDims.y * sizeof(float)));
//  ITMSafeCall(cudaMemset(noRemappedInactivePoints_device, 0, sizeof(int)));
	dim3 blockSize = dim3(16, 16);
	dim3 gridSize = dim3((int)ceil((float)trackingState->pointCloud->inactive_locations->noDims.x / (float)blockSize.x), (int)ceil((float)trackingState->pointCloud->inactive_locations->noDims.y / (float)blockSize.y));
//	remapInactiveLocations_device << <gridSize, blockSize >> >(inactiveLocations, inactiveNumUpdates, inactiveReliability, remappedInactiveLocations, remappedInactiveNumUpdates, remappedInactiveReliability, noRemappedInactivePoints_device, trackingState->pointCloud->inactive_locations->noDims, M_d, projParams_d);
//	ITMSafeCall(cudaMemcpy(inactiveLocations, remappedInactiveLocations, trackingState->pointCloud->inactive_locations->noDims.x * trackingState->pointCloud->inactive_locations->noDims.y * sizeof(Vector4f), cudaMemcpyDeviceToDevice));
//	ITMSafeCall(cudaMemcpy(inactiveNumUpdates, remappedInactiveNumUpdates, trackingState->pointCloud->inactive_update_count->noDims.x * trackingState->pointCloud->inactive_update_count->noDims.y * sizeof(short), cudaMemcpyDeviceToDevice));
//	ITMSafeCall(cudaMemcpy(inactiveReliability, remappedInactiveReliability, trackingState->pointCloud->inactive_reliability->noDims.x * trackingState->pointCloud->inactive_reliability->noDims.y * sizeof(float), cudaMemcpyDeviceToDevice));

	// CPU non-parellized version for remapping
	memset(remappedInactiveLocations, 0, trackingState->pointCloud->inactive_locations->noDims.x * trackingState->pointCloud->inactive_locations->noDims.y * sizeof(Vector4f));
  memset(remappedInactiveNumUpdates, 0, trackingState->pointCloud->inactive_update_count->noDims.x * trackingState->pointCloud->inactive_update_count->noDims.y * sizeof(short));
  memset(remappedInactiveReliability, 0, trackingState->pointCloud->inactive_reliability->noDims.x * trackingState->pointCloud->inactive_reliability->noDims.y * sizeof(float));
  memset(noRemappedInactivePoints_device, 0, sizeof(int));
	remapInactiveLocations(inactiveLocations, inactiveNumUpdates, inactiveReliability, remappedInactiveLocations, remappedInactiveNumUpdates, remappedInactiveReliability, noRemappedInactivePoints_device, trackingState->pointCloud->inactive_locations->noDims, M_d, projParams_d);
	memcpy(inactiveLocations, remappedInactiveLocations, trackingState->pointCloud->inactive_locations->noDims.x * trackingState->pointCloud->inactive_locations->noDims.y * sizeof(Vector4f));
  memcpy(inactiveNumUpdates, remappedInactiveNumUpdates, trackingState->pointCloud->inactive_update_count->noDims.x * trackingState->pointCloud->inactive_update_count->noDims.y * sizeof(short));
  memcpy(inactiveReliability, remappedInactiveReliability, trackingState->pointCloud->inactive_reliability->noDims.x * trackingState->pointCloud->inactive_reliability->noDims.y * sizeof(float));
  trackingState->pointCloud->UpdateDeviceFromHost();
  inactiveLocations = trackingState->pointCloud->inactive_locations->GetData(MEMORYDEVICE_CUDA);
  inactiveNumUpdates = trackingState->pointCloud->inactive_update_count->GetData(MEMORYDEVICE_CUDA);
  inactiveReliability = trackingState->pointCloud->inactive_reliability->GetData(MEMORYDEVICE_CUDA);
  remappedInactiveLocations = trackingState->pointCloud->remapped_inactive_locations->GetData(MEMORYDEVICE_CUDA);
  remappedInactiveNumUpdates = trackingState->pointCloud->remapped_inactive_update_count->GetData(MEMORYDEVICE_CUDA);
  remappedInactiveReliability = trackingState->pointCloud->remapped_inactive_reliability->GetData(MEMORYDEVICE_CUDA);

	int noRemappedPoints;
//  ITMSafeCall(cudaMemcpy(&noRemappedPoints, noRemappedInactivePoints_device, sizeof(int), cudaMemcpyDeviceToHost));
  memcpy(&noRemappedPoints, noRemappedInactivePoints_device, sizeof(int));
	std::cout << "num pts remapped: " << noRemappedPoints << std::endl;
	int noNeededEntries = this->LoadFromGlobalMemory(scene);

	int maxW = scene->sceneParams->maxW;

	if (noNeededEntries > 0) {// && noRemappedPoints < 30000) {
		blockSize = dim3(SDF_BLOCK_SIZE, SDF_BLOCK_SIZE, SDF_BLOCK_SIZE);
		gridSize = dim3(noNeededEntries);

		bool is_old = false;
		ITMSafeCall(cudaMemset(swappedInDataIsOld_device, 0, sizeof(bool)));
		readSwappedInDataAsPointCloud_device << <gridSize, blockSize >> >(localVBA, syncedVoxelBlocks_local, inactiveLocations,
		    inactiveNumUpdates, inactiveReliability, swapStates, neededEntryIDs_local, hashTable, M_d, projParams_d, imgSize, voxelSize,
		    alpha, beta, w_max, mu, sdkGetTimerValue(&renderState->timer), swappedInDataIsOld_device);
		ITMSafeCall(cudaMemcpy(&is_old, swappedInDataIsOld_device, sizeof(bool), cudaMemcpyDeviceToHost));
		if (is_old) {
		  should_fuse = false;
		  std::cout << "Swapped-in VOXELS are more than 50 sec old!!!!!!!!!!!!!" << std::endl;
		}
		if (should_fuse) {
			integrateOldIntoActiveData_device << <gridSize, blockSize >> >(localVBA, swapStates, syncedVoxelBlocks_local,
				neededEntryIDs_local, hashTable, maxW, sdkGetTimerValue(&renderState->timer));
		}
	}
}

template<class TVoxel>
void ITMSwappingEngine_CUDA<TVoxel, ITMVoxelBlockHash>::SaveToGlobalMemory(ITMScene<TVoxel, ITMVoxelBlockHash> *scene, ITMRenderState *renderState, visualization_msgs::Marker *meshMsg)
{
	ITMGlobalCache<TVoxel> *globalCache = scene->globalCache;

	ITMHashSwapState *swapStates = globalCache->GetSwapStates(true);

	ITMHashEntry *hashTable = scene->index.GetEntries();
	uchar *entriesVisibleType = ((ITMRenderState_VH*)renderState)->GetEntriesVisibleType();
	
	TVoxel *syncedVoxelBlocks_local = globalCache->GetSyncedVoxelBlocks(true);
	bool *hasSyncedData_local = globalCache->GetHasSyncedData(true);
	int *neededEntryIDs_local = globalCache->GetNeededEntryIDs(true);
	int *neededMeshEntryIDs_local = globalCache->GetNeededMeshEntryIDs(true);

	TVoxel *syncedVoxelBlocks_global = globalCache->GetSyncedVoxelBlocks(false);
	bool *hasSyncedData_global = globalCache->GetHasSyncedData(false);
	int *neededEntryIDs_global = globalCache->GetNeededEntryIDs(false);
	int *neededMeshEntryIDs_global = globalCache->GetNeededMeshEntryIDs(false);

	TVoxel *localVBA = scene->localVBA.GetVoxelBlocks();
	int *voxelAllocationList = scene->localVBA.GetAllocationList();

	int noTotalEntries = globalCache->noTotalEntries;
	
	dim3 blockSize, gridSize;
	int noNeededEntries;

	{
		blockSize = dim3(256);
		gridSize = dim3((int)ceil((float)scene->index.noTotalEntries / (float)blockSize.x));

		ITMSafeCall(cudaMemset(noNeededEntries_device, 0, sizeof(int)));

		buildListToSwapOut_device << <gridSize, blockSize >> >(neededEntryIDs_local, neededMeshEntryIDs_local, noNeededEntries_device, swapStates,
			hashTable, entriesVisibleType, noTotalEntries);

		ITMSafeCall(cudaMemcpy(&noNeededEntries, noNeededEntries_device, sizeof(int), cudaMemcpyDeviceToHost));
	}

	if (noNeededEntries > 0)
	{
		if (SDF_MESH_BLOCK_NUM < SDF_TRANSFER_BLOCK_NUM) {
			std::cout << "Be reasonable and choose SDF_MESH_BLOCK_NUM at least as big as SDF_TRANSFER_BLOCK_NUM" << std::endl;
			exit(1);
		}
		noNeededEntries = MIN(noNeededEntries, SDF_MESH_BLOCK_NUM);

		// mesh swapped-out scene
		this->meshingEngine->MeshSwappedScene(this->inactiveMesh, scene, neededMeshEntryIDs_local, noNeededEntries);
		this->inactiveMesh->publishROSMesh(meshMsg);
		this->inactiveMesh->publishROSPcl();
		std::cout << "Mesh no. triangles: " << this->inactiveMesh->noTotalTriangles << std::endl;

		noNeededEntries = MIN(noNeededEntries, SDF_TRANSFER_BLOCK_NUM);

		{
			blockSize = dim3(SDF_BLOCK_SIZE, SDF_BLOCK_SIZE, SDF_BLOCK_SIZE);
			gridSize = dim3(noNeededEntries);

			moveActiveDataToTransferBuffer_device << <gridSize, blockSize >> >(syncedVoxelBlocks_local, hasSyncedData_local,
				neededEntryIDs_local, hashTable, localVBA);
		}

		{
			blockSize = dim3(256);
			gridSize = dim3((int)ceil((float)noNeededEntries / (float)blockSize.x));

			ITMSafeCall(cudaMemcpy(noAllocatedVoxelEntries_device, &scene->localVBA.lastFreeBlockId, sizeof(int), cudaMemcpyHostToDevice));

			cleanMemory_device << <gridSize, blockSize >> >(voxelAllocationList, noAllocatedVoxelEntries_device, swapStates, hashTable, localVBA,
				neededEntryIDs_local, noNeededEntries);

			ITMSafeCall(cudaMemcpy(&scene->localVBA.lastFreeBlockId, noAllocatedVoxelEntries_device, sizeof(int), cudaMemcpyDeviceToHost));
			scene->localVBA.lastFreeBlockId = MAX(scene->localVBA.lastFreeBlockId, 0);
			scene->localVBA.lastFreeBlockId = MIN(scene->localVBA.lastFreeBlockId, SDF_LOCAL_BLOCK_NUM);
		}

		ITMSafeCall(cudaMemcpy(neededEntryIDs_global, neededEntryIDs_local, sizeof(int) * noNeededEntries, cudaMemcpyDeviceToHost));
		ITMSafeCall(cudaMemcpy(hasSyncedData_global, hasSyncedData_local, sizeof(bool) * noNeededEntries, cudaMemcpyDeviceToHost));
		ITMSafeCall(cudaMemcpy(syncedVoxelBlocks_global, syncedVoxelBlocks_local, sizeof(TVoxel) *SDF_BLOCK_SIZE3 * noNeededEntries, cudaMemcpyDeviceToHost));

		for (int entryId = 0; entryId < noNeededEntries; entryId++)
		{
			if (hasSyncedData_global[entryId])
				globalCache->SetStoredData(neededEntryIDs_global[entryId], syncedVoxelBlocks_global + entryId * SDF_BLOCK_SIZE3);
		}
	}
}

template<class TVoxel>
void ITMSwappingEngine_CUDA<TVoxel, ITMVoxelBlockHash>::remapInactiveLocations(const Vector4f* inactiveLocations, const short* inactiveNumUpdates, const float* inactiveReliability, Vector4f* remappedLocations, short* remappedInactiveNumUpdates, float* remappedReliability, int *noRemappedEntries, const Vector2i &imgSize, const Matrix4f &M_d, const Vector4f &projParams_d) {

  int pixelLoc, pixelLocNeighbour;
  int nSize = 1;
  Matrix4f M_d_inv;
  M_d.inv(M_d_inv);
  Vector3f pt_delta;
  float depth;
  Vector4f backProjectedPoint;
  bool allocatedNeighbour = false;
  for (int y = 0; y < imgSize.y; ++y) {
    for (int x = 0; x < imgSize.x; ++x) {

      allocatedNeighbour = false;
      int locId = x + y * imgSize.x;

//      if (inactiveLocations[locId].w > 0) {
//        std::cout << "Md: " << M_d << std::endl;
//        std::cout << "proj par: " << projParams_d << std::endl;
//        std::cout << "inac loc: " << inactiveLocations[locId] << std::endl;
//        std::cout << "siz: " << imgSize << std::endl;
//      }

      if (inactiveLocations[locId].w <= 0) continue;
      int pixelLoc = forwardProjectPoint(inactiveLocations[locId], M_d, projParams_d, imgSize);
      if (pixelLoc >= 0) {
        (*noRemappedEntries)++;
        pt_delta = inactiveLocations[locId].toVector3() - M_d_inv.getTrans();
        depth = sqrt(pt_delta.x*pt_delta.x + pt_delta.y*pt_delta.y + pt_delta.z*pt_delta.z);

        if (remappedLocations[pixelLoc].w <= 0) {
          remappedLocations[pixelLoc] = inactiveLocations[locId];
          remappedReliability[pixelLoc] = inactiveReliability[locId];
          remappedInactiveNumUpdates[pixelLoc] = inactiveNumUpdates[locId];
        } else {
          if (inactiveReliability[locId] > remappedReliability[pixelLoc]) {
            remappedLocations[pixelLoc] = inactiveLocations[locId];
            remappedReliability[pixelLoc] = inactiveReliability[locId];
          }
          remappedInactiveNumUpdates[pixelLoc] += inactiveNumUpdates[locId] + 1;

          // to conserve points during remapping, add the remapped point to one unallocated neighbour (if there is one)
          for (int ny = -nSize; ny <= nSize; ++ny) {
            for (int nx = -nSize; nx <= nSize; ++nx) {
              if (allocatedNeighbour || (nx == 0 && ny == 0)) continue;
              pixelLocNeighbour = pixelLoc + nx + (ny * imgSize.x);
              if (pixelLocNeighbour < 0 || (pixelLocNeighbour >= imgSize.x * imgSize.y) || remappedLocations[pixelLocNeighbour].w > 0) continue;

              backProjectedPoint = backProjectPixel(pixelLocNeighbour, depth, M_d_inv, projParams_d, imgSize);
              if (backProjectedPoint.w <= 0) continue;
              remappedLocations[pixelLocNeighbour] = backProjectedPoint;
              remappedReliability[pixelLocNeighbour] = inactiveReliability[locId] * 0.75;
              remappedInactiveNumUpdates[pixelLocNeighbour] = 1;//inactiveNumUpdates[locId];
              allocatedNeighbour = true;
            }
          }

        }

      }
    }
  }
}

__global__ void buildListToSwapIn_device(int *neededEntryIDs, int *noNeededEntries, ITMHashSwapState *swapStates, int noTotalEntries)
{
	int targetIdx = threadIdx.x + blockIdx.x * blockDim.x;
	if (targetIdx > noTotalEntries - 1) return;

	__shared__ bool shouldPrefix;

	shouldPrefix = false;
	__syncthreads();

	bool isNeededId = (swapStates[targetIdx].state == 1);

	if (isNeededId) shouldPrefix = true;
	__syncthreads();

	if (shouldPrefix)
	{
		int offset = computePrefixSum_device<int>(isNeededId, noNeededEntries, blockDim.x * blockDim.y, threadIdx.x);
		if (offset != -1 && offset < SDF_TRANSFER_BLOCK_NUM) neededEntryIDs[offset] = targetIdx;
	}
}

__global__ void buildListToSwapOut_device(int *neededEntryIDs, int *neededMeshEntryIDs, int *noNeededEntries, ITMHashSwapState *swapStates,
	ITMHashEntry *hashTable, uchar *entriesVisibleType, int noTotalEntries)
{
	int targetIdx = threadIdx.x + blockIdx.x * blockDim.x;
	if (targetIdx > noTotalEntries - 1) return;

	__shared__ bool shouldPrefix;

	shouldPrefix = false;
	__syncthreads();

	ITMHashSwapState &swapState = swapStates[targetIdx];

	bool isNeededId = ( swapState.state == 2 &&
		hashTable[targetIdx].ptr >= 0 && entriesVisibleType[targetIdx] == 0);

	if (isNeededId) shouldPrefix = true;
	__syncthreads();

	if (shouldPrefix)
	{
		int offset = computePrefixSum_device<int>(isNeededId, noNeededEntries, blockDim.x * blockDim.y, threadIdx.x);
		if (offset != -1 && offset < SDF_TRANSFER_BLOCK_NUM) neededEntryIDs[offset] = targetIdx;
		if (offset != -1 && offset < SDF_MESH_BLOCK_NUM) neededMeshEntryIDs[offset] = targetIdx;
	}
}

template<class TVoxel>
__global__ void cleanMemory_device(int *voxelAllocationList, int *noAllocatedVoxelEntries, ITMHashSwapState *swapStates,
	ITMHashEntry *hashTable, TVoxel *localVBA, int *neededEntryIDs_local, int noNeededEntries)
{
	int locId = threadIdx.x + blockIdx.x * blockDim.x;
	
	if (locId > noNeededEntries - 1) return;

	int entryDestId = neededEntryIDs_local[locId];
	
	swapStates[entryDestId].state = 0;

	int vbaIdx = atomicAdd(&noAllocatedVoxelEntries[0], 1);
	if (vbaIdx < SDF_LOCAL_BLOCK_NUM - 1)
	{
		voxelAllocationList[vbaIdx + 1] = hashTable[entryDestId].ptr;
		hashTable[entryDestId].ptr = -1;
	}
}

template<class TVoxel>
__global__ void moveActiveDataToTransferBuffer_device(TVoxel *syncedVoxelBlocks_local, bool *hasSyncedData_local,
	int *neededEntryIDs_local, ITMHashEntry *hashTable, TVoxel *localVBA)
{
	int entryDestId = neededEntryIDs_local[blockIdx.x];

	ITMHashEntry &hashEntry = hashTable[entryDestId];

	TVoxel *dstVB = syncedVoxelBlocks_local + blockIdx.x * SDF_BLOCK_SIZE3;
	TVoxel *srcVB = localVBA + hashEntry.ptr * SDF_BLOCK_SIZE3;

	int vIdx = threadIdx.x + threadIdx.y * SDF_BLOCK_SIZE + threadIdx.z * SDF_BLOCK_SIZE * SDF_BLOCK_SIZE;
	dstVB[vIdx] = srcVB[vIdx];
	srcVB[vIdx] = TVoxel();

	if (vIdx == 0) hasSyncedData_local[blockIdx.x] = true;
}

template<class TVoxel>
__global__ void integrateOldIntoActiveData_device(TVoxel *localVBA, ITMHashSwapState *swapStates, TVoxel *syncedVoxelBlocks_local,
	int *neededEntryIDs_local, ITMHashEntry *hashTable, int maxW, const double update_time)
{
	int entryDestId = neededEntryIDs_local[blockIdx.x];

	TVoxel *srcVB = syncedVoxelBlocks_local + blockIdx.x * SDF_BLOCK_SIZE3;
	TVoxel *dstVB = localVBA + hashTable[entryDestId].ptr * SDF_BLOCK_SIZE3;

	int vIdx = threadIdx.x + threadIdx.y * SDF_BLOCK_SIZE + threadIdx.z * SDF_BLOCK_SIZE * SDF_BLOCK_SIZE;

	CombineVoxelInformation<TVoxel::hasColorInformation, TVoxel>::compute(srcVB[vIdx], dstVB[vIdx], maxW, update_time);

	if (vIdx == 0) swapStates[entryDestId].state = 2;
}

__global__ void remapInactiveLocations_device(const Vector4f* inactiveLocations, const short* inactiveNumUpdates, const float* inactiveReliability, Vector4f* remappedLocations, short* remappedInactiveNumUpdates, float* remappedReliability, int *noRemappedEntries, const Vector2i imgSize, const Matrix4f M_d, const Vector4f projParams_d) {
	int x = (threadIdx.x + blockIdx.x * blockDim.x), y = (threadIdx.y + blockIdx.y * blockDim.y);

	if (x >= imgSize.x || y >= imgSize.y) return;

	int locId = x + y * imgSize.x;

	int pixelLoc = forwardProjectPoint(inactiveLocations[locId], M_d, projParams_d, imgSize);
	if (pixelLoc >= 0) {
	  atomicAdd(noRemappedEntries, 1);
	  remappedLocations[pixelLoc] = inactiveLocations[locId];
	  remappedReliability[pixelLoc] = inactiveReliability[locId];
	  remappedInactiveNumUpdates[pixelLoc] = inactiveNumUpdates[locId];
//	  for (int n = 1; n <= 9; ++n) {
//	    if (pixelLoc + n < imgSize.x*imgSize.y) {
//        remappedLocations[pixelLoc+n] = inactiveLocations[locId];
//        remappedReliability[pixelLoc+n] = inactiveReliability[locId];
//        remappedInactiveNumUpdates[pixelLoc+n] = inactiveNumUpdates[locId];
//	    }
//	    if (pixelLoc + n*imgSize.x < imgSize.x*imgSize.y) {
//        remappedLocations[pixelLoc+n*imgSize.x] = inactiveLocations[locId];
//        remappedReliability[pixelLoc+n*imgSize.x] = inactiveReliability[locId];
//        remappedInactiveNumUpdates[pixelLoc+n*imgSize.x] = inactiveNumUpdates[locId];
//      }
//	    if (pixelLoc - n >= 0) {
//        remappedLocations[pixelLoc-n] = inactiveLocations[locId];
//        remappedReliability[pixelLoc-n] = inactiveReliability[locId];
//        remappedInactiveNumUpdates[pixelLoc-n] = inactiveNumUpdates[locId];
//      }
//	    if (pixelLoc - n*imgSize.x >= 0) {
//        remappedLocations[pixelLoc-n*imgSize.x] = inactiveLocations[locId];
//        remappedReliability[pixelLoc-n*imgSize.x] = inactiveReliability[locId];
//        remappedInactiveNumUpdates[pixelLoc-n*imgSize.x] = inactiveNumUpdates[locId];
//      }
//	  }
	}
}

// reads swapped-in voxels and generates a zero-crossing estimate (Vector4f image)
// Also checks for swapped-in voxel update_times. If any voxel is pretty old, returns true, else false
template<class TVoxel>
__global__ void readSwappedInDataAsPointCloud_device(TVoxel *localVBA, TVoxel *syncedVoxelBlocks_local, Vector4f *inactiveLocations, short* inactiveNumUpdates, float *inactiveReliability,
	ITMHashSwapState *swapStates, int *neededEntryIDs_local, ITMHashEntry *hashTable, Matrix4f M_d, Vector4f projParams_d, Vector2i imgSize, float voxelSize,
	float alpha, float beta, int w_max, float mu, const double current_time, bool *is_old)
{
  *is_old = false; // indicates if any voxel that is being swapped-in is very old.
	int entryDestId = neededEntryIDs_local[blockIdx.x];

	TVoxel *inactiveVB = syncedVoxelBlocks_local + blockIdx.x * SDF_BLOCK_SIZE3;
	int vIdx = threadIdx.x + threadIdx.y * SDF_BLOCK_SIZE + threadIdx.z * SDF_BLOCK_SIZE * SDF_BLOCK_SIZE;
	if (vIdx == 0) swapStates[entryDestId].state = 2;
	if (inactiveVB[vIdx].w_depth == 0 || hashTable[entryDestId].ptr < 0) return;
	if (inactiveVB[vIdx].last_update_time < current_time/1000.0 - 10.0) {
	  *is_old = true;
	} else {
	  return;
	}

	Vector3i globalPos;
	globalPos.x = hashTable[entryDestId].pos.x;
	globalPos.y = hashTable[entryDestId].pos.y;
	globalPos.z = hashTable[entryDestId].pos.z;
	globalPos *= SDF_BLOCK_SIZE;

	Vector4f pt_model;

	int x = threadIdx.x, y = threadIdx.y, z = threadIdx.z;

	pt_model.x = (float)(globalPos.x + x) * voxelSize;
	pt_model.y = (float)(globalPos.y + y) * voxelSize;
	pt_model.z = (float)(globalPos.z + z) * voxelSize;
	pt_model.w = 1.0f;

	Matrix4f M_d_inv;
	M_d.inv(M_d_inv);
	Vector3f M_d_inv_t = M_d_inv.getTrans();
	Vector4f cam_center(M_d_inv_t.x, M_d_inv_t.y, M_d_inv_t.z, 1.0);
	Vector4f ray_direction = pt_model - cam_center;
	float direction_norm = 1.0f / sqrt(ray_direction.x * ray_direction.x + ray_direction.y * ray_direction.y + ray_direction.z * ray_direction.z);
	ray_direction *= direction_norm;

	// zero crossing
	pt_model += ray_direction * TVoxel::SDF_valueToFloat(inactiveVB[vIdx].sdf) * mu;

	// project into camera image and get x,y
	int pixelLoc = forwardProjectPoint(pt_model, M_d, projParams_d, imgSize);
	if (pixelLoc >= 0) {
//	  inactiveLocations[pixelLoc] = pt_model;
	  if (inactiveLocations[pixelLoc].w <= 0) {
	    if (inactiveVB[vIdx].getVoxelReliability(alpha, beta, w_max) >= 0) {
        inactiveLocations[pixelLoc] = pt_model;
        inactiveReliability[pixelLoc] = inactiveVB[vIdx].getVoxelReliability(alpha, beta, w_max);
	    }
	  } else {
	    // replace the inactive point only if the new one has higher reliability value than the earlier one
//	    if (abs(TVoxel::SDF_valueToFloat(inactiveVB[vIdx].sdf)) < abs(inactiveReliability[pixelLoc])) {
      if (inactiveVB[vIdx].getVoxelReliability(alpha, beta, w_max) > inactiveReliability[pixelLoc]) {
	      inactiveLocations[pixelLoc] = pt_model;
	      inactiveReliability[pixelLoc] = inactiveVB[vIdx].getVoxelReliability(alpha, beta, w_max);
	    }
	  }

	  inactiveNumUpdates[pixelLoc] += 1;
	}
}

template class ITMLib::Engine::ITMSwappingEngine_CUDA<ITMVoxel, ITMVoxelIndex>;

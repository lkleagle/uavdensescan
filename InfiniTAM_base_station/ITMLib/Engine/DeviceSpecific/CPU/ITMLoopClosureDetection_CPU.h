/*
 * ITMLoopClosureDetection_CPU.h
 *
 *  Created on: May 13, 2016
 *      Author: anurag
 */

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#ifndef INFINITAM_INFINITAM_ITMLIB_ENGINE_DEVICESPECIFIC_CPU_ITMLOOPCLOSUREDETECTION_CPU_H_
#define INFINITAM_INFINITAM_ITMLIB_ENGINE_DEVICESPECIFIC_CPU_ITMLOOPCLOSUREDETECTION_CPU_H_

#pragma once

#include "../../ITMLoopClosureDetection.h"

namespace ITMLib
{
  namespace Engine
  {
    class ITMLoopClosureDetection_CPU : public ITMLoopClosureDetection
    {
    protected:
      std::pair<Vector4f*, int> ComputeGandH(float &f, float *nabla, float *hessian, Matrix4f approxInvPose, bool visualize);

      template<bool shortIteration, bool rotationOnly>
      Vector4f computePerPointGH_Depth_NN(THREADPTR(float) *localNabla, THREADPTR(float) *localHessian, THREADPTR(float) &localF,
        const THREADPTR(int) & x, const THREADPTR(int) & y,
        const CONSTPTR(Vector4f) &depthPoint, const CONSTPTR(Vector2i) & viewImageSize, const CONSTPTR(Vector4f) & viewIntrinsics, const CONSTPTR(Vector2i) & sceneImageSize,
        const CONSTPTR(Vector4f) & sceneIntrinsics, const CONSTPTR(Matrix4f) & approxInvPose, const CONSTPTR(Matrix4f) & scenePose, const CONSTPTR(Vector4f) *pointsMap,
        const CONSTPTR(Vector4f) *normalsMap, float distThresh, const boost::shared_ptr<Nabo::NNSearchF>& nns);

      template<bool shortIteration, bool rotationOnly>
      Vector4f computePerPointGH_Depth_Ab_NN(THREADPTR(float) *A, THREADPTR(float) &b,
        const THREADPTR(int) & x, const THREADPTR(int) & y,
        const CONSTPTR(Vector4f) &depthPoint, const CONSTPTR(Vector2i) & viewImageSize, const CONSTPTR(Vector4f) & viewIntrinsics, const CONSTPTR(Vector2i) & sceneImageSize,
        const CONSTPTR(Vector4f) & sceneIntrinsics, const CONSTPTR(Matrix4f) & approxInvPose, const CONSTPTR(Matrix4f) & scenePose, const CONSTPTR(Vector4f) *pointsMap,
        const CONSTPTR(Vector4f) *normalsMap, float distThresh, const boost::shared_ptr<Nabo::NNSearchF>& nns);

      // Libpointmatcher
      PM::ICP icp;

      // Use libpointmatcher for ICP routine
      Matrix4f getLPMICPTF(Matrix4f& prevInvPose);

      // Flaot4Image to LPM Point cloud
      DP Float4ImagetoLPMPointCloud(const ITMFloat4Image* im);

      // FlaotImage to LPM Point cloud
      DP FloatImagetoLPMPointCloud(const ITMFloatImage* im, const Vector4f intrinsics);

    public:
      ITMLoopClosureDetection_CPU(Vector2i imgSize, TrackerIterationType *trackingRegime, int noHierarchyLevels, int noICPRunTillLevel, float distThresh,
        float terminationThreshold, ITMLibSettings::DepthTrackerType tracker_type, bool visualize_lc, std::string resourcesDir, const ITMLowLevelEngine *lowLevelEngine);
      ~ITMLoopClosureDetection_CPU(void);
    };
  }
}


#endif /* INFINITAM_INFINITAM_ITMLIB_ENGINE_DEVICESPECIFIC_CPU_ITMLOOPCLOSUREDETECTION_CPU_H_ */

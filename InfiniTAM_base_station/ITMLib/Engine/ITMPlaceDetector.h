/*
 * ITMPlaceDetector.h
 *
 *  Created on: Nov 26, 2016
 *      Author: anurag
 *      Heavily utilizing original code from DLoopDetector (Author: Dorian Galvez-Lopez)
 */

#ifndef INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMPLACEDETECTOR_H_
#define INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMPLACEDETECTOR_H_

#include "DBoW2/DBoW2.h"
#include "DBoW2/FSurf64.h"
#include "DBoW2/FBrief.h"
#include "DUtils/DUtils.h"
#include "DUtilsCV/DUtilsCV.h"
#include "DVision/DVision.h"
#include "../Utils/TemplatedLoopDetector.h"
#include "../Utils/ITMFeatureExtractors.h"
#include "../Utils/ITMLibDefines.h"

#include <iostream>
#include <vector>
#include <string>

// ROS
#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

/// SURF64 Loop Detector
typedef DLoopDetector::TemplatedLoopDetector
  <FSurf64::TDescriptor, FSurf64> Surf64LoopDetector;

/// BRIEF Loop Detector
typedef DLoopDetector::TemplatedLoopDetector
  <FBrief::TDescriptor, FBrief> BriefLoopDetector;

using namespace DLoopDetector;
using namespace DBoW2;

namespace ITMLib
{
  namespace Engine
  {
    /// @param TVocabulary vocabulary class (e.g: Surf64Vocabulary)
    /// @param TDetector detector class (e.g: Surf64LoopDetector)
    /// @param TDescriptor descriptor class (e.g: vector<float> for SURF)
    template<class TVocabulary, class TDetector, class TDescriptor>
    class ITMPlaceDetector
    {
     public:

      /**
       * @param vocfile vocabulary file to load
       * @param imgSize image size
       * @param maxNumImages max. no of images (needed to allocate memory)
       */
      ITMPlaceDetector(const std::string &vocfile, const Vector2i imgSize, const int maxNumImages);

      ~ITMPlaceDetector();

      /**
       * Processes a frame
       * @param extractor functor to extract features
       */
      void ProcessFrame(cv::Mat &image, const ITMPose *pose, const FeatureExtractor<TDescriptor> &extractor);

      /**
       * Publishes an arrow marker to detected place (RViz)
       */
      void publishROSMarker(const ITMPose &query, const ITMPose &match);

     protected:

      std::string vocfile;
      int width;
      int height;
      int num_loops_detected;

      std::vector<cv::KeyPoint> keys;
      std::vector<TDescriptor> descriptors;

      // Loop detector parameters
      typename TDetector::Parameters *params;
      TVocabulary *voc;
      TDetector *detector;

      // result
      DetectionResult result;

      // prepare profiler to measure times
      DUtils::Profiler profiler;
      DUtilsCV::Drawing drawer;

      // ROS
      ros::NodeHandle nh;
      int32_t arrowId;
      ros::Publisher pubArrow;
    };

    template<class TVocabulary, class TDetector, class TDescriptor>
    ITMPlaceDetector<TVocabulary, TDetector, TDescriptor>::ITMPlaceDetector
      (const std::string &vocfile, const Vector2i imgSize, const int maxNumImages)
      : vocfile(vocfile), width(imgSize.width), height(imgSize.height)
    {
      // Set loop detector parameters
      params = new typename TDetector::Parameters(height, width);

      // Parameters given by default are:
      // use nss = true
      // alpha = 0.3
      // k = 3
      // geom checking = GEOM_DI
      // di levels = 0

      // We are going to change these values individually:
      params->use_nss = true; // use normalized similarity score instead of raw score
      params->alpha = 0.3; // nss threshold
      params->k = 0; // a loop must be consistent with 1 previous matches
      params->geom_check = GEOM_DI; // use direct index for geometrical checking
      params->di_levels = 2; // use two direct index levels

      // To verify loops you can select one of the next geometrical checkings:
      // GEOM_EXHAUSTIVE: correspondence points are computed by comparing all
      //    the features between the two images.
      // GEOM_FLANN: as above, but the comparisons are done with a Flann structure,
      //    which makes them faster. However, creating the flann structure may
      //    be slow.
      // GEOM_DI: the direct index is used to select correspondence points between
      //    those features whose vocabulary node at a certain level is the same.
      //    The level at which the comparison is done is set by the parameter
      //    di_levels:
      //      di_levels = 0 -> features must belong to the same leaf (word).
      //         This is the fastest configuration and the most restrictive one.
      //      di_levels = l (l < L) -> node at level l starting from the leaves.
      //         The higher l, the slower the geometrical checking, but higher
      //         recall as well.
      //         Here, L stands for the depth levels of the vocabulary tree.
      //      di_levels = L -> the same as the exhaustive technique.
      // GEOM_NONE: no geometrical checking is done.
      //
      // In general, with a 10^6 vocabulary, GEOM_DI with 2 <= di_levels <= 4
      // yields the best results in recall/time.
      // Check the T-RO paper for more information.
      //

      // Load the vocabulary to use
      cout << "Loading vocabulary..." << endl;
      voc = new TVocabulary(vocfile);

      // Initiate loop detector with the vocabulary
      cout << "Initializing loop detector..." << endl;
      detector = new TDetector(*voc, *params);

      // we can allocate memory for the expected number of images
      detector->allocate(maxNumImages);

      num_loops_detected = 0;

      pubArrow = nh.advertise<visualization_msgs::Marker>("itm/mesh", 1);
    }


    template<class TVocabulary, class TDetector, class TDescriptor>
    ITMPlaceDetector<TVocabulary, TDetector, TDescriptor>::~ITMPlaceDetector()
    {
      if(num_loops_detected == 0)
      {
        std::cout << "No loops found in this image sequence" << std::endl;
      }
      else
      {
        std::cout << num_loops_detected << " loops found in this image sequence!" << std::endl;
      }

      std::cout << std::endl << "ITMPlaceDetector Execution time:" << std::endl
        << " - Feature computation: " << profiler.getMeanTime("features") * 1e3
        << " ms/image" << std::endl
        << " - Loop detection: " << profiler.getMeanTime("detection") * 1e3
        << " ms/image" << std::endl;
    }


    template<class TVocabulary, class TDetector, class TDescriptor>
    void ITMPlaceDetector<TVocabulary, TDetector, TDescriptor>::ProcessFrame
      (cv::Mat &image, const ITMPose *pose, const FeatureExtractor<TDescriptor> &extractor)
    {
      std::cout << "DLoopDetector running..." << std::endl;

      // get features
      profiler.profile("features");
      extractor(image, keys, descriptors);
      profiler.stop();

      // add image to the collection and check if there is some loop
      profiler.profile("detection");
      detector->detectLoop(*pose, keys, descriptors, result);
      drawer.drawKeyPoints(image, keys, true,false);
      profiler.stop();

      if(result.detection())
      {
        std::cout << "- Loop found with image " << result.match << "!" << std::endl;
        std::cout << "- Matched pose " << result.match_pose.GetM() << std::endl;
        ++num_loops_detected;
        // ROS
        publishROSMarker(*pose, result.match_pose);
      }
      else
      {
        std::cout << "- No loop: ";
        switch(result.status)
        {
          case CLOSE_MATCHES_ONLY:
            std::cout << "All the images in the database are very recent" << std::endl;
            break;

          case NO_DB_RESULTS:
            std::cout << "There are no matches against the database (few features in"
              " the image?)" << std::endl;
            break;

          case LOW_NSS_FACTOR:
            std::cout << "Little overlap between this image and the previous one"
              << std::endl;
            break;

          case LOW_SCORES:
            std::cout << "No match reaches the score threshold (alpha: " <<
              params->alpha << ")" << std::endl;
            break;

          case NO_GROUPS:
            std::cout << "Not enough close matches to create groups. "
              << "Best candidate: " << result.match << std::endl;
            break;

          case NO_TEMPORAL_CONSISTENCY:
            std::cout << "No temporal consistency (k: " << params->k << "). "
              << "Best candidate: " << result.match << std::endl;
            break;

          case NO_GEOMETRICAL_CONSISTENCY:
            std::cout << "No geometrical consistency. Best candidate: "
              << result.match << std::endl;
            break;

          default:
            break;
        }
      }
    }

    template<class TVocabulary, class TDetector, class TDescriptor>
    void ITMPlaceDetector<TVocabulary, TDetector, TDescriptor>::publishROSMarker(
        const ITMPose &query, const ITMPose &match) {
      visualization_msgs::Marker arrowMsg;
      // Set the frame ID and timestamp.  See the TF tutorials for information on these.
      arrowMsg.header.frame_id = "infinitam_global";
      arrowMsg.header.stamp = ros::Time::now();
      // Set the namespace and id for this marker.  This serves to create a unique ID
      // Any marker sent with the same namespace and id will overwrite the old one
      arrowMsg.ns = "place recognition";
      arrowMsg.id = arrowId;
      arrowId++;
      // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
      arrowMsg.type = visualization_msgs::Marker::ARROW;
      // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
      arrowMsg.action = visualization_msgs::Marker::ADD;
      arrowMsg.lifetime = ros::Duration(10);

      geometry_msgs::Point query_point, match_point;
      query_point.x = query.GetInvM().getTrans().x;
      query_point.y = query.GetInvM().getTrans().y;
      query_point.z = query.GetInvM().getTrans().z;
      match_point.x = match.GetInvM().getTrans().x;
      match_point.y = match.GetInvM().getTrans().y;
      match_point.z = match.GetInvM().getTrans().z;
      arrowMsg.points.push_back(query_point);
      arrowMsg.points.push_back(match_point);
      // Set the scale of the marker -- 1x1x1 here means 1m on a side
      arrowMsg.scale.x = 0.01;
      arrowMsg.scale.y = 0.03;
      arrowMsg.scale.z = 0.03;
      // Set the default colour = blue
      arrowMsg.color.r = 1.0f;
      arrowMsg.color.g = 0.0f;
      arrowMsg.color.b = 1.0f;
      arrowMsg.color.a = 1.0;

      pubArrow.publish(arrowMsg);
    }

  } // namespace Engine
} // namespace ITMLib

#endif /* INFINITAM_INFINITAM_ITMLIB_ENGINE_ITMPLACEDETECTOR_H_ */

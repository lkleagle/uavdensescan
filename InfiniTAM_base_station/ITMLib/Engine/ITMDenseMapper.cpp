// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMDenseMapper.h"

#include "../Objects/ITMRenderState_VH.h"

#include "../ITMLib.h"

using namespace ITMLib::Engine;

template<class TVoxel, class TIndex>
ITMDenseMapper<TVoxel, TIndex>::ITMDenseMapper(const ITMLibSettings *settings)
{
	swappingEngine = NULL;

	switch (settings->deviceType)
	{
	case ITMLibSettings::DEVICE_CPU:
		sceneRecoEngine = new ITMSceneReconstructionEngine_CPU<TVoxel,TIndex>();
		if (settings->useSwapping) swappingEngine = new ITMSwappingEngine_CPU<TVoxel,TIndex>();
		break;
	case ITMLibSettings::DEVICE_CUDA:
#ifndef COMPILE_WITHOUT_CUDA
		sceneRecoEngine = new ITMSceneReconstructionEngine_CUDA<TVoxel,TIndex>();
		if (settings->useSwapping) swappingEngine = new ITMSwappingEngine_CUDA<TVoxel,TIndex>();
#endif
		break;
	case ITMLibSettings::DEVICE_METAL:
#ifdef COMPILE_WITH_METAL
		sceneRecoEngine = new ITMSceneReconstructionEngine_Metal<TVoxel, TIndex>();
		if (settings->useSwapping) swappingEngine = new ITMSwappingEngine_CPU<TVoxel, TIndex>();
#endif
		break;
	}
}

template<class TVoxel, class TIndex>
ITMDenseMapper<TVoxel,TIndex>::~ITMDenseMapper()
{
	delete sceneRecoEngine;
	if (swappingEngine!=NULL) delete swappingEngine;
}

template<class TVoxel, class TIndex>
void ITMDenseMapper<TVoxel,TIndex>::ResetScene(ITMScene<TVoxel,TIndex> *scene, const float rewind_time)
{
	sceneRecoEngine->ResetScene(scene, rewind_time);
}

template<class TVoxel, class TIndex>
void ITMDenseMapper<TVoxel,TIndex>::ProcessFrame(const ITMView *view, ITMTrackingState *trackingState, ITMScene<TVoxel,TIndex> *scene, ITMRenderState *renderState, visualization_msgs::Marker *meshMsg, const float delta_time, bool &should_fuse)
{
	// allocation
	sceneRecoEngine->AllocateSceneFromDepth(scene, view, trackingState, renderState, delta_time);

	// integration
	sceneRecoEngine->IntegrateIntoScene(scene, view, trackingState, renderState);

	if (swappingEngine != NULL) {
		// swapping: CPU -> GPU
	  double bla_t;
	  if (TIMER_CHOICE == 0) {
	    bla_t = sdkGetTimerValue(&renderState->timer);
	  } else {
	    ros::Time rosTime = ros::Time::now();
	    bla_t = rosTime.toSec();
	  }
		swappingEngine->IntegrateGlobalIntoLocal(scene, view, trackingState, renderState, should_fuse);
	  double cpu_to_gpu_time;
	  if (TIMER_CHOICE == 0) {
	    cpu_to_gpu_time = sdkGetTimerValue(&renderState->timer) - bla_t;
	  } else {
	    ros::Time rosTime = ros::Time::now();
	    cpu_to_gpu_time = 1000.0 * (rosTime.toSec() - bla_t);
	  }
	  std::cout << "Took " << cpu_to_gpu_time << " ms to transfer from CPU to GPU" << std::endl;

//	  if (renderState->noTotalInactivePoints > 0.3*(trackingState->trackingImageSize.x*trackingState->trackingImageSize.y)) {
//      short * blabla = trackingState->pointCloud->inactive_update_count->GetData(MEMORYDEVICE_CUDA);
//      int num_read = 0;
//      short pix;
//      for (int i = 0; i < trackingState->pointCloud->inactive_update_count->noDims.x * trackingState->pointCloud->inactive_update_count->noDims.y; ++i) {
//    #ifndef COMPILE_WITHOUT_CUDA
//        ITMSafeCall(cudaMemcpy(&pix, &blabla[i], sizeof(short), cudaMemcpyDeviceToHost));
//    #else
//        pix = blabla[i];
//    #endif
//  //      if (pix.w > 0) {
//  ////        std::cout << "pix: " << (float)pix.x << ", " << (float)pix.y << ", " << (float)pix.z << std::endl;
//  //        num_read++;
//  //      }
//        if (pix > 0) {
//          std::cout << "pix: " << pix << std::endl;
//          num_read++;
//        }
//      }
//      std::cout << "inactive_update_count > 0: " << num_read << std::endl;
//	  }

	  // swapping: GPU -> CPU
	  swappingEngine->SaveToGlobalMemory(scene, renderState, meshMsg);
		double gpu_to_cpu_time;
		if (TIMER_CHOICE == 0) {
		  gpu_to_cpu_time = sdkGetTimerValue(&renderState->timer) - bla_t - cpu_to_gpu_time;
		} else {
		  ros::Time rosTime = ros::Time::now();
		  gpu_to_cpu_time = (1000.0 * (rosTime.toSec() - bla_t)) - cpu_to_gpu_time;
		}
    std::cout << "Took " << gpu_to_cpu_time << " ms to transfer from GPU to CPU" << std::endl;
//    if (gpu_to_cpu_time > 5000) {
//      std::cout << "transfer took too long!!!!!!!!!!1" << std::endl;
//      exit(1);
//    }
	}
}

template<class TVoxel, class TIndex>
void ITMDenseMapper<TVoxel,TIndex>::UpdateVisibleList(const ITMView *view, const ITMTrackingState *trackingState, ITMScene<TVoxel,TIndex> *scene, ITMRenderState *renderState)
{
	sceneRecoEngine->AllocateSceneFromDepth(scene, view, trackingState, renderState, true);
}

template class ITMLib::Engine::ITMDenseMapper<ITMVoxel, ITMVoxelIndex>;

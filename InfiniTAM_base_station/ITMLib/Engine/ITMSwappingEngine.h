// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../Utils/ITMLibDefines.h"

#include "../Objects/ITMScene.h"
#include "../Objects/ITMView.h"
#include "../Objects/ITMRenderState.h"

#include "../Objects/ITMTrackingState.h"
#include "../Objects/ITMMesh.h"

#include <visualization_msgs/Marker.h>

#include "ITMMeshingEngine.h"
#include "DeviceSpecific/CPU/ITMMeshingEngine_CPU.h"
#ifndef COMPILE_WITHOUT_CUDA
#include "DeviceSpecific/CUDA/ITMMeshingEngine_CUDA.h"
#endif
#ifdef COMPILE_WITH_METAL
#include "DeviceSpecific/CPU/ITMMeshingEngine_CPU.h"
#endif

using namespace ITMLib::Objects;

namespace ITMLib
{
	namespace Engine
	{
		/** \brief
			Interface to engines that swap data in and out of the
			fairly limited GPU memory to some large scale storage
			space.
			*/
		template<class TVoxel, class TIndex>
		class ITMSwappingEngine
		{
		public:
		  ITMMesh *inactiveMesh;
		  ITMMeshingEngine<ITMVoxel, ITMVoxelIndex> *meshingEngine;

			virtual void IntegrateGlobalIntoLocal(ITMScene<TVoxel, TIndex> *scene, const ITMView *view, ITMTrackingState *trackingState, ITMRenderState *renderState, bool &should_fuse) = 0;

			virtual void SaveToGlobalMemory(ITMScene<TVoxel, TIndex> *scene, ITMRenderState *renderState, visualization_msgs::Marker *meshMsg) = 0;

			virtual ~ITMSwappingEngine(void) { }
		};
	}
}

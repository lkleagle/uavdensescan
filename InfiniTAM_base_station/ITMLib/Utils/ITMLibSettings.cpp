// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "ITMLibSettings.h"

#include <stdio.h>
#include <iostream>

using namespace ITMLib::Objects;

ITMLibSettings::ITMLibSettings(void)
	: sceneParams(0.02f, 50, 0.005f, 0.2f, 3.0f, false, 0.5, 0.5), storageDirectory(".")
{
	/// depth threashold for the ICP tracker
	depthTrackerICPThreshold = 0.1f * 0.1f;

	/// For ITMDepthTracker: ICP iteration termination threshold
	depthTrackerTerminationThreshold = 1e-3f;

	/// skips every other point when using the colour tracker
	skipPoints = true;

#ifndef COMPILE_WITHOUT_CUDA
	deviceType = DEVICE_CUDA;
#else
#ifdef COMPILE_WITH_METAL
	deviceType = DEVICE_METAL;
#else
	deviceType = DEVICE_CPU;
#endif
#endif

	//deviceType = DEVICE_CPU;

	/// enables or disables swapping. HERE BE DRAGONS: It should work, but requires more testing
	useSwapping = true;

	// Time after which a voxel, if not updated, turns inactive
	deltaTime = 2.0;

	/// enables or disables approximate raycast
	useApproximateRaycast = false;

	/// enable or disable bilateral depth filtering;
	useBilateralFilter = false;

	//trackerType = TRACKER_COLOR;
	trackerType = TRACKER_ICP;
	//trackerType = TRACKER_REN;
	//trackerType = TRACKER_IMU;
	//trackerType = TRACKER_WICP;

	depthTrackerType = TRACKER_ITM;
	LCTrackerType = TRACKER_ITM;

	// ICP visualization
	visualizeICP = false;
	visualizeLC = false;

	inactiveReliabilityThresh = 0.6;
	// If number of reliable inactive points exceed this threshold (as a fraction of camera image area),
  // Loop closure check is performed
	numInactiveThresholdPercent = 0.3;

	/// model the sensor noise as  the weight for weighted ICP
	modelSensorNoise = false;
	if (trackerType == TRACKER_WICP || trackerType == TRACKER_WICP_ODOMETRY_COLOR) modelSensorNoise = true;
	

	// builds the tracking regime. level 0 is full resolution
	if (trackerType == TRACKER_IMU)
	{
//	  std::cout << "Setting IMU tracker!!" << std::endl;
		noHierarchyLevels = 2;
		trackingRegime = new TrackerIterationType[noHierarchyLevels];

		trackingRegime[0] = TRACKER_ITERATION_BOTH;
		trackingRegime[1] = TRACKER_ITERATION_TRANSLATION;
	    //trackingRegime[2] = TRACKER_ITERATION_TRANSLATION;
	} else if (trackerType == TRACKER_ICP_ODOMETRY || trackerType == TRACKER_ICP_ODOMETRY_COLOR || trackerType == TRACKER_WICP_ODOMETRY_COLOR)
  {
//    std::cout << "Setting ODOM tracker!!" << std::endl;
    noHierarchyLevels = 3;
    trackingRegime = new TrackerIterationType[noHierarchyLevels];

    trackingRegime[0] = TRACKER_ITERATION_BOTH;
    trackingRegime[1] = TRACKER_ITERATION_BOTH;
    trackingRegime[2] = TRACKER_ITERATION_ROTATION;
  } else if (trackerType == TRACKER_STRICT_ODOMETRY)
  {
//    std::cout << "Setting strict ODOM tracker!!" << std::endl;
    noHierarchyLevels = 0;
    trackingRegime = new TrackerIterationType[noHierarchyLevels];
  }
	else
	{
//	  std::cout << "Setting ICP tracker!!" << std::endl;
		noHierarchyLevels = 5;
		trackingRegime = new TrackerIterationType[noHierarchyLevels];

		trackingRegime[0] = TRACKER_ITERATION_BOTH;
		trackingRegime[1] = TRACKER_ITERATION_BOTH;
		trackingRegime[2] = TRACKER_ITERATION_ROTATION;
		trackingRegime[3] = TRACKER_ITERATION_ROTATION;
		trackingRegime[4] = TRACKER_ITERATION_ROTATION;
	}

	if (trackerType == TRACKER_REN) noICPRunTillLevel = 1;
	else noICPRunTillLevel = 0;

	if ((trackerType == TRACKER_COLOR || trackerType == TRACKER_ICP_COLOR || trackerType == TRACKER_ICP_ODOMETRY_COLOR || trackerType == TRACKER_WICP_ODOMETRY_COLOR) && (!ITMVoxel::hasColorInformation)) {
		printf("Error: Color tracker requires a voxel type with color information!\n");
	}
}

ITMLibSettings::~ITMLibSettings()
{
	delete[] trackingRegime;
}

void ITMLibSettings::setTrackerType(const TrackerType& new_tracker_type) {
  trackerType = new_tracker_type;

  /// model the sensor noise as  the weight for weighted ICP
  modelSensorNoise = false;
  if (trackerType == TRACKER_WICP || trackerType == TRACKER_WICP_ODOMETRY_COLOR) modelSensorNoise = true;

  // builds the tracking regime. level 0 is full resolution
  if (trackerType == TRACKER_IMU)
  {
    std::cout << "Setting IMU tracker!!" << std::endl;
    noHierarchyLevels = 2;
    trackingRegime = new TrackerIterationType[noHierarchyLevels];

    trackingRegime[0] = TRACKER_ITERATION_BOTH;
    trackingRegime[1] = TRACKER_ITERATION_TRANSLATION;
      //trackingRegime[2] = TRACKER_ITERATION_TRANSLATION;
  } else if (trackerType == TRACKER_ICP_ODOMETRY || trackerType == TRACKER_ICP_ODOMETRY_COLOR || trackerType == TRACKER_WICP_ODOMETRY_COLOR)
  {
    std::cout << "Setting ODOM tracker!!" << std::endl;
    noHierarchyLevels = 3;
    trackingRegime = new TrackerIterationType[noHierarchyLevels];

    trackingRegime[0] = TRACKER_ITERATION_BOTH;
    trackingRegime[1] = TRACKER_ITERATION_BOTH;
    trackingRegime[2] = TRACKER_ITERATION_ROTATION;
  } else if (trackerType == TRACKER_STRICT_ODOMETRY)
  {
    std::cout << "Setting strict ODOM tracker!!" << std::endl;
    noHierarchyLevels = 0;
    trackingRegime = new TrackerIterationType[noHierarchyLevels];
  }
  else
  {
    std::cout << "Setting default tracker regime!!" << std::endl;
    noHierarchyLevels = 5;
    trackingRegime = new TrackerIterationType[noHierarchyLevels];

    trackingRegime[0] = TRACKER_ITERATION_BOTH;
    trackingRegime[1] = TRACKER_ITERATION_BOTH;
    trackingRegime[2] = TRACKER_ITERATION_ROTATION;
    trackingRegime[3] = TRACKER_ITERATION_ROTATION;
    trackingRegime[4] = TRACKER_ITERATION_ROTATION;
  }

  if (trackerType == TRACKER_REN) noICPRunTillLevel = 1;
  else noICPRunTillLevel = 0;

  if ((trackerType == TRACKER_COLOR || trackerType == TRACKER_ICP_COLOR || trackerType == TRACKER_ICP_ODOMETRY_COLOR || trackerType == TRACKER_WICP_ODOMETRY_COLOR) && (!ITMVoxel::hasColorInformation)) {
    printf("Error: Color tracker requires a voxel type with color information!\n");
  }
}

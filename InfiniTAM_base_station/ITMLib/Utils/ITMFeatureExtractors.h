/*
 * ITMFeatureExtractors.h
 *
 *  Created on: Nov 26, 2016
 *      Author: anurag
 *      Utilizing original code from DLoopDetector (Author: Dorian Galvez-Lopez)
 */

#ifndef INFINITAM_INFINITAM_ITMLIB_UTILS_ITMFEATUREEXTRACTORS_H_
#define INFINITAM_INFINITAM_ITMLIB_UTILS_ITMFEATUREEXTRACTORS_H_

#include "DBoW2/DBoW2.h"
#include "DBoW2/FSurf64.h"
#include "DBoW2/FBrief.h"
#include "DUtils/DUtils.h"
#include "DUtilsCV/DUtilsCV.h"
#include "DVision/DVision.h"

#include <iostream>
#include <vector>
#include <string>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "TemplatedLoopDetector.h"

using namespace DLoopDetector;
using namespace DBoW2;
using namespace DVision;

/// Generic class to create functors to extract features
template<class TDescriptor>
class FeatureExtractor
{
public:
  /**
   * Extracts features
   * @param im image
   * @param keys keypoints extracted
   * @param descriptors descriptors extracted
   */
  virtual void operator()(const cv::Mat &im,
    std::vector<cv::KeyPoint> &keys, std::vector<TDescriptor> &descriptors) const = 0;
};


/// This functor extracts BRIEF descriptors in the required format
class BriefExtractor: public FeatureExtractor<FBrief::TDescriptor>
{
 private:
   /// BRIEF descriptor extractor
   DVision::BRIEF m_brief;

 public:
  /**
   * Extracts features from an image
   * @param im image
   * @param keys keypoints extracted
   * @param descriptors descriptors extracted
   */
  void operator()(const cv::Mat &im,
    std::vector<cv::KeyPoint> &keys, std::vector<BRIEF::bitset> &descriptors) const {
    // extract FAST keypoints with opencv
    const int fast_th = 20; // corner detector response threshold
    cv::FAST(im, keys, fast_th, true);

    // compute their BRIEF descriptor
    m_brief.compute(im, keys, descriptors);
  }

  /**
   * Creates the brief extractor with the given pattern file
   * @param pattern_file
   */
  BriefExtractor(const std::string &pattern_file) {
    // The DVision::BRIEF extractor computes a random pattern by default when
    // the object is created.
    // We load the pattern that we used to build the vocabulary, to make
    // the descriptors compatible with the predefined vocabulary

    // loads the pattern
    cv::FileStorage fs(pattern_file.c_str(), cv::FileStorage::READ);
    if(!fs.isOpened()) throw std::string("Could not open file ") + pattern_file;

    std::vector<int> x1, y1, x2, y2;
    fs["x1"] >> x1;
    fs["x2"] >> x2;
    fs["y1"] >> y1;
    fs["y2"] >> y2;

    m_brief.importPairs(x1, y1, x2, y2);
  }
};


/// This functor extracts SURF64 descriptors in the required format
class SurfExtractor: public FeatureExtractor<FSurf64::TDescriptor>
{
public:
  /**
   * Extracts features from an image
   * @param im image
   * @param keys keypoints extracted
   * @param descriptors descriptors extracted
   */
  void operator()(const cv::Mat &im,
      std::vector<cv::KeyPoint> &keys, std::vector<std::vector<float> > &descriptors) const {
    // extract surfs with opencv
    static cv::Ptr<cv::SURF> surf_detector(new cv::SURF(400));

    //surf_detector->setExtended(false);

    keys.clear(); // opencv 2.4 does not clear the vector
    cv::Mat desc;
    surf_detector->detect(im, keys);
    surf_detector->compute(im, keys, desc);
    std::vector<float> plain(desc.rows*desc.cols);
    plain.assign((float*)desc.datastart, (float*)desc.dataend);

    // change descriptor format
    const int L = surf_detector->descriptorSize();
    descriptors.resize(desc.rows);

    unsigned int j = 0;
    for(unsigned int i = 0; i < plain.size(); i += L, ++j)
    {
      descriptors[j].resize(L);
      std::copy(plain.begin() + i, plain.begin() + i + L, descriptors[j].begin());
    }
  }
};

#endif /* INFINITAM_INFINITAM_ITMLIB_UTILS_ITMFEATUREEXTRACTORS_H_ */

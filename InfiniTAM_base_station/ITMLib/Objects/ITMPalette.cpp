/*
 * ITMPalette.cpp
 *
 *  Created on: Nov 8, 2016
 *      Author: anurag
 */

#include "ITMPalette.h"

using namespace ITMLib::Objects;

#define DEG2RAD 0.01745329
void ITMPalette::SetPaletteType(palettetypes type) {

  int i, r, g, b;
  float f;

  switch (type)
  {
    case ITMPalette::Linear_red_palettes:
      /*
       * Linear red palettes.
       */
      for (i = 0; i < 256; i++)
      {
        colors[i].rgbBlue = 0;
        colors[i].rgbGreen = 0;
        colors[i].rgbRed = i;
      }
      break;
    case ITMPalette::GammaLog_red_palettes:
      /*
       * GammaLog red palettes.
       */
      for (i = 0; i < 256; i++)
      {
        f = log10(pow((i / 255.0), 1.0) * 9.0 + 1.0) * 255.0;
        colors[i].rgbBlue = 0;
        colors[i].rgbGreen = 0;
        colors[i].rgbRed = f;
      }
      break;
    case ITMPalette::Inversion_red_palette:
      /*
       * Inversion red palette.
       */
      for (i = 0; i < 256; i++)
      {
        colors[i].rgbBlue = 0;
        colors[i].rgbGreen = 0;
        colors[i].rgbRed = 255 - i;
      }
      break;
    case ITMPalette::Linear_palettes:
      /*
       * Linear palettes.
       */
      for (i = 0; i < 256; i++)
      {
        colors[i].rgbBlue = colors[i].rgbGreen = colors[i].rgbRed = i;
      }
      break;
    case ITMPalette::GammaLog_palettes:
      /*
       * GammaLog palettes.
       */
      for (i = 0; i < 256; i++)
      {
        f = log10(pow((i / 255.0), 1.0) * 9.0 + 1.0) * 255.0;
        colors[i].rgbBlue = colors[i].rgbGreen = colors[i].rgbRed = f;
      }
      break;
    case ITMPalette::Inversion_palette:
      /*
       * Inversion palette.
       */
      for (i = 0; i < 256; i++)
      {
        colors[i].rgbBlue = colors[i].rgbGreen = colors[i].rgbRed = 255 - i;
      }
      break;
    case ITMPalette::False_color_palette1:
      /*
       * False color palette #1.
       */
      for (i = 0; i < 256; i++)
      {
        r = (sin((i / 255.0 * 360.0 - 120.0 > 0 ? i / 255.0 * 360.0 - 120.0 : 0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        g = (sin((i / 255.0 * 360.0 + 60.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        b = (sin((i / 255.0 * 360.0 + 140.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        colors[i].rgbBlue = b;
        colors[i].rgbGreen = g;
        colors[i].rgbRed = r;
      }
      break;
    case ITMPalette::False_color_palette2:
      /*
       * False color palette #2.
       */
      for (i = 0; i < 256; i++)
      {
        r = (sin((i / 255.0 * 360.0 + 120.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        g = (sin((i / 255.0 * 360.0 + 240.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        b = (sin((i / 255.0 * 360.0 + 0.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        colors[i].rgbBlue = b;
        colors[i].rgbGreen = g;
        colors[i].rgbRed = r;
      }
      break;
    case ITMPalette::False_color_palette3:
      /*
       * False color palette #3.
       */
      for (i = 0; i < 256; i++)
      {
        r = (sin((i / 255.0 * 360.0 + 240.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        g = (sin((i / 255.0 * 360.0 + 0.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        b = (sin((i / 255.0 * 360.0 + 120.0) * DEG2RAD) * 0.5 + 0.5) * 255.0;
        colors[i].rgbBlue = b;
        colors[i].rgbGreen = g;
        colors[i].rgbRed = r;
      }
      break;

    case ITMPalette::False_color_palette4:
      /*
       * False color palette #4. Matlab JET
       */

      enum
      {
        nsep = 64, nvals = 192, n = 256
      };

      std::vector<double> vals;
      vals.resize(nvals, 0);

      int idx = 0;
      for (int i = 0; i < nsep; ++i)
      {
        vals.at(idx++) = (i / (double)nsep);
      }

      for (int i = 0; i < nsep; ++i){
        vals.at(idx + i) = 1.;
      }

      idx += nsep;
      for (int i = nsep - 1; i >= 0; --i)
      {
        vals.at(idx++) = i / (double)nsep;
      }

      std::vector<int> r;
      r.resize(nvals);
      std::vector<int> g;
      g.resize(nvals);
      std::vector<int> b;
      b.resize(nvals);
      for (std::size_t i = 0; i < nvals; ++i)
      {
        g.at(i) = ceil(nsep / 2) - 1 + i;
        r.at(i) = g.at(i) + nsep;
        b.at(i) = g.at(i) - nsep;
      }

      int idxr = 0;
      int idxg = 0;

      for (int i = 0; i < nvals; ++i)
      {
        if (r.at(i) >= 0 && r.at(i) < n)
          colors[r.at(i)].rgbRed = vals.at(idxr++) * 255.;

        if (g.at(i) >= 0 && g.at(i) < n)
          colors[g.at(i)].rgbGreen = vals.at(idxg++) * 255.;
      }

      int idxb = 0;
      int cntblue = 0;
      for (int i = 0; i < nvals; ++i)
      {
        if (b.at(i) >= 0 && b.at(i) < n)
          cntblue++;
      }

      for (int i = 0; i < nvals; ++i)
      {
        if (b.at(i) >= 0 && b.at(i) < n)
          colors[b.at(i)].rgbBlue = vals.at(nvals - 1 - cntblue + idxb++) * 255.;
      }
      break;
  }
}
#undef DEG2RAD

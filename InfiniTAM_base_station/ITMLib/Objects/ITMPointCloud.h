// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../Utils/ITMLibDefines.h"
#include "../../ORUtils/Image.h"

#include <stdlib.h>

namespace ITMLib
{
	namespace Objects
	{
		class ITMPointCloud
		{
		public:
			uint noTotalPoints, noTotalInactivePoints;

			ORUtils::Image<Vector4f> *locations, *colours, *inactive_locations, *remapped_inactive_locations, *filtered_inactive_locations, *inactive_colours;
			// reliability values of inactive voxels, remapped: carry-on from previous iteration
			ORUtils::Image<float> *inactive_reliability, *remapped_inactive_reliability;
			// number of times each inactive pixel location was updated
			ORUtils::Image<short> *inactive_update_count, *remapped_inactive_update_count;

			explicit ITMPointCloud(Vector2i imgSize, MemoryDeviceType memoryType)
			{
				this->noTotalPoints = 0;
				this->noTotalInactivePoints = 0;

				locations = new ORUtils::Image<Vector4f>(imgSize, memoryType);
//				inactive_locations = new ORUtils::Image<Vector4f>(imgSize, memoryType);
				inactive_locations = new ORUtils::Image<Vector4f>(imgSize, true, memoryType == MEMORYDEVICE_CUDA);
				filtered_inactive_locations = new ORUtils::Image<Vector4f>(imgSize, memoryType);
//				inactive_update_count = new ORUtils::Image<short>(imgSize, memoryType);
				inactive_update_count = new ORUtils::Image<short>(imgSize, true, memoryType == MEMORYDEVICE_CUDA);
//				remapped_inactive_update_count = new ORUtils::Image<short>(imgSize, memoryType);
				remapped_inactive_update_count = new ORUtils::Image<short>(imgSize, true, memoryType == MEMORYDEVICE_CUDA);
//				inactive_sdf = new ORUtils::Image<float>(imgSize, memoryType);
				inactive_reliability = new ORUtils::Image<float>(imgSize, true, memoryType == MEMORYDEVICE_CUDA);
//				remapped_inactive_locations = new ORUtils::Image<Vector4f>(imgSize, memoryType);
				remapped_inactive_locations = new ORUtils::Image<Vector4f>(imgSize, true, memoryType == MEMORYDEVICE_CUDA);
//				remapped_inactive_sdf = new ORUtils::Image<float>(imgSize, memoryType);
				remapped_inactive_reliability = new ORUtils::Image<float>(imgSize, true, memoryType == MEMORYDEVICE_CUDA);
				colours = new ORUtils::Image<Vector4f>(imgSize, memoryType);
				inactive_colours = new ORUtils::Image<Vector4f>(imgSize, memoryType);
			}

			void UpdateHostFromDevice()
			{
				this->locations->UpdateHostFromDevice();
				this->inactive_locations->UpdateHostFromDevice();
				this->filtered_inactive_locations->UpdateHostFromDevice();
				this->inactive_update_count->UpdateHostFromDevice();
				this->remapped_inactive_update_count->UpdateHostFromDevice();
				this->inactive_reliability->UpdateHostFromDevice();
				this->remapped_inactive_locations->UpdateHostFromDevice();
				this->remapped_inactive_reliability->UpdateHostFromDevice();
				this->colours->UpdateHostFromDevice();
				this->inactive_colours->UpdateHostFromDevice();
			}

			void UpdateDeviceFromHost()
			{
				this->locations->UpdateDeviceFromHost();
				this->inactive_locations->UpdateDeviceFromHost();
				this->filtered_inactive_locations->UpdateDeviceFromHost();
				this->inactive_update_count->UpdateDeviceFromHost();
				this->remapped_inactive_update_count->UpdateDeviceFromHost();
				this->inactive_reliability->UpdateDeviceFromHost();
				this->remapped_inactive_locations->UpdateDeviceFromHost();
				this->remapped_inactive_reliability->UpdateDeviceFromHost();
				this->colours->UpdateDeviceFromHost();
				this->inactive_colours->UpdateDeviceFromHost();
			}

			~ITMPointCloud()
			{
				delete locations;
				delete inactive_locations;
				delete filtered_inactive_locations;
				delete inactive_update_count;
				delete remapped_inactive_update_count;
				delete inactive_reliability;
				delete remapped_inactive_locations;
				delete remapped_inactive_reliability;
				delete colours;
				delete inactive_colours;
			}

			// Suppress the default copy constructor and assignment operator
			ITMPointCloud(const ITMPointCloud&);
			ITMPointCloud& operator=(const ITMPointCloud&);
		};
	}
}

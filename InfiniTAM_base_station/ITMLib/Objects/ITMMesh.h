// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include "../Utils/ITMLibDefines.h"
#include "../../ORUtils/Image.h"

#include <ros/ros.h>
#include <shape_msgs/Mesh.h>
#include <shape_msgs/MeshTriangle.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/PointCloud2.h>

#include <stdlib.h>

namespace ITMLib
{
	namespace Objects
	{
		class ITMMesh
		{
		private:
		  // ROS
      ros::NodeHandle nh_;
      ros::Publisher pubMesh_, pubPcl_;
      int32_t meshId_;
		public:
			struct Triangle { Vector3f p0, p1, p2; Vector3u c0, c1, c2;};
		
			MemoryDeviceType memoryType;

			uint noTotalTriangles;
			bool hasColour;
			static const uint noMaxTriangles = SDF_LOCAL_BLOCK_NUM * 32;

			ORUtils::MemoryBlock<Triangle> *triangles;

			explicit ITMMesh(MemoryDeviceType memoryType, bool hasColourInformation)
			{
				this->memoryType = memoryType;
				this->noTotalTriangles = 0;
				this->hasColour = hasColourInformation;

				triangles = new ORUtils::MemoryBlock<Triangle>(noMaxTriangles, memoryType);

				// ROS
        pubMesh_ = nh_.advertise<visualization_msgs::Marker>("itm/mesh", 1);
        pubPcl_ = nh_.advertise<sensor_msgs::PointCloud2>("itm/pcl", 1);
			}

			void publishROSMesh(visualization_msgs::Marker *meshMsg) {
			  ORUtils::MemoryBlock<Triangle> *cpu_triangles; bool shoulDelete = false;
        if (memoryType == MEMORYDEVICE_CUDA)
        {
          cpu_triangles = new ORUtils::MemoryBlock<Triangle>(noMaxTriangles, MEMORYDEVICE_CPU);
          cpu_triangles->SetFrom(triangles, ORUtils::MemoryBlock<Triangle>::CUDA_TO_CPU);
          shoulDelete = true;
        }
        else cpu_triangles = triangles;

        Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);

        // Set the frame ID and timestamp.  See the TF tutorials for information on these.
        meshMsg->header.frame_id = "infinitam_global";
        meshMsg->header.stamp = ros::Time::now();
        // Set the namespace and id for this marker.  This serves to create a unique ID
        // Any marker sent with the same namespace and id will overwrite the old one
        meshMsg->ns = "inactive mesh";
        meshMsg->id = meshId_;
        meshId_++;
        // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
        meshMsg->type = visualization_msgs::Marker::TRIANGLE_LIST;
        // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
        meshMsg->action = visualization_msgs::Marker::ADD;
        meshMsg->lifetime = ros::Duration();
        meshMsg->pose.position.x = 0.0;
        meshMsg->pose.position.y = 0.0;
        meshMsg->pose.position.z = 0.0;
        meshMsg->pose.orientation.x = 0.0;
        meshMsg->pose.orientation.y = 0.0;
        meshMsg->pose.orientation.z = 0.0;
        meshMsg->pose.orientation.w = 1.0;
        // Set the scale of the marker -- 1x1x1 here means 1m on a side
        meshMsg->scale.x = 1.0;
        meshMsg->scale.y = 1.0;
        meshMsg->scale.z = 1.0;
        // Set the default colour = blue
        meshMsg->color.r = 0.0f;
        meshMsg->color.g = 0.0f;
        meshMsg->color.b = 1.0f;
        meshMsg->color.a = 1.0;

//        std::cout << "no inactive triangle: " << noTotalTriangles << std::endl;
        geometry_msgs::Point p_temp1, p_temp2, p_temp3;
        std_msgs::ColorRGBA c_temp1, c_temp2, c_temp3;
        for (uint i = 0; i < noTotalTriangles; i++)
        {
          // vertex 1
          p_temp1.x = triangleArray[i].p0.x;
          p_temp1.y = triangleArray[i].p0.y;
          p_temp1.z = triangleArray[i].p0.z;
          if (hasColour) {
            c_temp1.r = (triangleArray[i].c0.r)/255.0f;
            c_temp1.g = (triangleArray[i].c0.g)/255.0f;
            c_temp1.b = (triangleArray[i].c0.b)/255.0f;
            c_temp1.a = 1.0;
          }

          // vertex 2
          p_temp2.x = triangleArray[i].p1.x;
          p_temp2.y = triangleArray[i].p1.y;
          p_temp2.z = triangleArray[i].p1.z;
          if (hasColour) {
            c_temp2.r = (triangleArray[i].c1.r)/255.0f;
            c_temp2.g = (triangleArray[i].c1.g)/255.0f;
            c_temp2.b = (triangleArray[i].c1.b)/255.0f;
            c_temp2.a = 1.0;
          }

          // vertex 3
          p_temp3.x = triangleArray[i].p2.x;
          p_temp3.y = triangleArray[i].p2.y;
          p_temp3.z = triangleArray[i].p2.z;
          if (hasColour) {
            c_temp3.r = (triangleArray[i].c2.r)/255.0f;
            c_temp3.g = (triangleArray[i].c2.g)/255.0f;
            c_temp3.b = (triangleArray[i].c2.b)/255.0f;
            c_temp3.a = 1.0;
          }

          // reject triangles with black vertices
          if ((c_temp1.r + c_temp1.g + c_temp1.b > 0.0 && c_temp2.r + c_temp2.g + c_temp2.b > 0.0 &&
               c_temp3.r + c_temp3.g + c_temp3.b > 0.0 && hasColour) ||
              !hasColour) {
            meshMsg->points.push_back(p_temp1);
            meshMsg->colors.push_back(c_temp1);

            meshMsg->points.push_back(p_temp2);
            meshMsg->colors.push_back(c_temp2);

            meshMsg->points.push_back(p_temp3);
            meshMsg->colors.push_back(c_temp3);
          }
        }

        pubMesh_.publish(*meshMsg);

        if (shoulDelete) delete cpu_triangles;
			}

      void publishROSPcl() {
        ORUtils::MemoryBlock<Triangle> *cpu_triangles; bool shoulDelete = false;
        if (memoryType == MEMORYDEVICE_CUDA)
        {
          cpu_triangles = new ORUtils::MemoryBlock<Triangle>(noMaxTriangles, MEMORYDEVICE_CPU);
          cpu_triangles->SetFrom(triangles, ORUtils::MemoryBlock<Triangle>::CUDA_TO_CPU);
          shoulDelete = true;
        }
        else cpu_triangles = triangles;

        Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);

        sensor_msgs::PointCloud2 pclMsg;
        // Set the frame ID and timestamp.  See the TF tutorials for information on these.
        pclMsg.header.frame_id = "infinitam_global";
        pclMsg.height = 1;                 // Unordered point cloud.
        pclMsg.width  = noTotalTriangles*3;  // Number of features/points.
        pclMsg.header.stamp = ros::Time::now();
        const int nFieldsPcl = 5;
        std::string namePcl[nFieldsPcl] = {"id","rgb","x","y","z"};
        int sizePcl[nFieldsPcl] = {4,4,4,4,4};
        int countPcl[nFieldsPcl] = {1,1,1,1,1};
        int datatypePcl[nFieldsPcl] = {sensor_msgs::PointField::INT32,sensor_msgs::PointField::UINT32,
            sensor_msgs::PointField::FLOAT32,sensor_msgs::PointField::FLOAT32,sensor_msgs::PointField::FLOAT32};
        pclMsg.fields.resize(nFieldsPcl);
        int byteCounter = 0;
        for(int i=0;i<nFieldsPcl;i++){
          pclMsg.fields[i].name     = namePcl[i];
          pclMsg.fields[i].offset   = byteCounter;
          pclMsg.fields[i].count    = countPcl[i];
          pclMsg.fields[i].datatype = datatypePcl[i];
          byteCounter += sizePcl[i]*countPcl[i];
        }
        pclMsg.point_step = byteCounter;
        pclMsg.row_step = pclMsg.point_step * pclMsg.width;
        pclMsg.data.resize(pclMsg.row_step * pclMsg.height);
        pclMsg.is_dense = false;
        int offset = 0;

        uint32_t rgb;
        for (uint i = 0; i < noTotalTriangles; i++, offset += pclMsg.point_step)
        {
          rgb = static_cast<uint8_t>(triangleArray[i].c0.r) << 16 |
                static_cast<uint8_t>(triangleArray[i].c0.g) << 8 |
                static_cast<uint8_t>(triangleArray[i].c0.b);
          memcpy(&pclMsg.data[offset + pclMsg.fields[0].offset], &meshId_, sizeof(int));  // id
          memcpy(&pclMsg.data[offset + pclMsg.fields[1].offset], &rgb, sizeof(uint32_t));  // rgb
          memcpy(&pclMsg.data[offset + pclMsg.fields[2].offset], &triangleArray[i].p0.x, sizeof(float));  // x
          memcpy(&pclMsg.data[offset + pclMsg.fields[3].offset], &triangleArray[i].p0.y, sizeof(float));  // y
          memcpy(&pclMsg.data[offset + pclMsg.fields[4].offset], &triangleArray[i].p0.z, sizeof(float));  // z
          offset += pclMsg.point_step;

          rgb = static_cast<uint8_t>(triangleArray[i].c1.r) << 16 |
                static_cast<uint8_t>(triangleArray[i].c1.g) << 8 |
                static_cast<uint8_t>(triangleArray[i].c1.b);
          memcpy(&pclMsg.data[offset + pclMsg.fields[0].offset], &meshId_, sizeof(int));  // id
          memcpy(&pclMsg.data[offset + pclMsg.fields[1].offset], &rgb, sizeof(uint32_t));  // rgb
          memcpy(&pclMsg.data[offset + pclMsg.fields[2].offset], &triangleArray[i].p1.x, sizeof(float));  // x
          memcpy(&pclMsg.data[offset + pclMsg.fields[3].offset], &triangleArray[i].p1.y, sizeof(float));  // y
          memcpy(&pclMsg.data[offset + pclMsg.fields[4].offset], &triangleArray[i].p1.z, sizeof(float));  // z
          offset += pclMsg.point_step;

          rgb = static_cast<uint8_t>(triangleArray[i].c2.r) << 16 |
                static_cast<uint8_t>(triangleArray[i].c2.g) << 8 |
                static_cast<uint8_t>(triangleArray[i].c2.b);
          memcpy(&pclMsg.data[offset + pclMsg.fields[0].offset], &meshId_, sizeof(int));  // id
          memcpy(&pclMsg.data[offset + pclMsg.fields[1].offset], &rgb, sizeof(uint32_t));  // rgb
          memcpy(&pclMsg.data[offset + pclMsg.fields[2].offset], &triangleArray[i].p2.x, sizeof(float));  // x
          memcpy(&pclMsg.data[offset + pclMsg.fields[3].offset], &triangleArray[i].p2.y, sizeof(float));  // y
          memcpy(&pclMsg.data[offset + pclMsg.fields[4].offset], &triangleArray[i].p2.z, sizeof(float));  // z
        }
        pubPcl_.publish(pclMsg);

        if (shoulDelete) delete cpu_triangles;
      }

			void WriteOBJ(const char *fileName)
			{
				ORUtils::MemoryBlock<Triangle> *cpu_triangles; bool shoulDelete = false;
				if (memoryType == MEMORYDEVICE_CUDA)
				{
					cpu_triangles = new ORUtils::MemoryBlock<Triangle>(noMaxTriangles, MEMORYDEVICE_CPU);
					cpu_triangles->SetFrom(triangles, ORUtils::MemoryBlock<Triangle>::CUDA_TO_CPU);
					shoulDelete = true;
				}
				else cpu_triangles = triangles;

				Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);

				FILE *f = fopen(fileName, "w+");
				if (f != NULL)
				{
					for (uint i = 0; i < noTotalTriangles; i++)
					{
						fprintf(f, "v %f %f %f\n", triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z);
						fprintf(f, "v %f %f %f\n", triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z);
						fprintf(f, "v %f %f %f\n", triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z);
					}

					for (uint i = 0; i<noTotalTriangles; i++) fprintf(f, "f %d %d %d\n", i * 3 + 2 + 1, i * 3 + 1 + 1, i * 3 + 0 + 1);
					fclose(f);
				}

				if (shoulDelete) delete cpu_triangles;
			}

			void WritePLY(const char *fileName)
			{
			  ORUtils::MemoryBlock<Triangle> *cpu_triangles; bool shoulDelete = false;
        if (memoryType == MEMORYDEVICE_CUDA)
        {
          cpu_triangles = new ORUtils::MemoryBlock<Triangle>(noMaxTriangles, MEMORYDEVICE_CPU);
          cpu_triangles->SetFrom(triangles, ORUtils::MemoryBlock<Triangle>::CUDA_TO_CPU);
          shoulDelete = true;
        }
        else cpu_triangles = triangles;

        Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);

//        FILE *f = fopen(fileName, "w+");
        FILE *f = fopen(fileName, "wb+");
        if (f != NULL)
        {
          std::cout << "opened mesh file." << std::endl;
          fprintf(f, "ply\n");
//          fprintf(f, "format ascii 1.0\n");
          fprintf(f, "format binary_little_endian 1.0\n");
          fprintf(f, "element vertex %d\n", noTotalTriangles*3);
          fprintf(f, "property float x\n");
          fprintf(f, "property float y\n");
          fprintf(f, "property float z\n");
          if (hasColour) {
            fprintf(f, "property uchar red\n");
            fprintf(f, "property uchar green\n");
            fprintf(f, "property uchar blue\n");
          }
          fprintf(f, "element face %d\n", noTotalTriangles);
          fprintf(f, "property list uchar int vertex_index\n");
          fprintf(f, "end_header\n");

          for (uint i = 0; i < noTotalTriangles; i++)
          {
            if (hasColour) {
//              fprintf(f, "%f %f %f %u %u %u\n", triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z, triangleArray[i].c0.r, triangleArray[i].c0.g, triangleArray[i].c0.b);
              fwrite(triangleArray[i].p0, sizeof(Vector3f), 1, f);
              fwrite(triangleArray[i].c0, sizeof(Vector3u), 1, f);
//              fprintf(f, "%f %f %f %u %u %u\n", triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z, triangleArray[i].c1.r, triangleArray[i].c1.g, triangleArray[i].c1.b);
              fwrite(triangleArray[i].p1, sizeof(Vector3f), 1, f);
              fwrite(triangleArray[i].c1, sizeof(Vector3u), 1, f);
//              fprintf(f, "%f %f %f %u %u %u\n", triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z, triangleArray[i].c2.r, triangleArray[i].c2.g, triangleArray[i].c2.b);
              fwrite(triangleArray[i].p2, sizeof(Vector3f), 1, f);
              fwrite(triangleArray[i].c2, sizeof(Vector3u), 1, f);
            } else {
//              fprintf(f, "%f %f %f\n", triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z);
              fwrite(triangleArray[i].p0, sizeof(Vector3f), 1, f);
//              fprintf(f, "%f %f %f\n", triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z);
              fwrite(triangleArray[i].p1, sizeof(Vector3f), 1, f);
//              fprintf(f, "%f %f %f\n", triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z);
              fwrite(triangleArray[i].p2, sizeof(Vector3f), 1, f);
            }
          }

          // faces
          unsigned char sides = 3;
          for (uint i = 0; i<noTotalTriangles; i++) {
//            fprintf(f, "3 %d %d %d\n", i * 3 + 2, i * 3 + 1, i * 3 + 0);
            Vector3i face(i * 3 + 2, i * 3 + 1, i * 3 + 0);
            fwrite(&sides, sizeof(unsigned char), 1, f);
            fwrite(face, sizeof(Vector3i), 1, f);
          }
          fclose(f);
          std::cout << "done writing to mesh file." << std::endl;
        }

        if (shoulDelete) delete cpu_triangles;
			}

			void WriteSTL(const char *fileName)
			{
				ORUtils::MemoryBlock<Triangle> *cpu_triangles; bool shoulDelete = false;
				if (memoryType == MEMORYDEVICE_CUDA)
				{
					cpu_triangles = new ORUtils::MemoryBlock<Triangle>(noMaxTriangles, MEMORYDEVICE_CPU);
					cpu_triangles->SetFrom(triangles, ORUtils::MemoryBlock<Triangle>::CUDA_TO_CPU);
					shoulDelete = true;
				}
				else cpu_triangles = triangles;

				Triangle *triangleArray = cpu_triangles->GetData(MEMORYDEVICE_CPU);

				FILE *f = fopen(fileName, "wb+");

				if (f != NULL) {
					for (int i = 0; i < 80; i++) fwrite(" ", sizeof(char), 1, f);

					fwrite(&noTotalTriangles, sizeof(int), 1, f);

					float zero = 0.0f; short attribute = 0;
					for (uint i = 0; i < noTotalTriangles; i++)
					{
						fwrite(&zero, sizeof(float), 1, f); fwrite(&zero, sizeof(float), 1, f); fwrite(&zero, sizeof(float), 1, f);

						fwrite(&triangleArray[i].p2.x, sizeof(float), 1, f); 
						fwrite(&triangleArray[i].p2.y, sizeof(float), 1, f); 
						fwrite(&triangleArray[i].p2.z, sizeof(float), 1, f);

						fwrite(&triangleArray[i].p1.x, sizeof(float), 1, f); 
						fwrite(&triangleArray[i].p1.y, sizeof(float), 1, f); 
						fwrite(&triangleArray[i].p1.z, sizeof(float), 1, f);

						fwrite(&triangleArray[i].p0.x, sizeof(float), 1, f);
						fwrite(&triangleArray[i].p0.y, sizeof(float), 1, f);
						fwrite(&triangleArray[i].p0.z, sizeof(float), 1, f);

						fwrite(&attribute, sizeof(short), 1, f);

						//fprintf(f, "v %f %f %f\n", triangleArray[i].p0.x, triangleArray[i].p0.y, triangleArray[i].p0.z);
						//fprintf(f, "v %f %f %f\n", triangleArray[i].p1.x, triangleArray[i].p1.y, triangleArray[i].p1.z);
						//fprintf(f, "v %f %f %f\n", triangleArray[i].p2.x, triangleArray[i].p2.y, triangleArray[i].p2.z);
					}

					//for (uint i = 0; i<noTotalTriangles; i++) fprintf(f, "f %d %d %d\n", i * 3 + 2 + 1, i * 3 + 1 + 1, i * 3 + 0 + 1);
					fclose(f);
				}

				if (shoulDelete) delete cpu_triangles;
			}

			~ITMMesh()
			{
				delete triangles;
			}

			// Suppress the default copy constructor and assignment operator
			ITMMesh(const ITMMesh&);
			ITMMesh& operator=(const ITMMesh&);
		};
	}
}

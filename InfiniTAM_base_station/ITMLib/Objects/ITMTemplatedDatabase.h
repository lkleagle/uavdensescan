/*
 * ITMTemplatedDatabase.h
 *
 *  Created on: Nov 29, 2016
 *      Author: anurag
 */

#ifndef INFINITAM_INFINITAM_ITMLIB_OBJECTS_ITMTEMPLATEDDATABASE_H_
#define INFINITAM_INFINITAM_ITMLIB_OBJECTS_ITMTEMPLATEDDATABASE_H_

#include "DBoW2/TemplatedDatabase.h"
#include "ITMPose.h"

using namespace DBoW2;

namespace ITMLib
{
  namespace Objects
  {
    /// @param TDescriptor class of descriptor
    /// @param F class of descriptor functions
    template<class TDescriptor, class F>
    /// Generic Database from DBoW2 modified for use in InfiniTAM.
    /// ITMPoses of camera are also indexed together with images.
    class ITMTemplatedDatabase : public TemplatedDatabase<TDescriptor, F>
    {
     public:

      using typename TemplatedDatabase<TDescriptor, F>::IFRow;
      using typename TemplatedDatabase<TDescriptor, F>::IFPair;

      /**
       * Creates an empty database without vocabulary
       * @param use_di a direct index is used to store feature indexes
       * @param di_levels levels to go up the vocabulary tree to select the
       *   node id to store in the direct index when adding images
       */
      explicit ITMTemplatedDatabase(bool use_di = true, int di_levels = 0);

      /**
       * Creates a database with the given vocabulary
       * @param T class inherited from TemplatedVocabulary<TDescriptor, F>
       * @param voc vocabulary
       * @param use_di a direct index is used to store feature indexes
       * @param di_levels levels to go up the vocabulary tree to select the
       *   node id to store in the direct index when adding images
       */
      template<class T>
      explicit ITMTemplatedDatabase(const T &voc, bool use_di = true,
        int di_levels = 0);

      /**
       * Copy constructor. Copies the vocabulary too
       * @param db object to copy
       */
      ITMTemplatedDatabase(const ITMTemplatedDatabase<TDescriptor, F> &db);

      /**
       * Creates the database from a file
       * @param filename
       */
      ITMTemplatedDatabase(const std::string &filename);

      /**
       * Creates the database from a file
       * @param filename
       */
      ITMTemplatedDatabase(const char *filename);

      virtual ~ITMTemplatedDatabase(void) {
        delete this->m_voc;
      };

      /**
       * Copies the given database and its vocabulary
       * @param db database to copy
       */
      ITMTemplatedDatabase<TDescriptor,F>& operator=(
        const ITMTemplatedDatabase<TDescriptor,F> &db);

      /**
       * Allocates some memory for the direct and inverted indexes
       * @param nd number of expected image entries in the database
       * @param ni number of expected words per image
       * @note Use 0 to ignore a parameter
       */
      void allocate(int nd = 0, int ni = 0);

      /**
       * Adds an entry to the database and returns its index
       * @param features features of the new entry
       * @param bowvec if given, the bow vector of these features is returned
       * @param fvec if given, the vector of nodes and feature indexes is returned
       * @param pose if given, camera pose to add to the entry
       * @return id of new entry
       */
      EntryId add(const std::vector<TDescriptor> &features, const ITMPose &pose,
        BowVector *bowvec = NULL, FeatureVector *fvec = NULL);

      /**
       * Adss an entry to the database and returns its index
       * @param vec bow vector
       * @param fec feature vector to add the entry. Only necessary if using the
       *   direct index
       * @return id of new entry
       */
      EntryId add(const ITMPose &pose, const BowVector &vec,
        const FeatureVector &fec = FeatureVector());

      /**
       * Returns the ITMPose associated with a database entry
       * @param id entry id (must be < size())
       * @return const reference to camera pose in
       *   the given entry
       */
      const ITMPose& retrievePose(EntryId id) const;

      /**
       * Empties the database
       */
      inline void clear();

     protected:

      /// Direct index
      typedef std::vector<ITMPose> DirectFilePoses;
      // DirectFilePoses[entry_id] --> [ directentrypose, ... ]

      /// Direct file for poses (resized for allocation)
      DirectFilePoses m_dfile_poses;
    };

    template<class TDescriptor, class F>
    ITMTemplatedDatabase<TDescriptor, F>::ITMTemplatedDatabase(bool use_di, int di_levels)
    : TemplatedDatabase<TDescriptor, F>(use_di, di_levels)
    {}

    template<class TDescriptor, class F>
    template<class T>
    ITMTemplatedDatabase<TDescriptor, F>::ITMTemplatedDatabase(const T &voc, bool use_di, int di_levels)
    : TemplatedDatabase<TDescriptor, F>(voc, use_di, di_levels)
    {}

    template<class TDescriptor, class F>
    ITMTemplatedDatabase<TDescriptor, F>::ITMTemplatedDatabase(const ITMTemplatedDatabase<TDescriptor, F> &db)
    : TemplatedDatabase<TDescriptor, F>(db)
    {}

    template<class TDescriptor, class F>
    ITMTemplatedDatabase<TDescriptor, F>::ITMTemplatedDatabase(const std::string &filename)
    : TemplatedDatabase<TDescriptor, F>(filename)
    {}

    template<class TDescriptor, class F>
    ITMTemplatedDatabase<TDescriptor, F>::ITMTemplatedDatabase(const char *filename)
    : TemplatedDatabase<TDescriptor, F>(filename)
    {}

    template<class TDescriptor, class F>
    ITMTemplatedDatabase<TDescriptor,F>& ITMTemplatedDatabase<TDescriptor,F>::operator=
      (const ITMTemplatedDatabase<TDescriptor,F> &db)
    {
      if(this != &db)
      {
        this->m_dfile = db.m_dfile;
        m_dfile_poses = db.m_dfile_poses;
        this->m_dilevels = db.m_dilevels;
        this->m_ifile = db.m_ifile;
        this->m_nentries = db.m_nentries;
        this->m_use_di = db.m_use_di;
        setVocabulary(*db.m_voc);
      }
      return *this;
    }

    template<class TDescriptor, class F>
    void ITMTemplatedDatabase<TDescriptor, F>::allocate(int nd, int ni)
    {
      // m_ifile already contains |words| items
      if(ni > 0)
      {
        typename std::vector<IFRow>::iterator rit;
        for(rit = this->m_ifile.begin(); rit != this->m_ifile.end(); ++rit)
        {
          int n = (int)rit->size();
          if(ni > n)
          {
            rit->resize(ni);
            rit->resize(n);
          }
        }
      }

      if(this->m_use_di && (int)this->m_dfile.size() < nd)
      {
        this->m_dfile.resize(nd);
        m_dfile_poses.resize(nd);
      }
    }

    template<class TDescriptor, class F>
    EntryId ITMTemplatedDatabase<TDescriptor, F>::add(
      const std::vector<TDescriptor> &features, const ITMPose &pose,
      BowVector *bowvec, FeatureVector *fvec)
    {
      BowVector aux;
      BowVector& v = (bowvec ? *bowvec : aux);

      if(this->m_use_di && fvec != NULL)
      {
        this->m_voc->transform(features, v, *fvec, this->m_dilevels); // with features
        return add(pose, v, *fvec);
      }
      else if(this->m_use_di)
      {
        FeatureVector fv;
        this->m_voc->transform(features, v, fv, this->m_dilevels); // with features
        return add(pose, v, fv);
      }
      else if(fvec != NULL)
      {
        this->m_voc->transform(features, v, *fvec, this->m_dilevels); // with features
        return add(pose, v);
      }
      else
      {
        this->m_voc->transform(features, v); // with features
        return add(pose, v);
      }
    }

    template<class TDescriptor, class F>
    EntryId ITMTemplatedDatabase<TDescriptor, F>::add(const ITMPose &pose,
        const BowVector &v, const FeatureVector &fv)
    {
      EntryId entry_id = this->m_nentries++;

      BowVector::const_iterator vit;
      std::vector<unsigned int>::const_iterator iit;

      if(this->m_use_di)
      {
        // update direct file
        if(entry_id == this->m_dfile.size())
        {
          this->m_dfile.push_back(fv);
          m_dfile_poses.push_back(pose);
        }
        else
        {
          this->m_dfile[entry_id] = fv;
          m_dfile_poses[entry_id] = pose;
        }
      }

      // update inverted file
      for(vit = v.begin(); vit != v.end(); ++vit)
      {
        const WordId& word_id = vit->first;
        const WordValue& word_weight = vit->second;

        IFRow& ifrow = this->m_ifile[word_id];
        ifrow.push_back(IFPair(entry_id, word_weight));
      }

      return entry_id;

    }

    template<class TDescriptor, class F>
    const ITMPose& ITMTemplatedDatabase<TDescriptor, F>::retrievePose
      (EntryId id) const
    {
      assert(id < this->size());
      return m_dfile_poses[id];
    }

    template<class TDescriptor, class F>
    inline void ITMTemplatedDatabase<TDescriptor, F>::clear()
    {
      // resize vectors
      this->m_ifile.resize(0);
      this->m_ifile.resize(this->m_voc->size());
      this->m_dfile.resize(0);
      m_dfile_poses.resize(0);
      this->m_nentries = 0;
    }

  }
}


#endif /* INFINITAM_INFINITAM_ITMLIB_OBJECTS_ITMTEMPLATEDDATABASE_H_ */

/*
 * ITMPalette.h
 *
 *  Created on: Nov 8, 2016
 *      Author: anurag
 *      Copied from FLIR_tools (ASL)
 */

#ifndef INFINITAM_INFINITAM_ITMLIB_UTILS_ITMPALETTE_H_
#define INFINITAM_INFINITAM_ITMLIB_UTILS_ITMPALETTE_H_

#include <opencv2/core/core.hpp>
#include <opencv2/contrib/retina.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>

#include "../../ORUtils/MemoryBlock.h"
#include "../../ORUtils/Vector.h"
#include "../Utils/ITMLibDefines.h"

namespace ITMLib
{
  namespace Objects
  {
    class ITMPalette
    {
     public:
      struct color
      {
        unsigned char rgbBlue;
        unsigned char rgbGreen;
        unsigned char rgbRed;
        color()
        {
          rgbBlue = rgbGreen = rgbRed = 0;
        }
      };

      enum palettetypes{
        Linear_red_palettes, GammaLog_red_palettes, Inversion_red_palette, Linear_palettes, GammaLog_palettes,
        Inversion_palette, False_color_palette1, False_color_palette2, False_color_palette3, False_color_palette4
      };

//      ORUtils::MemoryBlock<color> *colors;
      ORUtils::VectorX<color, 256> colors;
      MemoryDeviceType memory_type;

      inline color *GetColors(void) { return colors.getValues(); }
      inline const color *GetColors(void) const { return colors.getValues(); }

      explicit ITMPalette(MemoryDeviceType memoryType) {
        memory_type = memoryType;
      }
      ~ITMPalette() { };

      void SetPaletteType(palettetypes type);

      // Suppress the default copy constructor and assignment operator
      ITMPalette(const ITMPalette&);
      ITMPalette& operator=(const ITMPalette&);
    };
  }
}

#endif /* INFINITAM_INFINITAM_ITMLIB_UTILS_ITMPALETTE_H_ */

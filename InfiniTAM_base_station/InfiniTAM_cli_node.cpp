/*
 * InfiniTAM_cli_node.cpp
 *
 *  Created on: Oct 13, 2016
 *      Author: anurag
 */

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include <cstdlib>
#include <ros/ros.h>
#include <boost/assign/list_of.hpp>

#include "Engine/CLIEngine.h"
#include "Engine/ImageSourceEngine.h"
#include "Engine/ROSBagSourceEngine.h"
#include "Engine/ROSImageSourceEngine.h"
#include "Engine/ROSIMUSourceEngine.h"
#include "Engine/ROSOdometrySourceEngine.h"
#include "Engine/OpenNIEngine.h"
#include "Engine/Kinect2Engine.h"
#include "Engine/VISensorEngine.h"
#include "Engine/LibUVCEngine.h"
#include "Engine/RealSenseEngine.h"

#include <ros/package.h>
#include <memory>
#include "rovio/RovioFilter.hpp"
#include "rovio/RovioNode.hpp"

typedef rovio::RovioFilter<rovio::FilterState<ROVIO_NMAXFEATURE,ROVIO_NLEVELS,ROVIO_PATCHSIZE,ROVIO_NCAM,ROVIO_NPOSE>> mtFilter;

using namespace InfiniTAM::Engine;

class ITMIoHandler
{
 private:
  ros::NodeHandle nh_, nh_private_;

  // ROVIO
  // Filter
  std::shared_ptr<mtFilter> rovioFilter_;
  // Node
  rovio::RovioNode<mtFilter> *rovioNode_;

  std::map<std::string, ITMLibSettings::TrackerType> TrackerTypeMap =
      boost::assign::map_list_of("TRACKER_COLOR", ITMLibSettings::TRACKER_COLOR)
                                ("TRACKER_ICP", ITMLibSettings::TRACKER_ICP)
                                ("TRACKER_REN", ITMLibSettings::TRACKER_REN)
                                ("TRACKER_STRICT_ODOMETRY", ITMLibSettings::TRACKER_STRICT_ODOMETRY)
                                ("TRACKER_WICP", ITMLibSettings::TRACKER_WICP)
                                ("TRACKER_IMU", ITMLibSettings::TRACKER_IMU)
                                ("TRACKER_ICP_COLOR", ITMLibSettings::TRACKER_ICP_COLOR)
                                ("TRACKER_ICP_ODOMETRY", ITMLibSettings::TRACKER_ICP_ODOMETRY)
                                ("TRACKER_ICP_ODOMETRY_COLOR", ITMLibSettings::TRACKER_ICP_ODOMETRY_COLOR)
                                ("TRACKER_WICP_ODOMETRY_COLOR", ITMLibSettings::TRACKER_WICP_ODOMETRY_COLOR);

  std::map<std::string, ITMLibSettings::DepthTrackerType> DepthTrackerTypeMap =
      boost::assign::map_list_of("TRACKER_ITM", ITMLibSettings::TRACKER_ITM)
                                ("TRACKER_NABO", ITMLibSettings::TRACKER_NABO)
                                ("TRACKER_LPM", ITMLibSettings::TRACKER_LPM)
                                ("TRACKER_LPM_HIERARCHY", ITMLibSettings::TRACKER_LPM_HIERARCHY);
 public:
  std::string source, calib_file, bag_file, storage_directory, resources_directory;
  bool publish_bag_contents;
  bool use_odometry;
  bool use_imu;
  float viewfrustum_min;
  float viewfrustum_max;
  ITMLibSettings::TrackerType tracker_type;
  float voxel_size;
  bool use_swapping;
  float inactive_delta_time;
  bool visualize_icp;
  bool visualize_lc;
  float inactive_reliability_thresh;
  float num_inactive_threshold_percent;
  ITMLibSettings::DepthTrackerType depth_tracker_type, LC_tracker_type;
  int color_tracker_skip_frame_number;
  int depth_tracker_skip_frame_number;

  bool LoadParameters() {
    bool could_load_params = true;
    std::string tmp;

    could_load_params &= nh_private_.getParam("source", source);
    could_load_params &= nh_private_.getParam("calib_file", calib_file);
    could_load_params &= nh_private_.getParam("bag_file", bag_file);
    could_load_params &= nh_private_.getParam("storage_directory", storage_directory);
    could_load_params &= nh_private_.getParam("resources_directory", resources_directory);
    could_load_params &= nh_private_.getParam("publish_bag_contents", publish_bag_contents);
    could_load_params &= nh_private_.getParam("use_odometry", use_odometry);
    could_load_params &= nh_private_.getParam("use_imu", use_imu);
    could_load_params &= nh_private_.getParam("viewfrustum_min", viewfrustum_min);
    could_load_params &= nh_private_.getParam("viewfrustum_max", viewfrustum_max);
    could_load_params &= nh_private_.getParam("tracker_type", tmp);
    tracker_type = TrackerTypeMap[tmp];
    could_load_params &= nh_private_.getParam("voxel_size", voxel_size);
    could_load_params &= nh_private_.getParam("use_swapping", use_swapping);
    could_load_params &= nh_private_.getParam("inactive_delta_time", inactive_delta_time);
    could_load_params &= nh_private_.getParam("visualize_icp", visualize_icp);
    could_load_params &= nh_private_.getParam("visualize_lc", visualize_lc);
    could_load_params &= nh_private_.getParam("inactive_reliability_thresh", inactive_reliability_thresh);
    could_load_params &= nh_private_.getParam("num_inactive_threshold_percent", num_inactive_threshold_percent);
    could_load_params &= nh_private_.getParam("depth_tracker_type", tmp);
    depth_tracker_type = DepthTrackerTypeMap[tmp];
    could_load_params &= nh_private_.getParam("lc_tracker_type", tmp);
    LC_tracker_type = DepthTrackerTypeMap[tmp];
    could_load_params &= nh_private_.getParam("color_tracker_skip_frame_number", color_tracker_skip_frame_number);
    could_load_params &= nh_private_.getParam("depth_tracker_skip_frame_number", depth_tracker_skip_frame_number);
    return could_load_params;
  }

  /** Create a default source of depth images from a list of command line
      arguments. Typically, @para arg1 would identify the calibration file to
      use, @para arg2 the colour images, @para arg3 the depth images and
      @para arg4 the IMU images. If images are omitted, some live sources will
      be tried.
  */
  void CreateDefaultSources(
      ROSBagSourceEngine* & rosbagSource, ImageSourceEngine* & imageSource, IMUSourceEngine* & imuSource, OdometrySourceEngine* & odomSource,
      const char *imuTopic, const char *odometryTopic, const char *colorTopic,
      const char *depthTopic, const char *externalOdometryTopic,
      const char *rovioCam0Topic, const char *rovioCam1Topic, const char *rovioImuTopic)
  {
    printf("using calibration file: %s\n", calib_file.c_str());

    Vector2i colorSize, depthSize;
    if (source == std::string("kinect") || source == std::string("kinect_ros") || source == std::string("kinect_rosbag")) {
      colorSize = Vector2i(1920, 1080);
      depthSize = Vector2i(512, 424);
    } else if (source == std::string("realsense") || source == std::string("realsense_ros") || source == std::string("realsense_rosbag")) {
      colorSize = Vector2i(640, 480);
      depthSize = Vector2i(480, 360);
    } else if (source == std::string("simulator") || source == std::string("simulator_rosbag")) {
      colorSize = Vector2i(640, 480);
      depthSize = Vector2i(640, 480);
    }


    // OFFLINE: ROSBAG
    if (imageSource == NULL &&
        (source == std::string("kinect_rosbag") || source == std::string("realsense_rosbag") || source == std::string("simulator_rosbag")))
    {
      printf("trying ROSBAG image source\n");
      if (odomSource == NULL && use_odometry) {
        if (odometryTopic != NULL) {
          printf("using Odometry ROS topic: %s\n", odometryTopic);
          rosbagSource = new ROSBagSourceEngine(nh_, nh_private_, calib_file.c_str(), bag_file.c_str(), colorTopic, depthTopic, odometryTopic, externalOdometryTopic,
                                                rovioCam0Topic, rovioCam1Topic, rovioImuTopic,
                                                colorSize, depthSize, publish_bag_contents);
          imageSource = rosbagSource->rosbag_image_source_engine;
          odomSource = rosbagSource->rosbag_odometry_source_engine;
        } else {
          printf("Odometry source not provided! aborting.");
          return;
        }
      } else if (imuSource == NULL && use_imu) {
        if (imuTopic != NULL) {
          printf("using IMU ROS topic: %s\n", imuTopic);
          rosbagSource = new ROSBagSourceEngine(calib_file.c_str(), bag_file.c_str(), colorTopic, depthTopic, imuTopic, externalOdometryTopic,
                                                colorSize, depthSize, publish_bag_contents);
          imageSource = rosbagSource->rosbag_image_source_engine;
          imuSource = rosbagSource->rosbag_imu_source_engine;
        } else {
          printf("IMU source not provided! aborting.");
          return;
        }
      } else {
        rosbagSource = new ROSBagSourceEngine(calib_file.c_str(), bag_file.c_str(), colorTopic, depthTopic, externalOdometryTopic,
                                              colorSize, depthSize, publish_bag_contents);
        imageSource = rosbagSource->rosbag_image_source_engine;
      }
    }


    // LIVE: Odometry Source Engine
    if (odomSource == NULL && use_odometry) {
      if (odometryTopic != NULL) {
        printf("using Odometry ROS topic: %s\n", odometryTopic);
        // ROVIO
        // Filter
        std::string rootdir = ros::package::getPath("rovio"); // Leaks memory
        std::string filter_config = rootdir + "/cfg/rovio.info";
        nh_private_.param("filter_config", filter_config, filter_config);
        // Filter
        rovioFilter_->readFromInfo(filter_config);
        // Force the camera calibration paths to the ones from ROS parameters.
        for (unsigned int camID = 0; camID < ROVIO_NCAM; ++camID) {
          std::string camera_config;
          if (nh_private_.getParam("camera" + std::to_string(camID)
                                    + "_config", camera_config)) {
            rovioFilter_->cameraCalibrationFile_[camID] = camera_config;
          }
        }
        rovioFilter_->refreshProperties();
        // Node
        rovioNode_ = new rovio::RovioNode<mtFilter>(nh_, nh_private_, rovioFilter_);
        rovioNode_->makeTest();
        odomSource = new ROSOdometrySourceEngine(odometryTopic);
      } else {
        printf("Odometry source not provided! aborting.");
        return;
      }
    }


    // LIVE: IMU Source Engine
    if (imuSource == NULL && use_imu) {
      if (imuTopic != NULL) {
        printf("using IMU ROS topic: %s\n", imuTopic);
        imuSource = new ROSIMUSourceEngine(imuTopic);
      } else {
        printf("IMU source not provided! aborting.");
        return;
      }
    }


    // LIVE: Image Source Engine
    // ROS
    if (imageSource == NULL &&
        (source == std::string("kinect_ros") || source == std::string("realsense_ros") || source == std::string("simulator")))
    {
      printf("trying ROS image source\n");
      imageSource = new ROSImageSourceEngine(
          calib_file.c_str(), colorTopic, depthTopic, colorSize, depthSize);
    }
    // Kinect
    if (imageSource == NULL && source == std::string("kinect"))
    {
      printf("trying MS Kinect 2 device\n");
      imageSource = new Kinect2Engine(calib_file.c_str());
    }
    // Realsense
    if (imageSource == NULL && source == std::string("realsense"))
    {
      printf("trying Intel Realsense device\n");
  //    imageSource = new RealsenseEngine(calibFile);
      imageSource = new ROSImageSourceEngine(
          calib_file.c_str(), colorTopic, depthTopic, colorSize, depthSize);
    }
    // VI-Sensor
    if (imageSource == NULL && source == std::string("vi-sensor"))
    {
      printf("trying Skybotix VI-Sensor\n");
      imageSource = new VISensorEngine(calib_file.c_str());
    }


    // this is a hack to ensure backwards compatibility in certain configurations
    if (imageSource == NULL) return;
    if (imageSource->calib.disparityCalib.params == Vector2f(0.0f, 0.0f))
    {
      imageSource->calib.disparityCalib.type = ITMDisparityCalib::TRAFO_AFFINE;
      imageSource->calib.disparityCalib.params = Vector2f(1.0f/1000.0f, 0.0f);
    }
  }

  ITMIoHandler(ros::NodeHandle& nh, ros::NodeHandle& nh_private) : rovioFilter_(new mtFilter) {
    nh_ = nh;
    nh_private_ = nh_private;
  };
  ~ITMIoHandler(void) {};
};


int main(int argc, char** argv)
try
{
  ros::Time::init();
  ros::init(argc, argv, "infinitam_cli_node");
  ros::NodeHandle nh;
  ros::NodeHandle nh_private("~");
  ROS_INFO("Starting infinitam_cli_node with node name %s", ros::this_node::getName().c_str());

  // IO Handler
  ITMIoHandler io_handler(nh, nh_private);
  if (!io_handler.LoadParameters()) {
    std::cout << "failed to load user settings!" << std::endl;
    return -1;
  }

  std::string imuTopic = nh_private.resolveName("imu_topic");
  std::string odometryTopic = nh_private.resolveName("odometry_topic");
  std::string colorTopic = nh_private.resolveName("color_topic");
  std::string depthTopic = nh_private.resolveName("depth_topic");
  std::string externalOdometryTopic = nh_private.resolveName("external_odometry_topic");
  std::string rovioCam0Topic = nh_private.resolveName("rovio_cam0_topic");
  std::string rovioCam1Topic = nh_private.resolveName("rovio_cam1_topic");
  std::string rovioImuTopic = nh_private.resolveName("rovio_imu_topic");

  printf("initialising ...\n");
  ROSBagSourceEngine *rosbagSource = NULL;
  ImageSourceEngine *imageSource = NULL;
  IMUSourceEngine *imuSource = NULL;
  OdometrySourceEngine *odomSource = NULL;

  io_handler.CreateDefaultSources(
      rosbagSource, imageSource, imuSource, odomSource, imuTopic.c_str(), odometryTopic.c_str(),
      colorTopic.c_str(), depthTopic.c_str(), externalOdometryTopic.c_str(),
      rovioCam0Topic.c_str(), rovioCam1Topic.c_str(), rovioImuTopic.c_str());

  if (imageSource==NULL)
  {
    std::cout << "failed to open any image stream" << std::endl;
    return -1;
  }
  if (imuSource==NULL && odomSource == NULL)
  {
    std::cout << "Proceeding without a pose source" << std::endl;
  }

  ITMLibSettings *internalSettings = new ITMLibSettings();
  internalSettings->sceneParams.viewFrustum_min = io_handler.viewfrustum_min;
  internalSettings->sceneParams.viewFrustum_max = io_handler.viewfrustum_max;
  internalSettings->storageDirectory = const_cast<char*>(io_handler.storage_directory.c_str());
  internalSettings->resourcesDirectory = const_cast<char*>(io_handler.resources_directory.c_str());
  internalSettings->sceneParams.voxelSize = io_handler.voxel_size;
  internalSettings->useSwapping = io_handler.use_swapping;
  internalSettings->deltaTime = io_handler.inactive_delta_time;
  internalSettings->visualizeICP = io_handler.visualize_icp;
  internalSettings->visualizeLC = io_handler.visualize_lc;
  internalSettings->depthTrackerType = io_handler.depth_tracker_type;
  internalSettings->LCTrackerType = io_handler.LC_tracker_type;
  internalSettings->inactiveReliabilityThresh = io_handler.inactive_reliability_thresh;
  internalSettings->numInactiveThresholdPercent = io_handler.num_inactive_threshold_percent;
  internalSettings->setTrackerType(io_handler.tracker_type);
  internalSettings->colorTrackerSkipFrameNumber = io_handler.color_tracker_skip_frame_number;
  internalSettings->depthTrackerSkipFrameNumber = io_handler.depth_tracker_skip_frame_number;

  std::cout << "Setting viewFrustum to the range: [ "
      << internalSettings->sceneParams.viewFrustum_min << ", "
      << internalSettings->sceneParams.viewFrustum_max << " ]" << std::endl;

	ITMMainEngine *mainEngine = new ITMMainEngine(internalSettings, &imageSource->calib, imageSource->getRGBImageSize(), imageSource->getDepthImageSize());

	CLIEngine::Instance()->Initialise(imageSource, imuSource, odomSource, mainEngine, "./Files/Out", internalSettings);
	CLIEngine::Instance()->Run();
	CLIEngine::Instance()->Shutdown();

  printf("Exiting InfiniTAM!");
  ros::shutdown();

  delete mainEngine;
  delete internalSettings;
  delete imageSource;
  if (imuSource != NULL) delete imuSource;
  if (odomSource != NULL) delete odomSource;
  return 0;
}
catch(std::exception& e)
{
  std::cerr << e.what() << '\n';
  return EXIT_FAILURE;
}


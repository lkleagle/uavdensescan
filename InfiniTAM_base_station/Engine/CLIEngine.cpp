// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#include "CLIEngine.h"

#include <string.h>

#include "../Utils/FileUtils.h"

using namespace InfiniTAM::Engine;
CLIEngine* CLIEngine::instance;

void CLIEngine::Initialise(ImageSourceEngine *imageSource, IMUSourceEngine *imuSource, OdometrySourceEngine *odomSource, ITMMainEngine *mainEngine,
	const char *outFolder, ITMLibSettings* itmSettings)
{
    this->internalSettings = itmSettings;
	this->imageSource = imageSource;
	this->imuSource = imuSource;
	this->odomSource = odomSource;
	this->mainEngine = mainEngine;
	{
		size_t len = strlen(outFolder);
		this->outFolder = new char[len + 1];
		strcpy(this->outFolder, outFolder);
	}

	this->currentFrameNo = 0;

	bool allocateGPU = false;
	if (itmSettings->deviceType == ITMLibSettings::DEVICE_CUDA) allocateGPU = true;

	inputRGBImage = new ITMUChar4Image(imageSource->getRGBImageSize(), true, allocateGPU);
	inputRawDepthImage = new ITMShortImage(imageSource->getDepthImageSize(), true, allocateGPU);
	inputIMUMeasurement = new ITMIMUMeasurement();
	inputOdometryMeasurement = new ITMOdometryMeasurement();

	mainLoopAction = PROCESS_PAUSED;
	mainLoopActionExecuted = true;
	rvizPose = new ITMPose();
	processedFrameNo = 0;
	processedTime = 0.0f;
	currentTime = 0.0f;
	last_lc_time = 0.0f;

#ifndef COMPILE_WITHOUT_CUDA
	ITMSafeCall(cudaThreadSynchronize());
#endif

	sdkCreateTimer(&timer_instant);
	sdkCreateTimer(&timer_average);

	sdkResetTimer(&timer_average);
	sdkStartTimerAndRewindByTime(&timer_instant, sdkGetTimerValue(&(mainEngine->main_timer)));

	ros_cli_sub = nh.subscribe("/itm/cli/config", 1, &CLIEngine::cliConfigCallback, this);

	printf("initialised.\n");
}

void CLIEngine::cliConfigCallback(const rviz_talking_view_controller::CLIEngine::ConstPtr& msg)
{
  cli_config_msg = *msg;

  // Rviz view pose
  geometry_msgs::TransformStamped pose = cli_config_msg.rviz_pose;
  if (cli_config_msg.freeview_enabled) {
    Matrix3f rviz_R;
    QPF q(pose.transform.rotation.w, pose.transform.rotation.x, pose.transform.rotation.y, pose.transform.rotation.z);
    MPF R(q);
    rviz_R = Matrix3f((MPF(1,0,0,0,-1,0,0,0,-1)*R).matrix().data());
    rvizPose->SetR(rviz_R);

    Vector3f rviz_T;
    rviz_T = rviz_R*Vector3f(-pose.transform.translation.x, -pose.transform.translation.y, -pose.transform.translation.z);
    rvizPose->SetT(rviz_T);

    rvizPose->Coerce();
  }

  // Save Mesh to disk
  if (cli_config_msg.save_mesh && mainLoopActionExecuted) {
    mainLoopAction = SAVE_TO_DISK;
    mainLoopActionExecuted = false;
  }

  // Abort CLI
  if (cli_config_msg.abort && mainLoopActionExecuted) {
    mainLoopAction = EXIT;
    mainLoopActionExecuted = false;
  }

  // Play/Pause
  if (cli_config_msg.pause && mainLoopActionExecuted) {
    mainLoopAction = PROCESS_PAUSED;
    mainLoopActionExecuted = false;
  } else if(!cli_config_msg.pause && mainLoopActionExecuted) {
    mainLoopAction = PROCESS_VIDEO;
    mainLoopActionExecuted = false;
  }
}

bool CLIEngine::SaveCameraPose(const char *filename, const Matrix4f &pose) const {
  std::ofstream stream;
  stream.open(filename, std::ios::trunc);
  stream << pose.m00 << " " << pose.m10 << " " << pose.m20 << " " << pose.m30 << std::endl;
  stream << pose.m01 << " " << pose.m11 << " " << pose.m21 << " " << pose.m31 << std::endl;
  stream << pose.m02 << " " << pose.m12 << " " << pose.m22 << " " << pose.m32 << std::endl;
  stream << "0.0 0.0 0.0 1.0";
  if (stream.fail()) return false;

  return true;
}

void CLIEngine::SaveSceneToMesh(const char *filename) const
{
	mainEngine->SaveSceneToMesh(filename);
}

void CLIEngine::SaveGraph(const char *filename) const
{
  mainEngine->SaveGraph(filename);
}

bool CLIEngine::ProcessFrame()
{
	if (!imageSource->hasMoreImages()) return false;
	imageSource->getImages(inputRGBImage, inputRawDepthImage);

	if (imuSource != NULL) {
		if (!imuSource->hasMoreMeasurements()) return false;
		else imuSource->getMeasurement(inputIMUMeasurement);
	} else if (odomSource != NULL) {
	  if (!odomSource->hasMoreMeasurements()) return false;
      else odomSource->getMeasurement(inputOdometryMeasurement);
	}

	sdkStartTimer(&timer_average);

	//actual processing on the mailEngine
	if (imuSource != NULL) {
	  mainEngine->ProcessFrame(inputRGBImage, inputRawDepthImage, inputIMUMeasurement, cli_config_msg.freeview_enabled ? rvizPose : NULL, cli_config_msg.visualize_scene);
	} else if (odomSource != NULL) {
	  mainEngine->ProcessFrame(inputRGBImage, inputRawDepthImage, inputOdometryMeasurement, cli_config_msg.freeview_enabled ? rvizPose : NULL, cli_config_msg.visualize_scene);
	}	else {
	  mainEngine->ProcessFrame(inputRGBImage, inputRawDepthImage, cli_config_msg.freeview_enabled ? rvizPose : NULL, cli_config_msg.visualize_scene);
	}

#ifndef COMPILE_WITHOUT_CUDA
	ITMSafeCall(cudaThreadSynchronize());
#endif
	sdkStopTimer(&timer_average);

	currentTime = sdkGetTimerValue(&timer_instant);
	processedTime = sdkGetAverageTimerValue(&timer_average);

	printf("frame %i: time %.2f, avg %.2f\n", currentFrameNo, currentTime, processedTime);

	currentFrameNo++;

	return true;
}

void CLIEngine::Run()
{
	while (mainLoopAction != EXIT) {
	  ros::spinOnce();
//	  std::cout << "main loop action: " << mainLoopAction << std::endl;
//	  std::cout << "main loop action exec?: " << mainLoopActionExecuted << std::endl;
	  switch(mainLoopAction)
	  {
    case PROCESS_FRAME:
      ProcessFrame();
      processedFrameNo++;
      mainLoopActionExecuted = true;
      mainLoopAction = PROCESS_PAUSED;
      break;
    case PROCESS_VIDEO:
      ProcessFrame();
      processedFrameNo++;
      mainLoopActionExecuted = true;
      mainLoopAction = PROCESS_VIDEO;
      break;
    case SAVE_TO_DISK:
      std::cout << "saving pose graph to disk ..." << std::endl;
      SaveGraph("pose_graph.txt");
      std::cout << "saving mesh to disk ..." << std::endl;
      SaveSceneToMesh("mesh.ply");
      std::cout << " done!" << std::endl;
      mainLoopActionExecuted = true;
      mainLoopAction = PROCESS_PAUSED;
      break;
    case PROCESS_PAUSED:
      mainLoopActionExecuted = true;
      break;
    default:
      break;
	  }
	}
}

void CLIEngine::Shutdown()
{
	sdkDeleteTimer(&timer_instant);
	sdkDeleteTimer(&timer_average);

	delete inputRGBImage;
	delete inputRawDepthImage;
	delete inputIMUMeasurement;
	delete inputOdometryMeasurement;

	delete[] outFolder;
	delete instance;
	instance = NULL;
}

/*
 * ROSBagSourceEngine.cpp
 *
 *  Created on: Mar 18, 2016
 *      Author: anurag
 */


#include "ROSBagSourceEngine.h"

#include "../Utils/FileUtils.h"

using namespace InfiniTAM::Engine;

cv::viz::Viz3d ROSBagSourceEngine::viz_window = cv::viz::Viz3d("IMU pose");
cv::Affine3f ROSBagSourceEngine::viz_pose = cv::Affine3f();

ROSBagImageSourceEngine::ROSBagImageSourceEngine(
    ROSBagSourceEngine& source_engine, const char *calibFilename, const char *bagFileName,
    const char *rgbTopic, const char *depthTopic, const char *groundTruthTopic,
    const Vector2i rgbSize, const Vector2i depthSize, const bool publishBagContents) :
        ROSImageSourceEngine(calibFilename, rgbSize, depthSize),
        source_engine_(&source_engine), ground_truth_odometry_topic_("/drz_rig/estimated_odometry"),
        rgb_topic_(rgbTopic), depth_topic_(depthTopic), ground_truth_topic_(groundTruthTopic)
{
  rgb_msg_ = nullptr;
  depth_msg_ = nullptr;
  rovio_cam0_msg_ = nullptr;
  rovio_cam1_msg_ = nullptr;
  ground_truth_msg_ = nullptr;
  ground_truth_odometry_msg_ = nullptr;

  publish_bag_contents_ = publishBagContents;

  bag_.open(bagFileName, rosbag::bagmode::Read);

  std::vector<std::string> topics;
  topics.push_back(rgb_topic_);
  topics.push_back(depth_topic_);
  topics.push_back(ground_truth_topic_);
  topics.push_back(ground_truth_odometry_topic_);

  bag_view_.addQuery(bag_, rosbag::TopicQuery(topics));
  current_bag_pos_ = bag_view_.begin();
}
ROSBagImageSourceEngine::ROSBagImageSourceEngine(
    ROSBagSourceEngine& source_engine, const char *calibFilename, const char *bagFileName,
    const char *rgbTopic, const char *depthTopic, const char *poseTopic, const char *groundTruthTopic,
    const Vector2i rgbSize, const Vector2i depthSize, const bool publishBagContents,
    const char *rovioCam0Topic,
    const char *rovioCam1Topic,
    const char *rovioImuTopic) :
        ROSImageSourceEngine(calibFilename, rgbSize, depthSize),
        source_engine_(&source_engine), ground_truth_odometry_topic_("/drz_rig/estimated_odometry"),
        rgb_topic_(rgbTopic), depth_topic_(depthTopic), pose_topic_(poseTopic), ground_truth_topic_(groundTruthTopic),
        rovio_cam0_topic_(rovioCam0Topic), rovio_cam1_topic_(rovioCam1Topic), rovio_imu_topic_(rovioImuTopic)
{
  rgb_msg_ = nullptr;
  depth_msg_ = nullptr;
  rovio_cam0_msg_ = nullptr;
  rovio_cam1_msg_ = nullptr;
  ground_truth_msg_ = nullptr;
  ground_truth_odometry_msg_ = nullptr;
  odom_msg_ = nullptr;

  publish_bag_contents_ = publishBagContents;

  bag_.open(bagFileName, rosbag::bagmode::Read);

  std::vector<std::string> topics;
  topics.push_back(rovio_cam0_topic_);
  topics.push_back(rovio_cam1_topic_);
  topics.push_back(rovio_imu_topic_);
  topics.push_back(rgb_topic_);
  topics.push_back(depth_topic_);
  topics.push_back(pose_topic_);
  topics.push_back(ground_truth_topic_);
  topics.push_back(ground_truth_odometry_topic_);

  bag_view_.addQuery(bag_, rosbag::TopicQuery(topics));
  current_bag_pos_ = bag_view_.begin();
}
void ROSBagImageSourceEngine::getImages(
    ITMUChar4Image *rgbImage, ITMShortImage *rawDepthImage){

  for (; current_bag_pos_ != bag_view_.end() && !got_new_image_pair_; ++current_bag_pos_) {
    rosbag::MessageInstance& message_view = *current_bag_pos_;

    // Read ROS RGB Image message
    if (message_view.getTopic() == rgb_topic_) {
      rgb_msg_ = message_view.instantiate<sensor_msgs::Image>();
      if(rgb_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << rgb_topic_
            << ", expected sensor_msgs::Image" << std::endl;
        exit(1);
      }
      // publish
      if (publish_bag_contents_) {
        source_engine_->rgb_publisher_.publish(rgb_msg_);
        ros::spinOnce();
      }
    }

    // Read ROS Depth Image message
    if (message_view.getTopic() == depth_topic_) {
      depth_msg_ = message_view.instantiate<sensor_msgs::Image>();
      if(depth_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << depth_topic_
            << ", expected sensor_msgs::Image" << std::endl;
        exit(1);
      }
      // Clock message from ROSBAG
      source_engine_->clock_msg_.clock = depth_msg_->header.stamp;
      source_engine_->clock_publisher_.publish(source_engine_->clock_msg_);
      ros::spinOnce();
      // publish
      if (publish_bag_contents_) {
        source_engine_->depth_publisher_.publish(depth_msg_);
        ros::spinOnce();
      }
    }

    // Read ROVIO Cam0 message
    if (message_view.getTopic() == rovio_cam0_topic_) {
      rovio_cam0_msg_ = message_view.instantiate<sensor_msgs::Image>();
      if(rovio_cam0_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << rovio_cam0_topic_
            << ", expected sensor_msgs::Image" << std::endl;
        exit(1);
      }
      // callback
      if (source_engine_->rosbag_odometry_source_engine != NULL) {
        source_engine_->rovioNode_->imgCallback0(rovio_cam0_msg_);
      }
      // publish
      if (publish_bag_contents_) {
        source_engine_->cam0_publisher_.publish(rovio_cam0_msg_);
        ros::spinOnce();
      }
    }

    // Read ROVIO Cam1 message
    if (message_view.getTopic() == rovio_cam1_topic_ && ROVIO_NCAM > 1) {
      rovio_cam1_msg_ = message_view.instantiate<sensor_msgs::Image>();
      if(rovio_cam1_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << rovio_cam1_topic_
            << ", expected sensor_msgs::Image" << std::endl;
        exit(1);
      }
      // callback
      if (source_engine_->rosbag_odometry_source_engine != NULL) {
        source_engine_->rovioNode_->imgCallback1(rovio_cam1_msg_);
      }
      // publish
      if (publish_bag_contents_) {
        source_engine_->cam1_publisher_.publish(rovio_cam1_msg_);
        ros::spinOnce();
      }
    }

    // Read ROVIO IMU message
    if (message_view.getTopic() == rovio_imu_topic_) {
      rovio_imu_msg_ = message_view.instantiate<sensor_msgs::Imu>();
      if(rovio_imu_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << rovio_imu_topic_
            << ", expected sensor_msgs::Imu" << std::endl;
        exit(1);
      }
      // Clock message from ROSBAG
      source_engine_->clock_msg_.clock = rovio_imu_msg_->header.stamp;
      source_engine_->clock_publisher_.publish(source_engine_->clock_msg_);
      ros::spinOnce();
      // callback
      if (source_engine_->rosbag_odometry_source_engine != NULL) {
        source_engine_->rovioNode_->imuCallback(rovio_imu_msg_);
      }
      // publish
      if (publish_bag_contents_) {
        source_engine_->imu_publisher_.publish(rovio_imu_msg_);
        ros::spinOnce();
      }
    }

    // Read ground truth message
    if (message_view.getTopic() == ground_truth_topic_) {
      ground_truth_msg_ = message_view.instantiate<geometry_msgs::TransformStamped>();
      if(ground_truth_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << ground_truth_topic_
            << ", expected geometry_msgs::TransformStamped" << std::endl;
        exit(1);
      }
      // publish
      if (publish_bag_contents_) {
        source_engine_->tf_publisher_.sendTransform(*ground_truth_msg_);
        source_engine_->ground_truth_publisher_.publish(ground_truth_msg_);
        ros::spinOnce();
      }
    }

    // Read ground truth odometry message
    if (message_view.getTopic() == ground_truth_odometry_topic_) {
      ground_truth_odometry_msg_ = message_view.instantiate<nav_msgs::Odometry>();
      if(ground_truth_odometry_msg_ == nullptr) {
        std::cout << "Wrong type on topic " << ground_truth_odometry_topic_
            << ", expected nav_msgs::Odometry" << std::endl;
        exit(1);
      }
      // publish
      if (publish_bag_contents_) {
        source_engine_->ground_truth_odometry_publisher_.publish(ground_truth_odometry_msg_);
        ros::spinOnce();
      }
    }

    // Read ROVIO Odometry message
    if (source_engine_->rosbag_odometry_source_engine != NULL) {
      bool update_odom = true;//false;
      if(boost::make_shared<nav_msgs::Odometry>(source_engine_->rovioNode_->odometryMsg_) != nullptr) {
        if (odom_msg_ == nullptr) {
          update_odom = true;
        } else if (ros::Duration(boost::make_shared<nav_msgs::Odometry>(source_engine_->rovioNode_->odometryMsg_)->header.stamp - odom_msg_->header.stamp).toSec() > 0) {
          update_odom = true;
        }
      }

      if (update_odom == true) {
        odom_msg_ = boost::make_shared<nav_msgs::Odometry>(source_engine_->rovioNode_->odometryMsg_);
        if (pow(odom_msg_->pose.pose.orientation.x,2) + pow(odom_msg_->pose.pose.orientation.y,2) +
            pow(odom_msg_->pose.pose.orientation.z,2) + pow(odom_msg_->pose.pose.orientation.w,2) > 0) {
          // callback
          if (source_engine_->rosbag_imu_source_engine != NULL) {
            source_engine_->rosbag_imu_source_engine->OdometryCallback_IMU(odom_msg_);
          } else if (source_engine_->rosbag_odometry_source_engine != NULL) {
            source_engine_->rosbag_odometry_source_engine->OdometryCallback_Odom(odom_msg_);
          }
        }
      }
    }

    // Read ROS Pose message
    if (message_view.getTopic() == pose_topic_) {
      if (message_view.getDataType() == std::string("nav_msgs/Odometry")) {
        odom_msg_ = message_view.instantiate<nav_msgs::Odometry>();
        if(odom_msg_ == nullptr) {
          std::cout << "Wrong type on topic " << pose_topic_
              << ", expected nav_msgs::Odometry" << std::endl;
          exit(1);
        }
        // callback
        if (source_engine_->rosbag_imu_source_engine != NULL) {
          source_engine_->rosbag_imu_source_engine->OdometryCallback_IMU(odom_msg_);
        } else if (source_engine_->rosbag_odometry_source_engine != NULL) {
          source_engine_->rosbag_odometry_source_engine->OdometryCallback_Odom(odom_msg_);
        }
      }
      if (message_view.getDataType() == std::string("sensor_msgs/Imu")) {
        imu_msg_ = message_view.instantiate<sensor_msgs::Imu>();
        if(imu_msg_ == nullptr) {
          std::cout << "Wrong type on topic " << pose_topic_
              << ", expected sensor_msgs::Imu" << std::endl;
          exit(1);
        }
        // callback
        if (source_engine_->rosbag_imu_source_engine != NULL) {
          source_engine_->rosbag_imu_source_engine->IMUCallback_IMU(imu_msg_);
        }
      }
      if (message_view.getDataType() == std::string("geometry_msgs/TransformStamped")) {
        tf_msg_ = message_view.instantiate<geometry_msgs::TransformStamped>();
        if(tf_msg_ == nullptr) {
          std::cout << "Wrong type on topic " << pose_topic_
              << ", expected geometry_msgs::TransformStamped" << std::endl;
          exit(1);
        }
        // callback
        if (source_engine_->rosbag_imu_source_engine != NULL) {
          source_engine_->rosbag_imu_source_engine->TFCallback_IMU(tf_msg_);
        } else if (source_engine_->rosbag_odometry_source_engine != NULL) {
          source_engine_->rosbag_odometry_source_engine->TFCallback_Odom(tf_msg_);
        }
      }
      if (message_view.getDataType() == std::string("geometry_msgs/PoseStamped")) {
        pose_msg_ = message_view.instantiate<geometry_msgs::PoseStamped>();
        if(pose_msg_ == nullptr) {
          std::cout << "Wrong type on topic " << pose_topic_
              << ", expected geometry_msgs::PoseStamped" << std::endl;
          exit(1);
        }
        // callback
        if (source_engine_->rosbag_imu_source_engine != NULL) {
          source_engine_->rosbag_imu_source_engine->PoseCallback_IMU(pose_msg_);
        } else if (source_engine_->rosbag_odometry_source_engine != NULL) {
          source_engine_->rosbag_odometry_source_engine->PoseCallback_Odom(pose_msg_);
        }
      }

      // viz
      if (source_engine_->rosbag_imu_source_engine != NULL) {
        source_engine_->viz_cached_pose_ = source_engine_->rosbag_imu_source_engine->cached_imu->R;
      } else if (source_engine_->rosbag_odometry_source_engine != NULL) {
        source_engine_->viz_cached_pose_ = source_engine_->rosbag_odometry_source_engine->cached_odom->R;
      }
      source_engine_->VisualizePose();
    }

    // check if RGB and Depth image msgs have close enough timestamps to consider as pair
    if (rgb_msg_ != nullptr && depth_msg_ != nullptr) {
      if (ros::Duration(rgb_msg_->header.stamp - depth_msg_->header.stamp).toSec() < 0.07) {
        got_new_image_pair_ = true;
        source_engine_->rosbag_image_source_engine->ROSImageCallback(rgb_msg_, depth_msg_);

        // fill up ITM data
        Vector4u *rgb = rgbImage->GetData(MEMORYDEVICE_CPU);
        short *depth = rawDepthImage->GetData(MEMORYDEVICE_CPU);
        if (colorAvailable_ && depthAvailable_)
        {
          // RGB data
          uchar* rgb_pointer = (uchar*)rgb_.data;
          for (int j = 0; j < imageSize_rgb_.y; ++j)
          {
            for (int i = 0; i < imageSize_rgb_.x; ++i)
            {
              rgb[j*imageSize_rgb_.x + i].b = rgb_pointer[j*3*imageSize_rgb_.x + 3*i];
              rgb[j*imageSize_rgb_.x + i].g = rgb_pointer[j*3*imageSize_rgb_.x + 3*i +1];
              rgb[j*imageSize_rgb_.x + i].r = rgb_pointer[j*3*imageSize_rgb_.x + 3*i + 2];
              rgb[j*imageSize_rgb_.x + i].a = 255.0;
            }
          }

          // Depth data
          unsigned short* depth_pointer = (unsigned short*)depth_.data;
          for (int i = 0; i < imageSize_d_.x * imageSize_d_.y; ++i) {
            depth[i] = (short)depth_pointer[i];
          }
        }else{
          memset(depth, 0, rawDepthImage->dataSize * sizeof(short));
          memset(rgb, 0, rgbImage->dataSize * sizeof(Vector4u));
        }
      }
    }
  } // seeking through bag until I get an image pair

  got_new_image_pair_ = false;
  rgb_msg_ = nullptr;
  depth_msg_ = nullptr;
  return /*true*/;
}
bool ROSBagImageSourceEngine::hasMoreImages(void) {
  return (current_bag_pos_ != bag_view_.end());
}
Vector2i ROSBagImageSourceEngine::getDepthImageSize(void) { return imageSize_d_; }
Vector2i ROSBagImageSourceEngine::getRGBImageSize(void) { return imageSize_rgb_; }



ROSBagIMUSourceEngine::ROSBagIMUSourceEngine() : ROSIMUSourceEngine() { }
bool ROSBagIMUSourceEngine::hasMoreMeasurements(void)
{
  return (cached_imu != NULL);
}
void ROSBagIMUSourceEngine::getMeasurement(ITMIMUMeasurement *imu)
{
  if (cached_imu != NULL)
  {
    ROS_INFO("Using IMU data...");
    imu->R = cached_imu->R;
    delete cached_imu;
    cached_imu = NULL;
  }
}



ROSBagOdometrySourceEngine::ROSBagOdometrySourceEngine() :
    ROSOdometrySourceEngine() { }
bool ROSBagOdometrySourceEngine::hasMoreMeasurements(void)
{
  return (cached_odom != NULL);
}
void ROSBagOdometrySourceEngine::getMeasurement(ITMOdometryMeasurement *odom)
{
  if (cached_odom != NULL)
  {
    ROS_INFO("Using ODOM data...");
    odom->R = cached_odom->R;
    odom->t = cached_odom->t;
//    odom->cov = cached_odom->cov;
    delete cached_odom;
    cached_odom = NULL;
  }
}


ROSBagSourceEngine::ROSBagSourceEngine(
    const char *calibFilename, const char *bagFileName,
    const char *rgbTopic, const char *depthTopic, const char *groundTruthTopic,
    const Vector2i rgbSize, const Vector2i depthSize, const bool publishBagContents) :
        viz_key_event(cv::viz::KeyboardEvent::Action::KEY_DOWN, "A", cv::viz::KeyboardEvent::ALT, 1)
{
  rosbag_image_source_engine = new ROSBagImageSourceEngine(
      *this, calibFilename, bagFileName, rgbTopic, depthTopic, groundTruthTopic, rgbSize, depthSize, publishBagContents);
  rosbag_imu_source_engine = NULL;
  rosbag_odometry_source_engine = NULL;

  clock_publisher_ = nh_.advertise<rosgraph_msgs::Clock>("clock", 1000);
  if (publishBagContents) {
    rgb_publisher_ = nh_.advertise<sensor_msgs::Image>(rgbTopic, 10);
    depth_publisher_ = nh_.advertise<sensor_msgs::Image>(depthTopic, 10);
    ground_truth_publisher_ = nh_.advertise<geometry_msgs::TransformStamped>(groundTruthTopic, 10);
    ground_truth_odometry_publisher_ = nh_.advertise<nav_msgs::Odometry>("/drz_rig/estimated_odometry", 10);
  }

  // Add camera coordinate axes visualization widget
  viz_window.setWindowSize(cv::Size(600, 600));
//  viz_window.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem(200.0));
//  viz_window.showWidget("Test Sphere", cv::viz::WSphere(cv::Point3f(100.0, 0.0, 0.0), 5.0));
  viz_window.showWidget("Camera Widget", cv::viz::WCoordinateSystem(100.0));
  viz_window.registerKeyboardCallback(VizKeyboardCallback);
}
// imu
ROSBagSourceEngine::ROSBagSourceEngine(
    const char *calibFilename, const char *bagFileName,
    const char *rgbTopic, const char *depthTopic, const char *poseTopic, const char *groundTruthTopic,
    const Vector2i rgbSize, const Vector2i depthSize, const bool publishBagContents) :
        viz_cached_pose_(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
        viz_key_event(cv::viz::KeyboardEvent::Action::KEY_DOWN, "A", cv::viz::KeyboardEvent::ALT, 1)
{
  rosbag_image_source_engine = new ROSBagImageSourceEngine(
      *this, calibFilename, bagFileName, rgbTopic, depthTopic, poseTopic, groundTruthTopic, rgbSize, depthSize, publishBagContents);
  rosbag_imu_source_engine = new ROSBagIMUSourceEngine();
  rosbag_odometry_source_engine = NULL;

  clock_publisher_ = nh_.advertise<rosgraph_msgs::Clock>("clock", 1000);
  if (publishBagContents) {
    rgb_publisher_ = nh_.advertise<sensor_msgs::Image>(rgbTopic, 10);
    depth_publisher_ = nh_.advertise<sensor_msgs::Image>(depthTopic, 10);
    ground_truth_publisher_ = nh_.advertise<geometry_msgs::TransformStamped>(groundTruthTopic, 10);
    ground_truth_odometry_publisher_ = nh_.advertise<nav_msgs::Odometry>("/drz_rig/estimated_odometry", 10);
  }

  // Add camera coordinate axes visualization widget
  viz_window.setWindowSize(cv::Size(600, 600));
//  viz_window.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem(200.0));
//  viz_window.showWidget("Test Sphere", cv::viz::WSphere(cv::Point3f(100.0, 0.0, 0.0), 5.0));
  viz_window.showWidget("Camera Widget", cv::viz::WCoordinateSystem(100.0));
  viz_window.registerKeyboardCallback(VizKeyboardCallback);
}
// odom
ROSBagSourceEngine::ROSBagSourceEngine(
    ros::NodeHandle& nh, ros::NodeHandle& nh_private,
    const char *calibFilename, const char *bagFileName,
    const char *rgbTopic, const char *depthTopic, const char *poseTopic, const char *groundTruthTopic,
    const char *rovioCam0Topic, const char *rovioCam1Topic, const char *rovioImuTopic,
    const Vector2i rgbSize, const Vector2i depthSize, const bool publishBagContents) :
        nh_(nh), nh_private_(nh_private), viz_cached_pose_(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
        viz_key_event(cv::viz::KeyboardEvent::Action::KEY_DOWN, "A", cv::viz::KeyboardEvent::ALT, 1),
        rovioFilter_(new mtFilter)
{
  rosbag_image_source_engine = new ROSBagImageSourceEngine(
      *this, calibFilename, bagFileName, rgbTopic, depthTopic, poseTopic, groundTruthTopic,
      rgbSize, depthSize, publishBagContents, rovioCam0Topic, rovioCam1Topic, rovioImuTopic);
  rosbag_imu_source_engine = NULL;
  rosbag_odometry_source_engine = new ROSBagOdometrySourceEngine();

  clock_publisher_ = nh_.advertise<rosgraph_msgs::Clock>("clock", 1000);
  if (publishBagContents) {
    rgb_publisher_ = nh_.advertise<sensor_msgs::Image>(rgbTopic, 10);
    depth_publisher_ = nh_.advertise<sensor_msgs::Image>(depthTopic, 10);
    cam0_publisher_ = nh_.advertise<sensor_msgs::Image>(rovioCam0Topic, 10);
    cam1_publisher_ = nh_.advertise<sensor_msgs::Image>(rovioCam1Topic, 10);
    imu_publisher_ = nh_.advertise<sensor_msgs::Imu>(rovioImuTopic, 10);
    ground_truth_publisher_ = nh_.advertise<geometry_msgs::TransformStamped>(groundTruthTopic, 10);
    ground_truth_odometry_publisher_ = nh_.advertise<nav_msgs::Odometry>("/drz_rig/estimated_odometry", 10);
  }

  // ROVIO
  // Filter
  std::string rootdir = ros::package::getPath("rovio"); // Leaks memory
  std::string filter_config = rootdir + "/cfg/rovio.info";
  nh_private_.param("filter_config", filter_config, filter_config);
  // Filter
  rovioFilter_->readFromInfo(filter_config);
  // Force the camera calibration paths to the ones from ROS parameters.
  for (unsigned int camID = 0; camID < ROVIO_NCAM; ++camID) {
    std::string camera_config;
    if (nh_private_.getParam("camera" + std::to_string(camID)
                              + "_config", camera_config)) {
      rovioFilter_->cameraCalibrationFile_[camID] = camera_config;
    }
  }
  rovioFilter_->refreshProperties();
  // Node
  rovioNode_ = new rovio::RovioNode<mtFilter>(nh_, nh_private_, rovioFilter_, false);
  rovioNode_->makeTest();
  rovioNode_->forceOdometryPublishing_ = true;

  // Add camera coordinate axes visualization widget
  viz_window.setWindowSize(cv::Size(600, 600));
//  viz_window.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem(200.0));
//  viz_window.showWidget("Test Sphere", cv::viz::WSphere(cv::Point3f(100.0, 0.0, 0.0), 5.0));
  viz_window.showWidget("Camera Widget", cv::viz::WCoordinateSystem(100.0));
  viz_window.registerKeyboardCallback(VizKeyboardCallback);
}
void ROSBagSourceEngine::VisualizePose() {

  // Construct pose
  cv::Mat pose_mat(3, 3, CV_32F);
  float* mat_pointer = (float*)pose_mat.data;
  for (int row = 0; row < 3; ++row) {
    for (int col = 0; col < 3; ++col) {
      mat_pointer[3*row + col] = viz_cached_pose_(col, row);
    }
  }
  viz_pose.rotation(pose_mat);
  viz_pose.translate(cv::Vec3f(0.0, 0.0, 0.0));
  viz_window.setWidgetPose("Camera Widget", viz_pose);
  viz_window.spinOnce(1, true);
}

// Copyright 2014-2015 Isis Innovation Limited and the authors of InfiniTAM

#pragma once

#include <iostream>
#include <fstream>

#include "../ITMLib/Engine/ITMMainEngine.h"
#include "../ITMLib/Utils/ITMLibSettings.h"
#include "../Utils/FileUtils.h"
#include "../Utils/NVTimer.h"

#include "ImageSourceEngine.h"
#include "IMUSourceEngine.h"
#include "OdometrySourceEngine.h"

#include <std_msgs/Bool.h>
#include <rviz_talking_view_controller/CLIEngine.h>

#include <vector>

namespace InfiniTAM
{
	namespace Engine
	{
		class CLIEngine
		{
			static CLIEngine* instance;

			enum MainLoopAction
      {
        PROCESS_PAUSED, PROCESS_FRAME, PROCESS_VIDEO, EXIT, SAVE_TO_DISK
      } mainLoopAction;
      bool mainLoopActionExecuted;

			ITMLibSettings* internalSettings;
			ImageSourceEngine *imageSource;
			IMUSourceEngine *imuSource;
			OdometrySourceEngine *odomSource;
			ITMMainEngine *mainEngine;

			StopWatchInterface *timer_instant;
			StopWatchInterface *timer_average;

		private:
			ITMUChar4Image *inputRGBImage; ITMShortImage *inputRawDepthImage;
			ITMIMUMeasurement *inputIMUMeasurement;
			ITMOdometryMeasurement *inputOdometryMeasurement;

			ros::Subscriber ros_cli_sub;
			rviz_talking_view_controller::CLIEngine cli_config_msg;
			ITMPose *rvizPose;
			ros::NodeHandle nh;

			void cliConfigCallback(const rviz_talking_view_controller::CLIEngine::ConstPtr& msg);

			int currentFrameNo;
		public:
			static CLIEngine* Instance(void) {
				if (instance == NULL) instance = new CLIEngine();
				return instance;
			}

			float processedTime, currentTime;
			float last_lc_time;
			int processedFrameNo;
			char *outFolder;

			void Initialise(ImageSourceEngine *imageSource, IMUSourceEngine *imuSource, OdometrySourceEngine *odomSource, ITMMainEngine *mainEngine,
				const char *outFolder, ITMLibSettings* itmSettings);
			void Shutdown();

			void Run();
			bool ProcessFrame();

			bool SaveCameraPose(const char *filename, const Matrix4f &pose) const;
			void SaveSceneToMesh(const char *filename) const;
			void SaveGraph(const char *filename) const;
		};
	}
}
